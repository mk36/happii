var gulp 		 = require('gulp'),
	plumber		 = require('gulp-plumber'),
	watch 		 = require('gulp-watch'),
	rename 	 	 = require('gulp-rename'),
	autoprefixer = require('gulp-autoprefixer'),
	concat 		 = require('gulp-concat'),
	uglify 		 = require('gulp-uglify'),
	imagemin	 = require('gulp-imagemin'),
	cache 		 = require('gulp-cache'),
	minifycss 	 = require('gulp-clean-css'),
	sourcemaps	 = require('gulp-sourcemaps'),
	sass 		 = require('gulp-sass'),
	order		 = require('gulp-order'),
	browserSync  = require('browser-sync').create(),
	sassWatcher  = gulp.watch("css/**/*.scss", ['sass']);

gulp.task('images', function(){
	gulp.src('images/**/*')
		.pipe(cache(imagemin({
			optimizationLevel: 4,
			progressive: true,
			interlaced: true
		})))
		.pipe(gulp.dest('./images/'));
});

gulp.task('sass', function(){
	gulp.src(['css/styles.scss']).pipe(plumber({
		errorHandler: function (error) {
			console.log(error.message);
			this.emit('end');
		}})).pipe(sass())
		.pipe(autoprefixer('last 2 versions'))
		.pipe(gulp.dest('css/'))
		.pipe(minifycss())
		.pipe(gulp.dest('css'))
		.pipe(browserSync.stream());
});

gulp.task('serve', ['sass'], function() {

	browserSync.init({
		proxy: "localhost:8888"
	});

	gulp.watch("./css/**/*.scss", ['sass']).on('change', browserSync.reload);
	gulp.watch("./js/**/*.js", ['js']).on('change', browserSync.reload);
	gulp.watch("./templates/**/*.htm").on('change', browserSync.reload);
});

gulp.task('js', function () {
	return gulp.src([

		'./components/jquery/jquery.js',
		'./components/angular/angular.js',
		'./components/angular-bootstrap/ui-bootstrap.js',
		'./components/angular-bootstrap/ui-bootstrap-tpls.js',
		'./components/angular-ui-router/release/angular-ui-router.js',
		'./components/bootstrap/dist/js/bootstrap.js',
		'./components/scrollmagic/scrollmagic/uncompressed/ScrollMagic.js',
		'./components/angular-local-storage/dist/angular-local-storage.js',
		'./components/lodash/dist/lodash.js',
		'./components/pwstrength-bootstrap/dist/pwstrength-bootstrap.js',
		'./components/angularjs-slider/dist/rzslider.js',
		'./components/slick-carousel/slick/slick.js',
		'./components/angular-slick-carousel/dist/angular-slick.js',
		'./components/dropzone/dist/dropzone.js',
		'./components/dropzone/dist/dropzone-amd-module.js',
		'./components/cropperjs/dist/cropper.js',
		'./components/satellizer/dist/satellizer.js',
		'./components/video.js/dist/video.js',
		'./components/fullpage.js/dist/jquery.fullpage.js',
		'./components/fullpage.js/dist/jquery.fullpage.extensions.min.js',
		'./components/flip/dist/jquery.flip.js',
		'./components/jQuery.mmenu/dist/js/jquery.mmenu.all.min.js',
		'./components/aos/dist/aos.js',
		'./components/mediaelement/build/mediaelement-and-player.js',
		'./node_modules/blueimp-canvas-to-blob/js/canvas-to-blob.js',
		'./components/jssocials/dist/jssocials.js',
		'./components/twitter-fetcher/js/twitterFetcher.js',

		'./app/app.js',
		'./app/ui-routes.js',


		//typescript
		'./app/Typescript/*.js',
		'./app/Typescript/builder/*.js',
		'./app/Typescript/register/*.js',

		//controllers
		'./app/controllers/*.js',

		'./app/controllers/builder/*.js',
		'./app/controllers/builder/questions/*.js',
		'./app/controllers/builder/questions/basicStats/*.js',
		'./app/controllers/builder/questions/imageSection/*.js',
		'./app/controllers/builder/questions/interests/*.js',
		'./app/controllers/builder/questions/partner/*.js',
		'./app/controllers/builder/questions/profileDescription/*.js',
		'./app/controllers/builder/questions/travel/*.js',

		'./app/controllers/home/*.js',
		'./app/controllers/profile/*.js',
		'./app/controllers/register/*.js',

		//directives
		'./app/directives/*.js',
		'./app/directives/profile/*.js',
		'./app/directives/profile/gallery/*.js',
		'./app/directives/questions/*.js',

		//services
		'./app/services/*.js'
	])

	//select all javascript files under js/ and any subdirectory

		.pipe(order([
			'components/jquery/jquery.js',
			'components/angular/angular.js',
			'components/angular-bootstrap/ui-bootstrap.js',
			'components/angular-bootstrap/ui-bootstrap-tpls.js',
			'components/angular-ui-router/release/angular-ui-router.js',
			'components/bootstrap/dist/js/bootstrap.js',
			'components/scrollmagic/scrollmagic/uncompressed/ScrollMagic.js',
			'components/angular-local-storage/dist/angular-local-storage.js',
			'components/lodash/dist/lodash.js',
			'components/pwstrength-bootstrap/dist/pwstrength-bootstrap.js',
			'components/angularjs-slider/dist/rzslider.js',
			'components/slick-carousel/slick/slick.js',
			'components/angular-slick-carousel/dist/angular-slick.js',
			'components/dropzone/dist/dropzone.js',
			'components/dropzone/dist/dropzone-amd-module.js',
			'components/cropperjs/dist/cropper.js',
			'components/satellizer/dist/satellizer.js',
			'components/video.js/dist/video.js',
			'components/fullpage.js/dist/jquery.fullpage.js',
			'components/fullpage.js/dist/jquery.fullpage.extensions.min.js',
			'components/flip/dist/jquery.flip.js',
			'components/jQuery.mmenu/dist/js/jquery.mmenu.all.min.js',
			'components/aos/dist/aos.js',
			'components/mediaelement/build/mediaelement-and-player.js',
			'node_modules/blueimp-canvas-to-blob/js/canvas-to-blob.js',
			'components/jssocials/dist/jssocials.js',
			'components/twitter-fetcher/js/twitterFetcher.js',

			'app/app.js',
			'app/ui-routes.js',


			//typescript
			'app/Typescript/*.js',
			'app/Typescript/builder/*.js',
			'app/Typescript/register/*.js',

			//controllers
			'app/controllers/*.js',

			'app/controllers/builder/*.js',
			'app/controllers/builder/questions/*.js',
			'app/controllers/builder/questions/basicStats/*.js',
			'app/controllers/builder/questions/imageSection/*.js',
			'app/controllers/builder/questions/interests/*.js',
			'app/controllers/builder/questions/partner/*.js',
			'app/controllers/builder/questions/profileDescription/*.js',
			'app/controllers/builder/questions/travel/*.js',

			'app/controllers/home/*.js',
			'app/controllers/profile/*.js',
			'app/controllers/register/*.js',

			//directives
			'app/directives/*.js',
			'app/directives/profile/*.js',
			'app/directives/profile/gallery/*.js',
			'app/directives/questions/*.js',

			//services
			'app/services/*.js'


		], {
			base: './'
		}))
		.pipe(concat('main.min.js'))
		//DO THIS AFTER SITE IS DONE - TAKES A FEW EXTRA SECONDS OTHERWISE
		//.pipe(uglify({
		//mangle: false
		//}))
		.pipe(gulp.dest('./'));

	//the destination folder

});


gulp.task('watch', function(){
	sassWatcher.on('change', function(event){
		console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
	});
	//gulp.watch("./js/**/*.js", ['js']);
});