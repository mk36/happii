var register = (function () {
    function register(localStorageService, propertiesObj) {
        this.properties = {
            firstName: null,
            surname: null,
            email: null,
            mobile: null,
            password: null,
            gender: null,
            dob: null,
            location: {
                address: null,
                lat: null,
                lng: null,
                county: null,
                city: null
            }
        };
        this._injector = angular.injector(['ng']).get('$injector');
        this.localStorage = localStorageService;
        this.http = this._injector.get('$http');
        if (propertiesObj != null) {
            this.properties = propertiesObj;
        }
    }
    /**
     * Set the first name for the account
     *
     * @param firstname
     */
    register.prototype.setFirstName = function (firstname) {
        this.properties.firstName = firstname;
    };
    /**
     * Set the surname for the account
     *
     * @param surname
     */
    register.prototype.setSurname = function (surname) {
        this.properties.surname = surname;
    };
    /**
     * Set the email for the account
     *
     * @param email
     */
    register.prototype.setEmail = function (email) {
        this.properties.email = email;
    };
    /**
     * Set mob number for the account
     *
     * @param mobNumber
     */
    register.prototype.setMobileNo = function (mobNumber) {
        this.properties.mobile = mobNumber;
    };
    /**
     * Set the password for the account
     *
     * @param password
     */
    register.prototype.setPassword = function (password) {
        this.properties.password = password;
    };
    /**
     * Set the gender for the account
     * @param gender
     */
    register.prototype.setGender = function (gender) {
        this.properties.gender = gender;
        //Save the object
        this.update();
    };
    /**
     * Set the date of birth for the account
     *
     * @param day
     * @param month
     * @param year
     */
    register.prototype.setDob = function (day, month, year) {
        this.properties.dob = year + '-' + month + '-' + day;
        //Save the object
        this.update();
    };
    /**
     * Set the location for the account
     *
     * @param address
     * @param lat
     * @param lng
     */
    register.prototype.setLocation = function (address, lat, lng, county, city) {
        this.properties.location.address = address;
        this.properties.location.lat = lat;
        this.properties.location.lng = lng;
        this.properties.location.county = county;
        this.properties.location.city = city;
        //Save the object
        this.update();
    };
    /**
     * Set the personal details
     *
     * @param forename
     * @param surname
     * @param email
     * @param mobileNo
     * @param password
     */
    register.prototype.setPersonalDetails = function (forename, surname, email, mobileNo, password) {
        this.setFirstName(forename);
        this.setSurname(surname);
        this.setEmail(email);
        this.setMobileNo(mobileNo);
        this.setPassword(password);
        //Save the object
        this.update();
    };
    //Use this for local storage
    register.prototype.update = function () {
        this.localStorage.set('registerObject', this);
    };
    //Use this for the api save
    register.prototype.save = function ($q, $http) {
        var defer = $q.defer();
        var url = window.server_path + 'hAPI/account/register', data = {
            profile: this.properties
        };
        console.log('url', url);
        $http.post(url, data).
            then(function (data, status, headers, config) {
            console.log('Saved profile to server', data);
            defer.resolve(data);
        }).
            catch(function (data, status, headers, config) {
            console.log('Failure to save profile', data, status, headers, config);
            defer.reject(data);
        });
        return defer.promise;
    };
    return register;
}());
