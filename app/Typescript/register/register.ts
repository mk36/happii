class register {

    protected properties = {
        firstName: <string> null,
        surname: <string> null,
        email: <string> null,
        mobile: <string> null,
        password: <string> null,
        gender: <string> null,
        dob: <string> null,
        location: {
            address: <string> null,
            lat: <number> null,
            lng: <number> null,
            county : null,
            city : null
        }
    };

    protected localStorage: null;

    protected
        _injector: angular.auto.IInjectorService = angular.injector(['ng']).get('$injector');

    protected
        http: null;


    constructor(localStorageService, propertiesObj?) {

        this.localStorage = localStorageService;

        this.http = this._injector.get('$http');

        if(propertiesObj != null){
            this.properties = propertiesObj;
        }
    }


    /**
     * Set the first name for the account
     *
     * @param firstname
     */
    setFirstName(firstname: string){

        this.properties.firstName = firstname;
    }

    /**
     * Set the surname for the account
     *
     * @param surname
     */
    setSurname(surname: string){

        this.properties.surname = surname;
    }

    /**
     * Set the email for the account
     *
     * @param email
     */
    setEmail(email: string){
        this.properties.email = email;
    }

    /**
     * Set mob number for the account
     *
     * @param mobNumber
     */
    setMobileNo(mobNumber: string){
        this.properties.mobile = mobNumber;
    }

    /**
     * Set the password for the account
     *
     * @param password
     */
    setPassword(password: string){
        this.properties.password = password;
    }

    /**
     * Set the gender for the account
     * @param gender
     */
    setGender(gender: string){
        this.properties.gender = gender;

        //Save the object
        this.update();
    }

    /**
     * Set the date of birth for the account
     *
     * @param day
     * @param month
     * @param year
     */
    setDob(day: string, month: string, year: string){
        this.properties.dob = year + '-' + month + '-' + day;

        //Save the object
        this.update();
    }

    /**
     * Set the location for the account
     *
     * @param address
     * @param lat
     * @param lng
     */
    setLocation(address: string, lat: number, lng: number, county, city){

        this.properties.location.address = address;
        this.properties.location.lat = lat;
        this.properties.location.lng = lng;
        this.properties.location.county = county;
        this.properties.location.city = city;

        //Save the object
        this.update();
    }

    /**
     * Set the personal details
     *
     * @param forename
     * @param surname
     * @param email
     * @param mobileNo
     * @param password
     */
    setPersonalDetails(forename: string, surname: string, email: string, mobileNo: string, password: string){

        this.setFirstName(forename);
        this.setSurname(surname);
        this.setEmail(email);
        this.setMobileNo(mobileNo);
        this.setPassword(password);

        //Save the object
        this.update();
    }

    //Use this for local storage
    update(){
        this.localStorage.set('registerObject', this);
    }

    //Use this for the api save
    save($q, $http){

        var defer = $q.defer();


        var url = window.server_path + 'hAPI/account/register',
            data = {
                profile: this.properties
            };

        console.log('url', url);

        $http.post(url, data).
        then(function(data, status, headers, config) {

            console.log('Saved profile to server', data);
            defer.resolve(data);

        }).
        catch(function(data, status, headers, config) {

            console.log('Failure to save profile', data, status, headers, config);
            defer.reject(data);
        });


        return defer.promise;
    }

}