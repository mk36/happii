var profileBuilder = (function () {
    function profileBuilder(localStorageService, propertiesObj) {
        this.properties = {
            accountId: null,
            firstName: null,
        };
        this.localStorage = localStorageService;
        if (propertiesObj != null) {
            this.properties = propertiesObj;
        }
    }
    /**
     * Set the first name for the account
     *
     * @param firstname
     */
    profileBuilder.prototype.setFirstName = function (firstname) {
        this.properties.firstName = firstname;
    };
    profileBuilder.prototype.save = function () {
        this.localStorage.set('profileBuilderObject', this);
    };
    return profileBuilder;
}());
