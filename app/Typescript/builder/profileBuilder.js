var profileBuilder = (function () {
    function profileBuilder(localStorageService, $http, propertiesObj) {
        this.$http = null;
        this.properties = {
            // accountId: <number> null,
            profileDescription: null,
            questions: {
                // Basic Stats
                locationFrom: {
                    address: null,
                    lat: null,
                    lng: null
                },
                education: null,
                employment: null,
                religion: null,
                height: null,
                bodyType: null,
                children: null,
                smoking: null,
                drinking: null,
                profileImage: {},
                profileCoverImage: {},
                profileGallery: [],
                profileVideos: [],
                profileFaveVideos: [],
                // Interests
                personality: [],
                pastTimes: [],
                books: [],
                music: [],
                viewings: [],
                sports: [],
                //Travel
                travelBeen: [],
                travelGo: [],
                //Partner
                partnerGender: null,
                partnerAgeRange: {
                    min: null,
                    max: null,
                },
                partnerLocation: {
                    name: null,
                    lat: null,
                    lng: null,
                    miles: null
                },
                partnerChildren: null,
                partnerSmoking: null,
                partnerDrinking: null,
                partnerHeight: {
                    min: null,
                    max: null,
                },
                partnerEducation: null,
                partnerEmployment: null,
                partnerBodyType: null,
                partnerReligion: null,
                partnerPersonality: [],
                partnerPastTimes: [],
            }
        };
        this.localStorage = null;
        this.localStorage = localStorageService;
        this.$http = $http;
        if (propertiesObj != null) {
            this.properties = _.merge(this.properties, propertiesObj);
        }
        this.properties.questions = this.properties.questions || null;
    }
    /**
     * Set the profile description of the object
     *
     * @param description
     */
    profileBuilder.prototype.setProfileDescription = function (description) {
        this.properties.profileDescription = description;
        this.update();
    };
    /**
     * Set the location from stats for the basic stats question
     *
     * @param address
     * @param lat
     * @param lng
     */
    profileBuilder.prototype.setBasicStatsLocationFrom = function (address, lat, lng) {
        this.properties.questions.locationFrom = this.properties.questions.locationFrom || null;
        this.properties.questions.locationFrom.address = address;
        this.properties.questions.locationFrom.lat = lat;
        this.properties.questions.locationFrom.lng = lng;
        this.update();
    };
    profileBuilder.prototype.setBasicStatsHeight = function (height) {
        this.properties.questions.height = height;
    };
    /**
     * Set the value of a question
     *
     * requires the question ref and it's value
     *
     * @param questionRef
     * @param value
     */
    profileBuilder.prototype.setQuestionValueByQuestionReference = function (questionRef, value) {
        this.properties.questions[questionRef] = value;
        this.update();
    };
    /**
     * Get the question value by passing the question reference
     *
     * @param questionRef
     * @returns {any}
     */
    profileBuilder.prototype.getQuestionValueByQuestionReference = function (questionRef) {
        return this.properties.questions[questionRef];
    };
    /**
     * Use this for the api
     */
    profileBuilder.prototype.save = function () {
        var url = window.server_path + 'hAPI/account/save';
        this.$http.post(url, {
            profile: this.properties
        }).then(function (resp) {
            console.log('Response', resp);
        }, function (err) {
        });
    };
    //Use this for local storage
    profileBuilder.prototype.update = function () {
        this.localStorage.set('profileBuilderObject', this);
        this.save();
    };
    return profileBuilder;
}());
