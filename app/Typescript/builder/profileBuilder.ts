class profileBuilder {

	protected
		$http = null;

	protected properties = {
		// accountId: <number> null,
		profileDescription: <string> null,
		questions : {
			
			// Basic Stats
			locationFrom: {
				address: <string> null,
				lat: <number> null,
				lng: <number> null
			},
			education: <string> null,
			employment: <string> null,
			religion: <string> null,
			height: <string> null,
			bodyType: <string> null,
			children: <string> null,
			smoking: <string> null,
			drinking: <string> null,
			profileImage : {},
			profileCoverImage : {},
			profileGallery : [],
			profileVideos : [],
			profileFaveVideos : [],
			
			// Interests
			personality : [],
			pastTimes : [],
			books : [],
			music : [],
			viewings : [],
			sports : [],

			//Travel

			travelBeen: [],
			travelGo: [],

			//Partner
			partnerGender : <string> null,
			partnerAgeRange : {
				min: <number> null,
				max: <number> null,
			},
			partnerLocation : {
				name: <string> null,
				lat: <number> null,
				lng: <number> null,
				miles: <number> null
			},
			partnerChildren: <string> null,
			partnerSmoking: <string> null,
			partnerDrinking: <string> null,
			partnerHeight: {
				min: <number> null,
				max: <number> null,
			},
			partnerEducation: <string> null,
			partnerEmployment: <string> null,
			partnerBodyType: <string> null,
			partnerReligion: <string> null,
			partnerPersonality: [],
			partnerPastTimes: [],
		}
	};

	protected localStorage: {} = null;


	constructor(localStorageService, $http, propertiesObj?) {

		this.localStorage = localStorageService;

		this.$http = $http;

		if(propertiesObj != null){
			this.properties = _.merge(this.properties, propertiesObj);
		}

		this.properties.questions = this.properties.questions || null;
	}

	/**
	 * Set the profile description of the object
	 *
	 * @param description
	 */
	setProfileDescription(description: string){
		this.properties.profileDescription = description;

		this.update();
	}

	/**
	 * Set the location from stats for the basic stats question
	 *
	 * @param address
	 * @param lat
	 * @param lng
	 */
	setBasicStatsLocationFrom(address: string, lat: number, lng: number){

		this.properties.questions.locationFrom = this.properties.questions.locationFrom || null;

		this.properties.questions.locationFrom.address = address;
		this.properties.questions.locationFrom.lat = lat;
		this.properties.questions.locationFrom.lng = lng;

		this.update();
	}

	setBasicStatsHeight(height){

		this.properties.questions.height = height;
	}

	/**
	 * Set the value of a question
	 *
	 * requires the question ref and it's value
	 *
	 * @param questionRef
	 * @param value
	 */
	setQuestionValueByQuestionReference(questionRef, value){

		this.properties.questions[questionRef] = value;

		this.update();
	}

	/**
	 * Get the question value by passing the question reference
	 *
	 * @param questionRef
	 * @returns {any}
	 */
	getQuestionValueByQuestionReference(questionRef){

		return this.properties.questions[questionRef];
	}

	/**
	 * Use this for the api
	 */
	save () {

		var url = window.server_path+ 'hAPI/account/save';

		this.$http.post(url, {
			profile: this.properties
		}).then (
			function (resp) {
				console.log('Response', resp);
			},
			function (err) {

			});
	}

	//Use this for local storage
	update(){
		this.localStorage.set('profileBuilderObject', this);
		this.save();
	}

}