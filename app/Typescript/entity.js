/**
 *
 */
var Entity = (function () {
    /**
     *
     */
    function Entity($http, $q, server) {
        this.modified = false; // Flag this to be updated
        this.removed = false; // Flag this to be deleted
        this.created = false;
        this._id = null;
        this._details = {};
        this._original = null;
        this._loaded = false;
        this._server = null; // URL to server
        this._loadPromise = null;
        this._savePromise = null;
        this._createURL = null;
        this._createIdent = null;
        this._saveURL = null;
        this._saveIdent = null;
        this._loadURL = null;
        this._loadIdent = null;
        this._removeURL = null;
        this._removeIdent = null;
        this.$http = $http;
        this.$q = $q;
        this._server = server || Finally.server;
    }
    /**
     *
     */
    Entity.prototype._removeHTTP = function (details) {
        var defer = this.$q.defer();
        var t = this;
        // Validate parameters
        if (!this._removeURL || !this._removeIdent) {
            throw new Error("Remove properties not set.");
        }
        // Create object
        var obj = {};
        obj[this._removeIdent] = details;
        //
        this.$http.post(this._server + this._removeURL, obj).then(function (resp) {
            defer.resolve(resp['data'][t._removeIdent]);
        }, function (err) {
            defer.reject(err);
        });
        return defer.promise;
    };
    /**
     *
     */
    Entity.prototype._loadHTTP = function (details) {
        var defer = this.$q.defer();
        var t = this;
        // Validate parameters
        if (!this._loadURL || !this._loadIdent) {
            throw new Error("Load properties not set.");
        }
        // Create object
        var obj = {};
        obj[this._loadIdent] = details;
        //
        this.$http.post(this._server + this._loadURL, obj).then(function (resp) {
            defer.resolve(resp['data'][t._loadIdent]);
        }, function (err) {
            defer.reject(err);
        });
        return defer.promise;
    };
    /**
     *
     */
    Entity.prototype._saveHTTP = function (details) {
        var defer = this.$q.defer();
        var t = this;
        // Validate parameters
        if (!this._saveURL || !this._saveIdent) {
            throw new Error("Save properties not set.");
        }
        // Create object
        var obj = {};
        obj[this._saveIdent] = details;
        //
        this.$http.post(this._server + this._saveURL, obj).then(function (resp) {
            defer.resolve(resp['data'][t._saveIdent]);
        }, function (err) {
            defer.reject(err);
        });
        return defer.promise;
    };
    /**
     *
     */
    Entity.prototype._createHTTP = function (details) {
        var defer = this.$q.defer();
        var t = this;
        // Validate parameters
        if (!this._createURL || !this._createIdent) {
            throw new Error("Create properties not set.");
        }
        // Create object
        var obj = {};
        obj[this._createIdent] = details;
        //
        this.$http.post(this._server + this._createURL, obj).then(function (resp) {
            defer.resolve(resp['data'][t._createIdent]);
        }, function (err) {
            defer.reject(err);
        });
        return defer.promise;
    };
    /**
     *
     */
    Entity.prototype.getId = function () {
        if (!this._id) {
            throw new Error("Cannot get ID as it has not been set.");
        }
        return this._id;
    };
    /**
     * Post process function
     *
     * Perform any operations on the loaded data
     */
    Entity.prototype._process = function (details) {
        return details;
    };
    /**
     * Load details if they are not already loaded and return
     *
     */
    Entity.prototype._loadDetails = function (nuke) {
        if (nuke === void 0) { nuke = false; }
        var defer = this.$q.defer();
        var t = this;
        // Loading at the mo!
        if (this._loadPromise !== null) {
            return this._loadPromise;
        }
        // Created?
        if (this.created == true) {
            defer.resolve(t._details);
            return defer.promise;
        }
        //
        if (_.isEmpty(this._details) || nuke) {
            // Save promise
            this._loadPromise = defer.promise;
            // Load via HTTP
            this._loadHTTP({ id: this.getId() }).then(function (details) {
                if (!details) {
                    throw new Error("Could not load response. Incorrect?");
                }
                // Run post process
                details = t._process(details);
                // Assign the data to the class module
                _.assign(t._details, details);
                // Create waypoint
                t._waypoint();
                // Resolve details
                defer.resolve(t._details);
                // Reset saved promise
                t._loadPromise = null;
            });
        }
        else {
            defer.resolve(this._details);
        }
        return defer.promise;
    };
    /**
     * Load an existing entity
     */
    Entity.prototype.load = function (id, details) {
        //var defer = this.$q.defer();
        //var t = this;
        if (details === void 0) { details = null; }
        // No ID
        if (!id) {
            throw new Error("No ID set: " + id);
        }
        if (this._loaded) {
            throw new Error("Loading twice!" + id);
        }
        // Mark as loaded
        this._id = id;
        this._loaded = true;
        //
        if (details !== null) {
            // Run post process
            details = this._process(details);
            // Cheeky merge, to persist object references.
            _.assign(this._details, details);
            // Create waypoint
            this._waypoint();
        }
    };
    /**
     * Create a new tag
     */
    Entity.prototype.create = function () {
        console.log('Entity Create!');
        var defer = this.$q.defer();
        if (!_.isEmpty(this._details)) {
            throw new Error("Cannot create new tag from an existing, already loaded, tag.");
        }
        this.created = true;
        return this;
    };
    /**
     * Get details object
     *
     * Returns details. You can request the object instead rather than a promise, useful for angularJS templates.
     */
    Entity.prototype.details = function (noPromise) {
        if (noPromise === void 0) { noPromise = false; }
        // Trigger load
        var promise = this._loadDetails();
        // Return promise if it is requested
        if (!noPromise) {
            return promise;
        }
        else {
            return this._details;
        }
    };
    /**
     * Save changes
     */
    Entity.prototype.save = function () {
        console.log('Entity Save!');
        var defer = this.$q.defer();
        var t = this;
        // Return existing promise if it exists
        if (this._savePromise) {
            console.log('Existing promise - Save in progress!');
            return this._savePromise.promise;
        }
        // Save promise to stop concurrent saves
        this._savePromise = defer;
        // Save new or update
        if (typeof (this._details.id) !== 'undefined') {
            // Update
            this._saveHTTP(this._details).then(function (response) {
                // Mark as created - no longer new.
                _.assign(t._details, response);
                t.created = false;
                t._savePromise = null;
                defer.resolve(response);
            }, function (err) {
                t._savePromise = null;
                defer.reject(err);
            });
        }
        else {
            // Save New
            this._createHTTP(this._details).then(function (response) {
                // Mark as created - no longer new.
                t.created = false;
                t._savePromise = null;
                // Load new
                _.assign(t._details, response);
                // t._details.id = response.id;
                console.log('Created entities!', response, t._details);
                defer.resolve(response);
            }, function (err) {
                t._savePromise = null;
                defer.reject(err);
            });
        }
        return defer.promise;
    };
    /**
     * Reset to waypoint
     */
    Entity.prototype.reset = function () {
        if (_.isEmpty(this._details)) {
            throw new Error("Could not reset to waypoint as details not loaded");
        }
        console.log('Resetting values!');
        _.assign(this._details, this._original);
        this.modified = false;
        return this;
    };
    /**
     * Create a waypoint.
     *
     * If the user cancels a modification action, it will reset it to the point at which this was last called.
     */
    Entity.prototype._waypoint = function () {
        if (_.isEmpty(this._details)) {
            throw new Error("Could not save waypoint as details not loaded");
        }
        this._original = _.cloneDeep(this._details);
        return this;
    };
    /**
     * Remove
     *
     * @param	boolean
     */
    Entity.prototype.remove = function (remove) {
        console.log('Entity Remove!');
        var defer = this.$q.defer();
        var t = this;
        // Return existing promise if it exists
        if (this._savePromise) {
            console.log('Existing promise - Remove in progress!');
            return this._savePromise.promise;
        }
        // Save promise to stop concurrent saves
        this._savePromise = defer;
        // Save new or update
        if (typeof (this._details.id) !== 'undefined') {
            // Update
            this._removeHTTP(this._details).then(function (response) {
                // Mark as created - no longer new.
                t.created = false;
                t.removed = true;
                t._savePromise = null;
                defer.resolve(response);
            }, function (err) {
                t._savePromise = null;
                defer.reject(err);
            });
        }
        else {
        }
        return defer.promise;
    };
    return Entity;
}());
