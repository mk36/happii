
/**
 *
 */
abstract class Entity {

	public modified:boolean = false;		// Flag this to be updated
	public removed:boolean = false;			// Flag this to be deleted
	public created:boolean = false;

	protected _id:number = null;
	protected _details:{} = {};
	protected _original:{} = null;
	protected _loaded:boolean = false;

	protected _server:string = null;        // URL to server

	protected _loadPromise:ng.IPromise = null;
	protected _savePromise:ng.IPromise = null;

	protected $http:ng.IHttpService;
	protected $q:ng.IPromise;

	protected _createURL:string = null;
	protected _createIdent:string = null;

	protected _saveURL:string = null;
	protected _saveIdent:string = null;

	protected _loadURL:string = null;
	protected _loadIdent:string = null;

	protected _removeURL:string = null;
	protected _removeIdent:string = null;

	/**
	 *
	 */
	constructor($http:ng.IHttpService, $q:ng.IPromise, server) {
		this.$http = $http;
		this.$q = $q;
		this._server = server || Finally.server;
	}

	/**
	 *
	 */
	protected _removeHTTP(details:{}):ng.IPromise {

		var defer = this.$q.defer();
		var t = this;

		// Validate parameters
		if (!this._removeURL || !this._removeIdent) {
			throw new Error("Remove properties not set.");
		}

		// Create object
		var obj = {};
		obj[this._removeIdent] = details;

		//
		this.$http.post(this._server + this._removeURL, obj).then(
			function (resp) {
				defer.resolve(resp['data'][t._removeIdent]);
			}, function (err) {
				defer.reject(err);
			});

		return defer.promise;
	}

	/**
	 *
	 */
	protected _loadHTTP(details:{}):ng.IPromise {

		var defer = this.$q.defer();
		var t = this;

		// Validate parameters
		if (!this._loadURL || !this._loadIdent) {
			throw new Error("Load properties not set.");
		}

		// Create object
		var obj = {};
		obj[this._loadIdent] = details;

		//
		this.$http.post(this._server + this._loadURL, obj).then(
			function (resp) {
				defer.resolve(resp['data'][t._loadIdent]);
			}, function (err) {
				defer.reject(err);
			});

		return defer.promise;
	}

	/**
	 *
	 */
	protected _saveHTTP(details:{}):ng.IPromise {

		var defer = this.$q.defer();
		var t = this;

		// Validate parameters
		if (!this._saveURL || !this._saveIdent) {
			throw new Error("Save properties not set.");
		}

		// Create object
		var obj = {};
		obj[this._saveIdent] = details;

		//
		this.$http.post(this._server + this._saveURL, obj).then(
			function (resp) {
				defer.resolve(resp['data'][t._saveIdent]);
			}, function (err) {
				defer.reject(err);
			});

		return defer.promise;
	}

	/**
	 *
	 */
	protected _createHTTP(details:{}):ng.IPromise {

		var defer = this.$q.defer();
		var t = this;

		// Validate parameters
		if (!this._createURL || !this._createIdent) {
			throw new Error("Create properties not set.");
		}

		// Create object
		var obj = {};
		obj[this._createIdent] = details;

		//
		this.$http.post(this._server + this._createURL, obj).then(
			function (resp) {
				defer.resolve(resp['data'][t._createIdent]);
			}, function (err) {
				defer.reject(err);
			});

		return defer.promise;
	}

	/**
	 *
	 */
	public getId() {

		if (!this._id) {
			throw new Error("Cannot get ID as it has not been set.");
		}

		return this._id;
	}

	/**
	 * Post process function
	 *
	 * Perform any operations on the loaded data
	 */
	protected _process (details) {
		return details;
	}

	/**
	 * Load details if they are not already loaded and return
	 *
	 */
	protected _loadDetails(nuke:boolean = false): ng.IPromise<{}> {

		var defer = this.$q.defer();
		var t = this;

		// Loading at the mo!
		if (this._loadPromise !== null) {
			return this._loadPromise;
		}

		// Created?
		if (this.created == true) {
			defer.resolve(t._details);

			return defer.promise;
		}

		//
		if (_.isEmpty(this._details) || nuke) {

			// Save promise
			this._loadPromise = defer.promise;

			// Load via HTTP
			this._loadHTTP({id: this.getId()}).then(
				function (details) {

					if (!details) {
						throw new Error("Could not load response. Incorrect?");
					}

					// Run post process
					details = t._process(details);

					// Assign the data to the class module
					_.assign(t._details, details);

					// Create waypoint
					t._waypoint();

					// Resolve details
					defer.resolve(t._details);

					// Reset saved promise
					t._loadPromise = null;
				}
			);
		} else {
			defer.resolve(this._details);
		}

		return defer.promise;
	}

	/**
	 * Load an existing entity
	 */
	public load(id:number, details:{}=null): void {

		//var defer = this.$q.defer();
		//var t = this;

		// No ID
		if (!id) {
			throw new Error("No ID set: "+ id);
		}

		if (this._loaded) {
			throw new Error("Loading twice!"+ id);
		}

		// Mark as loaded
		this._id = id;
		this._loaded = true;

		//
		if (details !== null) {

			// Run post process
			details = this._process(details);

			// Cheeky merge, to persist object references.
			_.assign(this._details, details);

			// Create waypoint
			this._waypoint();
		}
	}

	/**
	 * Create a new tag
	 */
	public create(): ng.IPromise {

		console.log('Entity Create!');

		var defer = this.$q.defer();

		if (!_.isEmpty(this._details)) {
			throw new Error("Cannot create new tag from an existing, already loaded, tag.");
		}

		this.created = true;

		return this;
	}

	/**
	 * Get details object
	 *
	 * Returns details. You can request the object instead rather than a promise, useful for angularJS templates.
	 */
	public details(noPromise:boolean=false): ng.IPromise<{}> {

		// Trigger load
		var promise = this._loadDetails();

		// Return promise if it is requested
		if (!noPromise) {
			return promise;
		} else {
			return this._details;
		}
	}

	/**
	 * Save changes
	 */
	public save(): ng.IPromise {

		console.log('Entity Save!');

		var defer = this.$q.defer();
		var t = this;

		// Return existing promise if it exists
		if (this._savePromise) {
			console.log('Existing promise - Save in progress!');
			return this._savePromise.promise;
		}

		// Save promise to stop concurrent saves
		this._savePromise = defer;

		// Save new or update
		if (typeof(this._details.id) !== 'undefined') {

			// Update
			this._saveHTTP(this._details).then(
				function (response) {

					// Mark as created - no longer new.
					_.assign(t._details, response);

					t.created = false;
					t._savePromise = null;

					defer.resolve(response);
				},
				function (err) {

					t._savePromise = null;

					defer.reject(err);
				}
			);

		} else {

			// Save New
			this._createHTTP(this._details).then(
				function (response) {

					// Mark as created - no longer new.
					t.created = false;
					t._savePromise = null;

					// Load new
					_.assign(t._details, response);
					// t._details.id = response.id;

					console.log('Created entities!', response, t._details);

					defer.resolve(response);
				},
				function (err) {

					t._savePromise = null;

					defer.reject(err);
				}
			);
		}

		return defer.promise;
	}

	/**
	 * Reset to waypoint
	 */
	public reset () {

		if (_.isEmpty(this._details)) {
			throw new Error("Could not reset to waypoint as details not loaded");
		}

		console.log('Resetting values!');

		_.assign(this._details, this._original);
		this.modified = false;

		return this;
	}

	/**
	 * Create a waypoint.
	 *
	 * If the user cancels a modification action, it will reset it to the point at which this was last called.
	 */
	protected _waypoint () {

		if (_.isEmpty(this._details)) {
			throw new Error("Could not save waypoint as details not loaded");
		}

		this._original = _.cloneDeep(this._details);

		return this;
	}

	/**
	 * Remove
	 *
	 * @param	boolean
	 */
	remove(remove:boolean): ng.IPromise {

		console.log('Entity Remove!');

		var defer = this.$q.defer();
		var t = this;

		// Return existing promise if it exists
		if (this._savePromise) {
			console.log('Existing promise - Remove in progress!');
			return this._savePromise.promise;
		}

		// Save promise to stop concurrent saves
		this._savePromise = defer;

		// Save new or update
		if (typeof(this._details.id) !== 'undefined') {

			// Update
			this._removeHTTP(this._details).then(
				function (response) {

					// Mark as created - no longer new.
					t.created = false;
					t.removed = true;
					t._savePromise = null;

					defer.resolve(response);
				},
				function (err) {

					t._savePromise = null;

					defer.reject(err);
				}
			);

		} else {

			// Doesn't exist on server yet, do nothing!
		}

		return defer.promise;
	}
}