(function () {

	angular.module('app').service('profileService', function ($http, $q, $timeout, localStorageService, profilePartnerQuestionService) {

		var vm = this;

		//An object storing the profile data
		vm.profileObj = {};
		vm.partnerAnswers = [];

		//Clear all when changing the profilebuilder object layout or the passed properties locally won't match the new object changes
		localStorageService.clearAll();

        /**
         * Create a new profile object
		 *
		 * todo change this to the real profile object when the api is working
         */
        vm.createProfileObj = function(){

            if(localStorageService.get('profileBuilderObject') != null){

                var profileBuilderObj = localStorageService.get('profileBuilderObject');
				vm.profileObj = new profileBuilder(localStorageService, $http, profileBuilderObj.properties);

                console.log('getting profile object from local storage', vm.profileObj);
            }
            else{

				vm.profileObj = new profileBuilder(localStorageService, $http, null);
                console.log('create a new profile object');

            }

        };

		/**
		 * Get the profile builder object
         *
         * @returns {{}|*|Array}
         */
        vm.getProfileObj = function(){
            return vm.profileObj;
        };

        vm.createProfileObjFromServer = function (data) {

        	console.log('data passed to create function', data);

			var properties = {
				profileDescription : '',
				questions : {}
			};


        	if(data.data.properties == null){

				properties = {
					profileDescription : data.data.profileDescription,
					questions : data.data.questions
				};
			}
			else{

				properties = {
					profileDescription : data.data.profileDescription,
					questions : data.data.questions
				};
			}

            console.log('properties passed to profile service', properties, 'data sent via param', data.data);


            vm.profileObj = new profileBuilder(localStorageService, $http, properties);


            localStorageService.set('profileBuilderObject', vm.profileObj);


            console.log('profile object created', vm.profileObj);
		};

		/**
		 * Get all pings
		 */
		vm.getAllPings = function(){

			//only used for local json files
			var jsonFile =  'pingSample.json';

			var defer = $q.defer();

			//Local url
			var url = site_rel_path + 'app/json/' + jsonFile;

			//Api url
			//var url = site_rel_path + 'api/question/' + questionRef;

			$http.post(url, {}).
			then(function(data, status, headers, config) {

				console.log('pings received', data);

				defer.resolve(data.data);

			}).
			catch(function(data, status, headers, config) {

				console.log('Failure to get pings', data, status, headers, config);
			});


			return defer.promise;
		};

		/**
		 * Get all similar profiles
		 *
		 * @returns {Promise}
		 */
		vm.getSimilarProfiles = function(){

			//only used for local json files
			var jsonFile =  'similarProfiles.json';

			var defer = $q.defer();

			//Local url
			var url = site_rel_path + 'app/json/' + jsonFile;

			//Api url
			//var url = site_rel_path + 'api/question/' + questionRef;

			$http.post(url, {}).
			then(function(data, status, headers, config) {

				console.log('similar profiles received', data);

				defer.resolve(data.data);

			}).
			catch(function(data, status, headers, config) {

				console.log('Failure to get similar profiles', data, status, headers, config);
			});


			return defer.promise;
		};

		/**
		 * Get the partner icons
		 */
		vm.getPartnerAnswers = function(){

			var defer = $q.defer();

			profilePartnerQuestionService.setUpPartnerAnswers(vm.profileObj.properties.questions).then(function(data){

				defer.resolve(data);
			},
			function(){
				console.log('failed to get partner answers');
				defer.resolve('failed');
			});

			return defer.promise;
		};

		/**
		 * Get the user account details
		 */
		vm.getUserAccountDetails = function () {

			var defer = $q.defer();

			//Api url
			var url = window.server_path + 'hAPI/account/status';

			$http.post(url, {}).
			then(function(data, status, headers, config) {

				console.log('account status received', data);

				defer.resolve(data.data);

			}).
			catch(function(data, status, headers, config) {

				console.log('Failure to get account status', data, status, headers, config);
			});


			return defer.promise;

		};

		/**
		 * Save the builder complete details as complete
		 */
		vm.saveBuilderCompleteDetails = function (builderDetails) {

			var defer = $q.defer();

			//Api url
			var url = window.server_path + 'hAPI/account/complete';

			var data = {

				profile : builderDetails
			};

			console.log('builderDetails', builderDetails);

			$http.post(url, data).
			then(function(data, status, headers, config) {

				console.log('profile builder complete saved', data);

				defer.resolve(data.data);

			}).
			catch(function(data, status, headers, config) {

				console.log('Failure to complete builder question save', data, status, headers, config);
			});


			return defer.promise;

		};

		/**
		 * Get the user account details
		 */
		vm.getProfileDetailsById = function (id) {

			var defer = $q.defer();

			//Api url
			var url = window.server_path + 'hAPI/account/status' ;
			var params = {profile_id: id};

			$http.post(url, params).
			then(function(data, status, headers, config) {

				//console.log('profile status received id: ' + id, data);

				defer.resolve(data);

			}).
			catch(function(data, status, headers, config) {

				console.log('Failure to get profile status id: ', id, data, status, headers, config);
			});


			return defer.promise;

		};



	});
})();