(function () {

	angular.module('app').service('siteService', function ($http, $q, $timeout) {

		var vm = this,
			imageChange = $q.defer();

		vm.backgroundImage = '';

        /**
		 * Set the background image source
		 *
         * @param url
         */
		vm.setBackgroundImage = function(url){

			vm.backgroundImage = url;

			// Added delay to allow everything else to glom on to the imageChange object.
			$timeout(function () {
				imageChange.notify(url);
			}, 20);
		};

        /**
		 * Get the background image promise
		 *
         * @returns Promise
         */
		vm.getBackgroundImageNotify = function(){
			return imageChange.promise;
		};

		/**
		 * Set the site wrapper height
		 * @param value
		 */
		vm.setSiteWrapperHeight = function (value) {

			$('#site-wrapper').css('height', value);
		}

	});
})();