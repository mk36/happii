/**
 *
 * Generates the array containing a combination of all the wanted partner
 * answers, in the correct format needed for displaying them in the icon
 * slider on the profile
 *
 */


(function () {

	angular.module('app').service('profilePartnerQuestionService', function ($http, $q, $timeout, localStorageService) {

		var vm = this;

		//An object storing the profile data
		vm.partnerAnswers = [];


		/**
		 * Get the profile builder object
         *
         * @returns {{}|*|Array}
         */
        vm.getPartnerAnswers = function(){
            return vm.profileObj;
        };

		/**
		 * Set up all of the partner questions to be returned
		 *
		 * @param questions
		 * @returns {Promise}
		 */
		vm.setUpPartnerAnswers = function(questions){

			var defer = $q.defer();

			vm.setPartnerAgeRange(questions.partnerAgeRange);
			vm.setAnswer('body type', questions.partnerBodyType);
			vm.setAnswer('children', questions.partnerChildren);
			vm.setAnswer('religion', questions.partnerReligion);
			vm.setAnswer('drinking', questions.partnerDrinking);
			vm.setAnswer('education', questions.partnerEducation);
			vm.setAnswer('employment', questions.partnerEmployment);
			vm.setAnswer(questions.partnerGender, questions.partnerGender);

			vm.setPartnerHeight(questions.partnerHeight);

			vm.setLocation(questions.partnerLocation);

			vm.setPastTimes(questions.partnerPastTimes);

			vm.setTraits(questions.partnerPersonality);

			defer.resolve(vm.partnerAnswers);


			return defer.promise;

			// //Partner profile answers
			// vm.partnerAnswers.push({title : 'past times', value : questions.partnerPastTimes});
			// vm.partnerAnswers.push({title : 'personality', value : questions.partnerPersonality});
		};

		/**
		 * Set the age range
		 *
		 * @param ageRange
		 */
		vm.setPartnerAgeRange = function(ageRange){

			vm.setAnswer('Age', 'Age ' + ageRange.min + '-' + ageRange.max);
		};

		/**
		 * Set the partner height
		 */
		vm.setPartnerHeight = function(height){

			vm.setAnswer('Height', 'Height Range ' + height.min + '-' + height.max);
		};

		/**
		 * Set the location
		 *
		 * @param location
		 */
		vm.setLocation = function(location){

			vm.setAnswer('Location', 'Within: ' + location.miles + ' Miles');
		};

		/**
		 * Set the past times
		 *
		 * @param pastTimes
		 */
		vm.setPastTimes = function(pastTimes){

			for(var i = 0; i < pastTimes.length; i++){

				vm.setAnswer(pastTimes[i], pastTimes[i]);
			}
		};

		/**
		 * Set the past times
		 *
		 * @param pastTimes
		 */
		vm.setTraits = function(traits){

			for(var i = 0; i < traits.length; i++){

				vm.setAnswer(traits[i], traits[i]);
			}
		};



		/**
		 * Set a answer
		 *
		 * @param title
		 * @param value
		 */
		vm.setAnswer = function(title, value){

			var answer = {'title' : title, 'value' : value};

			if(value != null){
				vm.partnerAnswers.push(answer);
			}

		};



	});
})();