(function () {

	angular.module('app').service('questionService', function ($http, $q, $timeout, localStorageService) {

		var vm = this;


		/**
		 * Get the question data from the api
		 */
		vm.getQuestionData = function(questionRef){

			//only used for local json files
			var jsonFile = questionRef + '.json';

			var defer = $q.defer();

			//Local url
			var url = site_rel_path + 'app/json/' + jsonFile;

			//Api url
			//var url = site_rel_path + 'api/question/' + questionRef;

			$http.post(url, {}).
				then(function(data, status, headers, config) {

					//console.log('question data received', data);

					defer.resolve(data);

			}).
			catch(function(data, status, headers, config) {

					console.log('Failure could not get question data', data, status, headers, config);
			});


			return defer.promise;

		};

		/**
		 * Upload
		 * @param image
		 * @returns {Promise}
		 */
		vm.uploadImageToServer = function(imageObj){

			var defer = $q.defer();

			//Local url
			var url = window.server_path + 'upload/' + imageObj.fileName;


			//Api url
			//var url = window.server_path + 'builder/upload/';

			console.log('url to upload', url, 'object to upload', imageObj);

			$http.post(url, imageObj.form).
			then(function(data, status, headers, config) {

				console.log('uploaded image to server', data);

				defer.resolve(data);

			}).
			catch(function(data, status, headers, config) {

				console.log('Failure to upload image to server', data, status, headers, config);
			});


			return defer.promise;
		};

		/**
		 * Upload
		 * @param videoObj
		 * @returns {Promise}
		 */
		vm.uploadVideoToServer = function(videoObj){

			var defer = $q.defer();

			//Local url
			var url = window.server_path + 'upload/' + videoObj.fileName;


			//Api url
			//var url = window.server_path + 'builder/upload/';

			console.log('url to upload', url, 'object to upload', videoObj);

			$http.post(url, videoObj.form).
			then(function(data, status, headers, config) {

				console.log('uploaded video to server', data);

				defer.resolve(data);

			}).
			catch(function(data, status, headers, config) {

				console.log('Failure to upload video to server', data, status, headers, config);
			});


			return defer.promise;
		};

	});
})();