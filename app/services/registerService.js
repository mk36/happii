(function () {

	angular.module('app').service('registerService', function ($http, $q, $timeout, localStorageService) {

		var vm = this;

		//Clear all when changing the profilebuilder object layout or the passed properties locally won't match the new object changes
		//localStorageService.clearAll();

		//An object storing the new account answers in the register setup
		vm.accountObj = {};

		vm.returnToRegister = false;

		vm.registerReturnPromise = $q.defer();

        /**
         * Create a new account object
         */
        vm.createAccountObj = function(){

            if(localStorageService.get('registerObject') != null){

                var accountObj = localStorageService.get('registerObject');
                vm.accountObj = new register(localStorageService, accountObj.properties);

                console.log('getting object from local storage', vm.accountObj);
            }
            else{
                vm.accountObj = new register(localStorageService, null);
                console.log('create a new account object');
            }

        };

        /**
         * Get the account object
         *
         * @returns {{}|*|Array}
         */
        vm.getAccountObj = function(){
            return vm.accountObj;
        };

		/**
		 * Set the return to register
		 *
		 * @param value
		 */
		vm.setReturnToRegister = function (value) {
			vm.returnToRegister = value;

			vm.registerReturnPromise.notify(value);
		};

        vm.getReturnToRegister = function () {

        	return vm.returnToRegister;
		};

        vm.getRegisterReturnPromise = function () {

        	return vm.registerReturnPromise.promise;
		}

	});
})();