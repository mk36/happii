(function () {

	/**
	 * 
	 */
	angular.module('app').service('alertService', function ($http, $q, $timeout) {

		var vm = this,
			alerts = [];

		/**
		 * Get alerts
		 */
		vm.getAlerts = function () {
			return alerts;
		};

		/**
		 * Trigger a notification
		 *
		 * @param	string		message - Message to be displayed
		 * @param	string		type - Type of notification (notice, warning, error)
		 * @return	defer		Returns a defer object, resolving it will dismiss the error
		 */
		vm.triggerAlert = function (message, type, duration) {

			var defer = $q.defer(),
				alert = {};

			// Create alert
			alert = {
				message: message,
				type: type || 'notice',
				promise: defer,
				dismiss: function () {
					defer.resolve();
				}
			};
			
			// Add to queue
			alerts.push(alert);

			// Remove on completion
			defer.promise.then(function () {
				
				// Remove from array
				alerts.splice(alerts.indexOf(alert), 1);
				
				// TODO: Remove entity itself properly. GC and all that...
			});

			// Remove after duration
			if (duration) {
				$timeout(function () {
					defer.resolve();
				}, duration);
			}

			console.log('alerts', alerts);

			return defer;
		};


	});
})();