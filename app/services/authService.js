(function () {

	angular.module('app').service('authService', function ($http, $q, $timeout, $auth, localStorageService) {

		var vm = this;

		vm.accessTokens = {
			facebook : '',
			instagram : ''
		};

		/**
		 * Get the facebook user photots
		 *
		 * @returns {Promise}
		 */
		vm.getFacebookUserPhotos = function(token){

			var defer = $q.defer();

			FB.login(function(){

				FB.api('/me?fields=id,name, email', function(response) {
					console.log(response);

				});

				FB.api('/me/photos/uploaded', function(response) {

					console.log(response);

					var photos = [];

					for(var i = 0; i < response.data.length; i ++){

						//https://graph.facebook.com/{{photo}}/picture?type=normal&access_token=
						var photo = {name : response.data[i].id + '.png', src : 'https://graph.facebook.com/' + response.data[i].id +'/picture?type=normal&access_token=' + token};
						photos.push(photo);
					}

					defer.resolve(photos);
				});

			});

			return defer.promise;
		};

		/**
		 * Get the instagram user photo
		 *
		 * @returns {Promise}
		 */
		vm.getInstagramUserPhotos = function(token){

			var defer = $q.defer();

			var path = 'https://api.instagram.com/v1/users/self/media/recent?callback=JSON_CALLBACK&access_token=' + token;

			$http.jsonp(path, {}, {}).then(function (response) {

					defer.resolve(response.data.data);
				},
				function (err) {

					defer.reject(err.data.error);
				});


			//defer.resolve(photos);

			return defer.promise;
		};

		/**
		 * Authenticate Instagram
		 *
		 * @param provider
		 */
		vm.authenticateInstagram = function() {

			var defer = $q.defer();

			if(vm.accessTokens.instagram != ''){

				//Resolve the promise
				defer.resolve(vm.accessTokens.instagram);
			}
			else{
				$auth.authenticate('instagram').then(function(data){

					console.log('authentication successful',  data);
					//localStorageService.set('instagram-token', data.access_token);

					//Save the access token
					//vm.accessTokens.instagram = data.access_token;

					//Resolve the promise
					defer.resolve(data);
				},function(data){
					console.log('failed to authorise instagram', data);
				});
			}

			return defer.promise;
		};

		/**
		 * Login to outh
		 *
		 * @param provider
		 * @returns {Promise}
		 */
		vm.outhLogin = function (provider) {

			var defer = $q.defer();

			var url = server_path + 'user/endpoint/' + provider;

			$http.post(url, {}).then(function (response) {

				console.log('outh status ' + provider, response);

				defer.resolve(response.data.url);

			},function (err) {

				defer.reject(err.data);
			});

			return defer.promise;
		};

		/**
		 * User oauth
		 *
		 * @returns {Promise}
		 */
		vm.outhSocialUser = function (provider) {

			var defer = $q.defer();

			var url = server_path + 'user/oauth/' + provider;

			$http.post(url, {}).then(function (response) {

				console.log('outh user' + provider, response);

				defer.resolve(response);

			},function (err) {

				defer.reject('could not get user auth');
			});

			return defer.promise;
		};

		vm.authSpotify = function () {

			var defer = $q.defer();

			var url = 'https://accounts.spotify.com/api/token/';

			var data = {
				grant_type : 'client_credentials'
			};

			var  headers = {
				//'Authorization' : 'Basic ' + (new Buffer('e6116be3533247b793b6647a912fdbf0' + ':' + '389ed128624046c0a8a829e9539327e2').toString('base64'))
				'Authorization': 'Basic ' + 'e6116be3533247b793b6647a912fdbf0' + ':' + '389ed128624046c0a8a829e9539327e2'
			};

			$http.post(url, data, headers).then(function (response) {

				console.log('spotify oauth' + provider, response);

				defer.resolve(response);

			},function (err) {

				defer.reject('could not get spotify oauth');
			});

			return defer.promise;
		};



	});
})();