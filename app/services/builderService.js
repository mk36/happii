(function () {

	angular.module('app').service('builderService', function ($http, $q, $timeout, localStorageService, profileService) {

		var vm = this;

		//An object storing the new profile answers in the profile builder setup
		vm.profileBuilderObj = {};

		vm.activeQuestion = 5;
		vm.activeSubQuestion = 0;

		var activeQuestionPromise = $q.defer();
		var activeSubQuestionPromise = $q.defer();

		//Clear all when changing the profilebuilder object layout or the passed properties locally won't match the new object changes
		//localStorageService.clearAll();

        /**
         * Create a new profile builder object
         */
        vm.createProfileBuilderObj = function(){

			if(localStorageService.get('profileBuilderObject') != null){

                var profileBuilderObj = localStorageService.get('profileBuilderObject');
				vm.profileBuilderObj = new profileBuilder(localStorageService, $http, profileBuilderObj.properties);

                console.log('getting profile builder object from local storage', vm.profileBuilderObj);
            }
            else{

				vm.profileBuilderObj = new profileBuilder(localStorageService, $http,null);
                console.log('create a new profile builder object');
            }

        };

		/**
		 *
		 */
		vm.getActiveQuestionPromise = function () {

			return activeQuestionPromise.promise;
		};

        /**
         *
         */
        vm.getActiveSubQuestionPromise = function () {

            return activeSubQuestionPromise.promise;
        };

		/**
		 * Get the profile builder object
         *
         * @returns {{}|*|Array}
         */
        vm.getProfileBuilderObj = function(){

            return vm.profileBuilderObj;
        };

		/**
		 * Set the active questions
		 *
		 * @param number
		 */
		vm.setActiveQuestion = function(number){

        	vm.activeQuestion = number;

        	$timeout(function () {
        		activeQuestionPromise.notify(vm.activeQuestion);
			}, 100);
		};

		/**
		 * Get the active question
		 *
		 * @returns {number|*}
		 */
        vm.getActiveQuestion = function(){
        	return vm.activeQuestion;
		};

		/**
		 * Set the active sub question
		 *
		 * @param number
		 */
		vm.setActiveSubQuestion = function(number){

			vm.activeSubQuestion = number;

			$timeout(function () {
				activeSubQuestionPromise.notify(vm.activeSubQuestion);
			}, 100);
		};

		/**
		 * Get the active sub question
		 *
		 * @returns {*|number}
		 */
        vm.getActiveSubQuestion = function(){

        	return vm.activeSubQuestion;
		};

		vm.createProfileBuilderObj();

	});
})();