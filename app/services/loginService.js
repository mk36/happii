(function () {

			angular.module('app').service('loginService', function ($http, $q, $timeout) {

				var vm = this,
					userStatus = null,
					listenQ = $q.defer();

				/**
				 * Listen for a user status change
				 *
				 * @param	boolean		trigger - Cause the system to check on the user and emit any new found status.
				 * @return	promise		Returns a promise. Use the notify property.
				 */
				vm.statusMonitor = function (trigger) {

					// True/False
					trigger = !!trigger || false;

					// Force a check
					if (trigger) {
						vm.status().then(function (user){

							// Return user
							listenQ.notify(user);
						}, function () {

							// No user
							listenQ.notify();
						});
					}

					return listenQ.promise;
				};

				/**
				 * Get user status
				 *
				 * @param	boolean		refresh - Force a refresh
				 */
				vm.status = function (refresh) {

					var defer = $q.defer();

					// True/False
					refresh = !!refresh || false;

					if (userStatus === null || refresh)
					{
						// Go off and do a real check
						isLoggedIn().then(
							function (user) {
								// In
								userStatus = {
									user: user,
									err: false
								};

								defer.resolve(userStatus.user);

								// Trigger status monitor
								vm.statusMonitor(true);
							},
							function (err) {
								// Out
								userStatus = {
									user: false,
									error: err
								};

								defer.reject(userStatus.error);

								// Trigger status monitor
								vm.statusMonitor(true);
							});
					} else {

						// Use internal
						if (userStatus.user) {

							defer.resolve(userStatus.user);

						} else {

							defer.reject(userStatus.error);
						}
					}

					return defer.promise;

				};

				/**
				 * Log In User
				 */
				vm.logIn = function (username, password) {

					var defer = $q.defer(),
						path = SITE_REL_PATH + 'auth/log-in';

					$http.post(path, {
						email: username,
						password: password
					}).then(
						function (response) {

							// Update status object
							userStatus = {
								user: response.data.user,
								error: null
							};

							console.log('user status returned', response);

							defer.resolve(response.data.user);

							// Trigger status monitor
							vm.statusMonitor(true);
						},
						function (err) {

							// Update status object
							userStatus = {
								user: null,
								error: err
							};

							defer.reject(err.data.error);

							// Trigger status monitor
							vm.statusMonitor(true);
						});

					return defer.promise;
				};

				/**
				 * Register New User
				 */
				vm.register = function (username, password) {

					var defer = $q.defer(),
						path = SITE_ADMIN_PATH + 'auth/register';

					$http.post(path, {
						email: username,
						password: password
					}).then(
						function (response) {

							// Update status object
							userStatus = {
								user: response.data.user,
								error: null
							};

							console.log('Register Reponse', response);

							defer.resolve(response.data.user);
						},
						function (err) {

							defer.reject(err.data.error);
						});

					return defer.promise;
				};

				/**
				 * Request password reset
				 */
				vm.request = function (email) {

					var defer = $q.defer(),
						path = SITE_REL_PATH + 'auth/request';

					$http.post(path, {
						email: email
					}).then(
						function (response) {

							// Update status object
							userStatus = {
								user: response.data.user,
								error: null
							};

							console.log('Register Reponse', response);

							defer.resolve(response.data.user);
						},
						function (err) {

							defer.reject(err.data.error);
						});

					return defer.promise;
				};

				/**
				 * Recover Password New User
				 */
				vm.recover = function (email, code, password) {

					var defer = $q.defer(),
						path = SITE_REL_PATH + 'auth/recover';

					console.log('Recovering', email, code, password);

					$http.post(path, {
						email: email,
						code: code,
						password: password
					}).then(
						function (response) {

							// Update status object
							userStatus = {
								user: response.data.user,
								error: null
							};

							console.log('Register Reponse', response);

							defer.resolve(response.data.user);
						},
						function (err) {

							defer.reject(err.data.error);
						});

					return defer.promise;
				};

				/**
				 * Activate New User
				 */
				vm.activate = function (email, code) {

					var defer = $q.defer(),
						path = SITE_REL_PATH + 'auth/activate';

					var data = {
                        email: email,
                        code: code
					}

					$http.post(path, data).then(
						function (response) {

							defer.resolve(response.data.user);

							// Trigger status check
							vm.status(true);
						},
						function (err) {

							defer.reject(err.data.error);
						});

					return defer.promise;
				};

				/**
				 * Log Out User
				 */
				vm.logOut = function () {

					var defer = $q.defer(),
						path = SITE_REL_PATH + 'auth/log-out';

					$http.post(path, {}).then(
						function (response) {

							// Update status object
							userStatus = {
								user: null,
								error: null
							};

							defer.resolve();

							// Trigger status monitor
							vm.statusMonitor(true);
						},
						function (err) {

							// Update status object
							userStatus = {
								user: null,
								error: null
							};

							defer.reject();

							// Trigger status monitor
							vm.statusMonitor(true);
						});

					return defer.promise;
				}

				/**
				 * Is User Logged In
				 */
				function isLoggedIn () {

					var defer = $q.defer(),
						path = SITE_REL_PATH + 'auth/status';

					$http.post(path, {}).then(
						function (response) {
							defer.resolve(response.data.user);
						},
						function (err) {
							defer.reject(err.data.error);
						});

					return defer.promise;
				}
			});
		})();