(function () {

	angular
		.module('app')
		.controller('registerGenderController', registerGenderControllerSetUp);

	function registerGenderControllerSetUp($scope, $state, $stateParams, $timeout, siteService, registerService, builderService){

		var vm = this;

        vm.selectedGender = {};

        /**
		 * Set up the register controller
         */
		vm.setUpRegisterGenderController = function(){

			builderService.setActiveSubQuestion(1);

			console.log('register gender controller setup!');

			//Update the gender object and the local storage object
			var accountObj = registerService.getAccountObj();

			vm.selectedGender = accountObj.properties.gender;

		};

        /**
         * Set the selected gender
         * @param gender
         */
        vm.setSelectedGender = function(gender){

            //Set the gender locally
            vm.selectedGender = gender;

			// BG Image
			if (gender === "male") {
				//siteService.setBackgroundImage('images/backgrounds/animals/lion.jpg')
			} else {
				//siteService.setBackgroundImage('images/backgrounds/animals/lioness.jpg')
			}

            //Update the gender object and the local storage object
            var accountObj = registerService.getAccountObj();
            accountObj.setGender(gender);
        };



	}
})();

