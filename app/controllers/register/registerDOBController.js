(function () {

	angular
		.module('app')
		.controller('registerDOBController', registerDOBControllerSetUp);

	function registerDOBControllerSetUp($scope, $state, $stateParams, $timeout, siteService, registerService, builderService){

		var vm = this;


        vm.days = [];
        vm.months = [];
        vm.years = [];

        vm.selectedDay = 0;
        vm.selectedMonth = 0;
        vm.selectedYear = 0;

		console.log('register dob controller setup');

        /**
		 * Set up the register controller
         */
		vm.setUpRegisterDOBController = function(){

			//Set the background image
           // siteService.setBackgroundImageSrc('url(images/backgrounds/animals/puffins.png)');
			vm.setUpDateInputBoxes();

			//Set the active sub question
			builderService.setActiveSubQuestion(2);

			//Update the gender object and the local storage object
			var accountObj = registerService.getAccountObj();

			//todo repopulate the day field (break up stored date)
			console.log('account object', accountObj);
			
			vm.repopulateDOB(accountObj.properties.dob);
		};

        /**
		 * Set up the input boxes for all of the date of birth options
         */
		vm.setUpDateInputBoxes = function(){

            vm.setUpDays();
            vm.setUpMonths();
            vm.setUpYears();

            console.log('days', vm.days, 'months', vm.months, 'years', vm.years);
		};

        /**
		 * Set up the days
         */
		vm.setUpDays = function(){

			vm.days = _.range(1, 32);
		};

        /**
		 * Set up months
         */
		vm.setUpMonths = function(){

			vm.months = [{name : 'January', value : 1},{name : 'February', value : 2},{name : 'March', value : 3},
				{name : 'April', value : 4},{name : 'May', value : 5},{name : 'June', value : 6},
				{name : 'July', value : 7},{name : 'August', value : 8},{name : 'September', value : 9},
				{name : 'October', value : 10},{name : 'November', value : 11},{name : 'December', value : 12}
			];

			//vm.months = _.range(1, 13);
		};

        /**
		 * Set up years
         */
        vm.setUpYears = function(){

        	var date = new Date();
        	var year = date.getUTCFullYear();

        	year = year - 18;

            vm.years = _.range(1900, year);
            vm.years.reverse();
		};

        /**
		 * Set the selected date of birth
         */
        vm.setSelectedDob = function(){

            //Update the Dob object
			var accountObj = registerService.getAccountObj();

			accountObj.setDob(vm.selectedDay.toString(), vm.selectedMonth.value.toString(), vm.selectedYear.toString());

			console.log('account obj', accountObj);

			//Go to the location state
            $state.transitionTo('register.location');

		};

		/**
		 * Repopulate the dob answers
		 *
		 * @param fullDob
		 */
		vm.repopulateDOB = function (fullDob) {

			console.log('full dob', fullDob);

			//If the dob has been set
			if(fullDob.length > 0){

				var split = fullDob.split('-');
				console.log('split', split);

				vm.selectedDay = parseInt(split[2]);
				vm.selectedMonth = vm.getMonthObjByNumber(split[1]);
				vm.selectedYear = parseInt(split[0]);
			}
		};

		/**
		 * Get month by number
		 *
		 * @param number
		 * @returns {*}
		 */
		vm.getMonthObjByNumber = function (number) {

			for(var i = 0; i < vm.months.length; i++){

				if(vm.months[i].value == number){
					return vm.months[i];
				}
			}

			return false;
		};


	}
})();

