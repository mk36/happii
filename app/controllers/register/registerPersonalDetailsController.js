/**
 *
 * todo Validate email, name, phone number fields
 *
 */


(function () {

	angular
		.module('app')
		.controller('registerPersonalDetailsController', registerPersonalDetailsControllerSetUp);

	function registerPersonalDetailsControllerSetUp($scope, $state, $stateParams, $timeout, siteService, registerService, builderService, profileService){

		var vm = this;

		vm.firstName = '';
		vm.surname = '';
		vm.email = '';
		vm.mobileNo = '';
		vm.password = '';
		vm.passwordRetype = '';
		vm.termsAccepted = false;

		vm.accountObj = {};

		//error messages
		vm.showEmailError = false;
		vm.showPasswordError = false;
		vm.showPasswordRetypeError = false;

		console.log('register personal details controller setup');

		vm.oauth = false;

		$scope.$watch('vm.oauth', function(value, old) {}, true);

        /**
		 * Set up the register controller
         */
		vm.setUpRegisterPersonalDetailsController = function(){

			//Set the active sub question
			builderService.setActiveSubQuestion(4);

            $('#register-password').pwstrength({
                ui: { showVerdictsInsideProgressBar: true }
            });

			vm.accountObj = registerService.getAccountObj();
			console.log('accoutn object', vm.accountObj);

			vm.preFillPersonalDetails();

			vm.setUpValidationMessageConditions();

			profileService.getUserAccountDetails().then(function (data) {

				console.log('account data received', data);

				if(data.user.oauth == true){

					vm.oauth = true;
					console.log('oath value', vm.oauth);
				}

			});
		};

		/**
		 * Set up all the validation messages for
		 * the inputs
		 */
		vm.setUpValidationMessageConditions = function () {

			//Email
			$(".form-input-email").on("blur",  function() {
				console.log('email has been left valid: ', vm.checkEmailIsValid());

				if(vm.checkEmailIsValid() == false){
					vm.showEmailError = true;
				}
				else{
					vm.showEmailError = false;
				}

				$scope.$apply();

				console.log('show email error', vm.showEmailError);
			});

			//Password
			$("#register-password").on("blur",  function() {
				console.log('password has been left valid: ', vm.checkPasswordConditions());

				if(vm.checkPasswordConditions()== false){
					vm.showPasswordError = true;
				}
				else{
					vm.showPasswordError = false;
				}

				$scope.$apply();

				console.log('show password error', vm.showPasswordError);
			});

			//Password retype
			$("#register-password-retype").on("blur",  function() {
				console.log('password retype');

				if(vm.password != vm.passwordRetype){
					vm.showPasswordRetypeError = true;
				}
				else{
					vm.showPasswordRetypeError = false;
				}

				$scope.$apply();

				console.log('show password retype error', vm.showPasswordRetypeError);
			});
		};

		/**
		 *	Enter the prefilled values if they have already been set
		 */
		vm.preFillPersonalDetails = function () {

			vm.preFillProperty('firstName',vm.accountObj.properties.firstName);
			vm.preFillProperty('surname',vm.accountObj.properties.surname);
			vm.preFillProperty('email',vm.accountObj.properties.email);
			vm.preFillProperty('mobileNo',vm.accountObj.properties.mobile);
			vm.preFillProperty('password',vm.accountObj.properties.password);
			vm.preFillProperty('passwordRetype',vm.accountObj.properties.password);
		};

		/**
		 * Prefill a property if it isn't null
		 *
		 * @param propery
		 * @param value
		 */
		vm.preFillProperty = function(property, value){

			if(value != null){
				vm[property] = value;
			}

			//console.log('property', property);
		};


        /**
		 * Check all of the personal details have been filled out
		 *
         * @returns {boolean}
         */
		vm.checkAllPersonalDetailsAreFilled = function(){

			if(vm.oauth == false){

				if(vm.firstName != '' && vm.surname != '' && vm.email != '' &&
					vm.mobileNo != '' && vm.checkPasswordConditions() == true && vm.termsAccepted == true && vm.checkEmailIsValid() == true){

					return true;
				}
			}
			else{

				if(vm.firstName != '' && vm.surname != '' && vm.email != '' &&
					vm.mobileNo != '' && vm.termsAccepted == true && vm.checkEmailIsValid() == true){

					return true;
				}
			}


			return false;
		};

        /**
		 * Check if the password matches the needed conditions
		 *
		 * returns true if the password is ok
		 *
         * @returns {boolean}
         */
		vm.checkPasswordConditions = function(){

			var numberCheck = vm.checkPasswordContainsNumber(vm.password);
			var lengthCheck = vm.checkPasswordValidLength(vm.password);
			var uppercaseCheck =vm.checkPasswordContainsUppercaseCharacter(vm.password);

			console.log('password word', vm.password, 'number', numberCheck, 'character length', lengthCheck, 'contains uppercase', uppercaseCheck);

			if(vm.password != vm.passwordRetype){

				vm.showPasswordRetypeError = true;

				return false
			}
			else{
				vm.showPasswordRetypeError = false;
			}

			if(numberCheck == true && lengthCheck == true && uppercaseCheck == true && vm.passwordRetype != ''){

				vm.showPasswordError = false;

				return true;
			}

			vm.showPasswordError = true;

			return false;
		};

        /**
		 * Check if the email is valid
         * @returns {boolean}
         */
		vm.checkEmailIsValid = function(){

			var pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

            return pattern.test(vm.email);

		};

		/**
		 * Check password contains a number
		 *
		 * @param password
		 * @returns {boolean}
		 */
		vm.checkPasswordContainsNumber = function(password){

			var parts = password.split("");

			for(var i = 0; i < parts.length; i++){

				if(parseInt(parts[i]).toString() != 'NaN'){
					return true;
				}
			}

			return false;
		};

		/**
		 * Check the password is a valid length
		 *
		 * @param password
		 * @returns {boolean}
		 */
		vm.checkPasswordValidLength = function (password) {

			var minLength = 6;

			if(password.length <= minLength){
				return false;
			}

			return true;
		};

		/**
		 * Check the password contains at least one uppercase character
		 *
		 * @param password
		 * @returns {boolean}
		 */
		vm.checkPasswordContainsUppercaseCharacter = function (password) {

			var pattern = /[A-Z]+/;
			return pattern.test(password);
		};

		/**
		 * Save the personal details to the register object
		 */
		vm.savePersonalDetails = function(){

            //Update the personal detail object
			var accountObj = registerService.getAccountObj();
			accountObj.setPersonalDetails(vm.firstName, vm.surname, vm.email, vm.mobileNo, vm.password);

			console.log('account object', accountObj);

			//Send email to api

            //Go to the complete state state
            $state.transitionTo('register.complete');
		};

		vm.finishRegisterProcess = function () {

			if(vm.checkAllPersonalDetailsAreFilled() == true){
				vm.savePersonalDetails();
			}

		};



	}
})();

