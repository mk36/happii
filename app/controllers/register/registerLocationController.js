(function () {

	angular
		.module('app')
		.controller('registerLocationController', registerLocationControllerSetUp);

	function registerLocationControllerSetUp($scope, $state, $stateParams, $timeout, $http, $q, siteService, registerService, builderService){

		var vm = this;

		console.log('register location controller setup');

		vm.postcode = '';
		vm.validAddress = false;
		vm.locationObj = {
			address : '',
			lat: 0,
			lng: 0
		};

		vm.locationError = '';

        /**
		 * Set up the register controller
         */
		vm.setUpRegisterLocationController = function(){

			//Set the active sub question
			builderService.setActiveSubQuestion(3);

            var input = document.getElementById('register-location-search');

			//todo remove options for live version without london postcode limit
			//var londonBounds = new google.maps.LatLngBounds(new google.maps.LatLng(51.332757,-0.475159),new google.maps.LatLng(51.680219,0.191574));

			//51.5076° N, 0.1278° W central london

			var options = {
				//bounds: londonBounds,
				types:['geocode'],
				region: 'uk'
			};

            var searchBox = new google.maps.places.Autocomplete(input, options);

			google.maps.event.addListener(searchBox, 'place_changed',function(){
				//console.log('hello I have changed', searchBox);

				var place = searchBox.getPlace();
				var latLng = place.geometry.location;

				//console.log('place object google', place.address_components);

				//var londonCntLat = 51.5076;
				//var londonCntLng = 0.1278;

				//var distanceKM = vm.getDistanceFromLatLonInKm(londonCntLat, londonCntLng, latLng.lat(), latLng.lng());
				//var miles = distanceKM *0.62137;
				//console.log('distance miles', miles);

				// if(miles <= 25){
				// 	vm.setLocationObject(place.formatted_address, latLng.lat(), latLng.lng());
				// 	vm.locationError = '';
				// }
				// else{
				// 	vm.locationError = 'Location must be within London, please change';
				// 	vm.validAddress = false;
				// 	console.log('location is not within london');
				// }

				var placeCounty = vm.getPlaceCountyAndCity(place.address_components);

				if(placeCounty.county.length > 0){
					vm.setLocationObject(place.formatted_address, latLng.lat(), latLng.lng(), placeCounty.county, placeCounty.city);
					vm.locationError = '';
				}
				else{
					vm.validAddress = false;
					vm.locationError = 'Please add an address with a valid county';
				}

				$scope.$apply();
			});

			//Repopulate existign location data
			vm.repopulateLocationData();

		};

		/**
		 * Get the google place county and city
		 *
		 * @param addressParts
		 * @returns {{city: string, county: string}}
		 */
		vm.getPlaceCountyAndCity = function (addressParts) {

			console.log('address parts', addressParts);

			var addressObj = {
				city : '',
				county : ''
			};


			for(var i = 0; i < addressParts.length; i++){

				var types = addressParts[i].types;

				for(var j = 0; j < types.length; j++){

					if(types[j] == 'administrative_area_level_2'){

						addressObj.county = addressParts[i].short_name;
					}

					if(types[j] == 'locality'){
						addressObj.city = addressParts[i].short_name;
					}
				}

			}

			return addressObj;
		};

		/**
		 * Get a distance (in km) between two latlngs
		 *
		 * @param lat1
		 * @param lon1
		 * @param lat2
		 * @param lon2
		 * @returns {number}
		 */
		vm.getDistanceFromLatLonInKm = function(lat1,lon1,lat2,lon2) {
			var R = 6371; // Radius of the earth in km
			var dLat = vm.deg2rad(lat2-lat1);  // deg2rad below
			var dLon = vm.deg2rad(lon2-lon1);
			var a =
					Math.sin(dLat/2) * Math.sin(dLat/2) +
					Math.cos(vm.deg2rad(lat1)) * Math.cos(vm.deg2rad(lat2)) *
					Math.sin(dLon/2) * Math.sin(dLon/2)
				;
			var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
			var d = R * c; // Distance in km
			return d;
		};

		vm.deg2rad = function(deg) {
			return deg * (Math.PI/180)
		};

        /**
		 * Set the location object
		 *
         * @param address
         * @param lat
         * @param lng
         */
		vm.setLocationObject = function(address, lat, lng, county, city){

            vm.locationObj.address = address;
            vm.locationObj.lat = lat;
            vm.locationObj.lng = lng;
            vm.locationObj.county = county;
            vm.locationObj.city = city;

			//Update the location object
			var accountObj = registerService.getAccountObj();
			accountObj.setLocation(address, lat, lng, county, city);

			//The address is valid
            vm.validAddress = true;

			console.log('account obj', accountObj);

			//Refresh the scope
			$scope.$apply();
		};

		/**
		 * Repopulate the location data
		 *
		 */
		vm.repopulateLocationData = function () {

			//Update the gender object and the local storage object
			var accountObj = registerService.getAccountObj();

			//Repopulate the postcode stored
			vm.postcode = accountObj.properties.location.address;

			if(vm.postcode.length > 0){
				vm.locationObj = accountObj.properties.location;
				vm.validAddress = true;
			}

		};


	}
})();

