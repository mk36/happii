(function () {

	angular
		.module('app')
		.controller('registerCompleteController', registerCompleteControllerSetUp);

	function registerCompleteControllerSetUp($scope, $state, $stateParams, $q, $http, $timeout, siteService, registerService, builderService, profileService){

		var vm = this;

		//can be complete, processing or error
		vm.completeStatus = 'processing';

		vm.errorMessage = '';
		vm.detailsToFix = [];

        /**
		 * Set up the register controller
         */
		vm.setUpRegisterCompleteController = function(){

			//Set the active sub question
			builderService.setActiveSubQuestion(5);

            //Create a new account object
            var registerObject = registerService.getAccountObj();
			console.log('set up complete controller', registerObject);

			//Reset details to fix
			vm.detailsToFix = [];

			//Save the register account object
			registerObject.save($q, $http).then(function(data){

				console.log('success, saved register object', data);


				console.log('complete status', vm.completeStatus);

				profileService.getUserAccountDetails().then(function (data) {

					console.log('account data received', data);

					if(data.user.oauth == true){

						vm.completeStatus = 'oauth';
					}
					else{
						vm.completeStatus = 'complete';
					}

				});




			}).catch(function(data){

				console.log('failed to save register object', data);

				vm.completeStatus = 'error';

				console.log('server path', window.server_path);

				vm.manageRegisterErrors(data.data.error);

				registerService.setReturnToRegister(true);
			});

		};

		/**
		 * Manage register errors returned
		 *
		 * @param errorObj
		 */
		vm.manageRegisterErrors = function (errorObj) {

			if(errorObj.code == 'SCHEMA_PARSE_ERROR'){
				vm.manageSchemaError(errorObj.message);
			}

			if(errorObj.code == 'EMAIL_ADDRESS_TAKEN'){
				vm.addDetailsToFix('email');
			}


		};

		vm.manageSchemaError = function (schemaErrors) {

			for(var i = 0; i < schemaErrors.length; i++){

				vm.addDetailsToFix(schemaErrors[i].property);
			}

			console.log('register error occuried', vm.detailsToFix);
		};

		/**
		 * Add a detail to fix
		 *
		 * @param detail
		 */
		vm.addDetailsToFix = function (detail) {

			var detailObj = vm.createDetailsObj(detail);

			if(vm.checkErrorAlreadyAdded(detailObj) == false){
				vm.detailsToFix.push(detailObj);
			}


		};

		/**
		 * Create a detail object
		 *
		 * @param propery
		 */
		vm.createDetailsObj = function (property) {

			var detailObj = {
				name : property,
				sref : 'register.personal-details'
			};

			if(property == 'location.address' || property == 'location.lat' || property == 'location.lng'){

				detailObj = {
					name : 'location',
					sref : 'register.location'
				}
			}

			if(property == 'gender'){

				detailObj = {
					name : 'gender',
					sref : 'register.gender'
				}
			}

			if(property == 'dob'){

				detailObj = {
					name : 'dob',
					sref : 'register.dob'
				}
			}

			console.log('property passed', property);

			return detailObj;
		};

		/**
		 * Check error already added to the error list
		 *
		 * @param property
		 * @returns {boolean}
		 */
		vm.checkErrorAlreadyAdded = function (property) {

			for(var i = 0; i < vm.detailsToFix.length; i++){

				if(vm.detailsToFix[i].name == property.name){
					return true;
				}
			}

			return false;
		};

		// /**
		//  * Close the page
		//  */
		// vm.closePage = function () {
		//
		// 	window.open('','_parent','');
		// 	window.close();
		// };

		vm.setUpRegisterCompleteController();

	}
})();

