(function () {

	angular
		.module('app')
		.controller('registerController', registerControllerSetUp);

	function registerControllerSetUp($scope, $state, $stateParams, $timeout, siteService, registerService, builderService){

		var vm = this;

		console.log('register controller setup');

		vm.registerSubQuestionDots = [];
		vm.activeSubQuestion = 0;

		vm.returnToRegister = registerService.getReturnToRegister();

		// Get promise listener
		var promise = registerService.getRegisterReturnPromise();

		var activeSubQuestionPromise = builderService.getActiveSubQuestionPromise();

		activeSubQuestionPromise.then(null, null, function (number) {
			vm.activeSubQuestion = number;
		});

		// Handle change of image
		promise.then(null, null, function (value) {

			vm.returnToRegister = value;
			//console.log('return to register value changed!!');
		});

        /**
		 * Set up the register controller
         */
		vm.setUpRegisterController = function(){

			//Set the background image
            siteService.setBackgroundImage('images/backgrounds/register.jpg');

            //Create a new account object
            registerService.createAccountObj();

            //Setup builder dots
			vm.setUpBuilderSubDotsArray();



		};

		vm.setUpBuilderSubDotsArray = function(){

			vm.registerSubQuestionDots = _.range(1, 6);
			//builderService.setActiveSubQuestion(3);

			$timeout(function(){

				vm.activeSubQuestion = builderService.getActiveSubQuestion();
			}, 100);


			console.log('dots', vm.registerSubQuestionDots, 'active sub question', vm.activeSubQuestion);

		};

		vm.setUpRegisterController();

	}
})();

