(function () {

	angular
		.module('app')
		.controller('homeController', homeControllerSetUp);

	function homeControllerSetUp($scope, $state, $stateParams, $timeout, siteService){

		var vm = this;

		vm.deviceType = 'desktop';

		/**
		 * Watch for changes on the show variable
		 */
		$scope.$watch('vm.deviceType', function(value, old) {

		}, true);

		vm.setUpHomePage = function(){

			siteService.setSiteWrapperHeight('auto');
			vm.setDeviceType();
		};

		/**
		 * Set whether the device is mobile, desktop or tablet
		 * as it affects the animation flow
		 *
		 */
		vm.setDeviceType = function(){

			if(window.innerWidth <  768 ){

				vm.deviceType = 'mobile';
			}
			else{
				vm.deviceType = 'desktop';
			}

			console.log('device type', vm.deviceType);
		};


		vm.setUpHomePage();
	}
})();

