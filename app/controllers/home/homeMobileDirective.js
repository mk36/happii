(function () {

    angular
        .module('app')
        .directive("homeMobile", homeMobileSetUp);


    function homeMobileSetUp($timeout){


        var directive = {

            templateUrl : 'app/views/home/device/mobile.html',

            controller: homeMobileController,

            controllerAs: 'vm',
            bindToController: true,

            scope: {

            }
        };

        return directive;



        /**
         *	Set up controller
         */
        function homeMobileController ($scope, $state, questionService, builderService) {

            var vm = this;

			vm.cardArray = [];
			vm.pageSetUp = false;

			vm.menuTop = false;

			vm.setUpHomePage = function(){

				console.log('home mobile controller setup');
				if($.fn.fullpage.destroy){

					$.fn.fullpage.destroy('all');
					console.log('destroyed full page js');
				}

				$timeout(function(){

					$('#fullpage').fullpage({

						anchors: ['firstPage', 'secondPage', 'thirdPage', 'fourthPage', 'lastPage'],
						menu: '#myMenu',
						controlArrows: false,
						paddingTop: '50px',
						normalScrollElements: '#recentTweets',
						responsiveSlides: true,
						responsiveWidth: 764,
						responsiveHeight: 700,

						
						afterLoad: function(anchorLink, index){

							vm.removeActiveMenuClass();

							if(vm.pageSetUp == false){
								vm.pageSetUp = true;
								vm.manageSlideFragmentNavigation();
							}

							vm.setSlideFragment(anchorLink + index);

							if(index >= 1){
								//$('.home-header').toggleClass('appear', $(document).scrollTop() > 0);
								$('.home-header').removeClass('appear');
								vm.menuTop = true;
							}

							if(index >= 2){

								$('.home-header').addClass('appear');
								vm.menuTop = false;
							}


							// Adding classes to slides
							if(index == 1){
								$('#menu-item-1').addClass('activeMenuItem');
								$('.back-to-top-arrow').css('visibility', 'hidden');
							}
							else{
								$('.back-to-top-arrow').css('visibility', 'visible');
							}

							if(index == 2){
								$('#menu-item-2').addClass('activeMenuItem');
							}


							if(index == 3){
								$('#menu-item-3').addClass('activeMenuItem');
							}

							if(index == 4){
								$('#menu-item-4').addClass('activeMenuItem');
							}
						},
						afterSlideLoad: function( anchorLink, index, slideAnchor, slideIndex){
							//Set the fragment in the url
							vm.setSlideFragment(anchorLink + index);
						},
						afterResponsive: function(isResponsive){

							console.log('is responsive');
							$('.fp-auto-height-responsive').css('padding-top', '15px').css('padding-bottom', '15px');
						}
					});


					$(document).on('click', '#moveDown', function(){
						$.fn.fullpage.moveSectionDown();
					});

					$(document).on('click', '#moveRight', function(){
						$.fn.fullpage.moveSlideRight();
					});

					$(document).on('click', '#moveLeft', function(){
						$.fn.fullpage.moveSlideLeft();
					});

					vm.setUpFlipCards();

					$(document).ready(function(){

						$('.slider').slick({
							infinite: true,
							dots: true,
							slidesToShow: 1,
							slidesToScroll: 1,
							arrows: false,
							autoplay: false,
							autoplaySpeed: 5000,
							adaptiveHeight: true,
							mobileFirst: true

						});

					});

					$(document).ready(function () {
						if (window.innerWidth <= 764){
							//$.fn.fullpage.destroy('all');
						}

						$.fn.fullpage.setAutoScrolling(false);
					});


					$(document).ready(function () {
						AOS.init();
					});

					$('.home-page').css('visibility', 'visible');

				}, 100);

				$timeout(function () {

					twttr.widgets.createTimeline(
						{
							sourceType: "profile",
							screenName: "happiidating"
						},
						document.getElementById("recentTweetsMob")
					);

					console.log('set up twitter plugin');
				}, 1000);

				//Set up the cards
				vm.setUpCardArray();

				//Set up the share plugin
				vm.setUpSharePlugin();


			};

			vm.viewMoreTweets = function () {

				vm.setUpTwitter(10);

				$('.view-more-tweets-wrapper').css('display', 'none');

			};

			vm.setUpTwitter = function (noOfTweets) {

				var configProfile = {
					"profile": {
						"screenName": 'HappiiDating'
					},
					"domId": 'recentTweets',
					"maxTweets": noOfTweets,
					"enableLinks": true,
					"showUser": true,
					"showTime": true,
					"showImages": false,
					"lang": 'en'
				};
				twitterFetcher.fetch(configProfile);

				$timeout(function () {
					$('.twitter_reply_icon').html('<i class="fa fa-reply fa-lg"></i>');
					$('.twitter_retweet_icon').html('<i class="fa fa-retweet fa-lg"></i>');
					$('.twitter_fav_icon').html('<i class="fa fa-heart fa-lg"></i>');

				}, 500);
			};

			/**
			 * Manage which slide to navigate to
			 */
			vm.manageSlideFragmentNavigation = function () {

				var fragments = window.location.hash.split("#");
				console.log('current onload fragments', fragments);

				if(fragments[2]){

					var slideClass = fragments[2].slice(0, -1);
					var slideNo = fragments[2].slice(fragments[2].length -1);

					console.log('slide class', slideClass, 'slide No', slideNo);

					vm.moveToSlide(slideClass, slideNo);
				}

			};

			/**
			 * Add the fragment to the page url when
			 * the slide is selected
			 *
			 * @param fragmentName
			 */
			vm.setSlideFragment = function (fragmentName) {


				// var fragments = window.location.hash.split("#");
				// console.log('current fragments', fragments);
				//
				// window.location = site_rel_path + '#' + fragments[1] + '#' + fragmentName;
				// console.log(site_rel_path + '#' + fragments[1] + '#' + fragmentName);
			};

			vm.setUpSharePlugin = function () {

				$("#share").jsSocials({
					shares: ["email", "twitter", "facebook"]
				});

				vm.changeSharePluginContent();
			};

			/**
			 * Change the content in the share plugin
			 */
			vm.changeSharePluginContent = function () {

				//Replace the label text
				var shareLabels = $('.jssocials-share-label');
				$(shareLabels['0']).text('SHARE');
				$(shareLabels['1']).text('TWEET');
				$(shareLabels['2']).text('EMAIL');

				//Replace the font awesome icons
				$('.fa-at').addClass('fa-envelope-o').removeClass('fa-at');

			};

			/**
			 * remove the active menu item
			 */
			vm.removeActiveMenuClass = function(){

				$('.home-menu-item').removeClass('activeMenuItem');
			};

			vm.toggleHeader = function(){
				$('.home-header').toggleClass('appear', $(document).scrollTop() > 0);
				console.log("cool");
			};

			/**
			 * Set up the cards to be used
			 */
			vm.setUpCardArray = function(){

				vm.cardArray = [
					{titleColor: '#ccd4dc',color: '#58728D', frontImg : 'images/home/features/white-six-pack-cleavage.png', frontName : 'Six-pack & Cleavage ban', backText : "No blatant skin shots as your main profile photo and nothing more risque across your gallery. There's a time and place people!"},
					{titleColor: '#f8d6dc',color: '#E9798B', frontImg : 'images/home/features/white-introductions.png', frontName : 'proper introductions', backText : '200-character minimum first message between users - we hate ‘Hey’ too!'},
					{titleColor: '#fef4e4',color: '#FAD8A4', frontImg : 'images/home/features/white-happii-twitter.png', frontName : 'happii twitter', backText : 'Lot’s of entertaining, useful and frankly bizarre dating hints and tips from around the world, along with other stuff we just find amusing.'},
					{titleColor: '#ccd4dc',color: '#58728D', frontImg : 'images/home/features/white-no-dormant.png', frontName : 'No dormant profiles', backText : 'We’ll refresh the database every month and suspend any in-active profiles.'},
					{titleColor: '#f8d6dc',color: '#E9798B', frontImg : 'images/home/features/white-matchmaker.png', frontName : 'Matchmake your friends', backText : "If you see someone who would be perfect for your single friend then you can share a limited version of their profile with your friends. Your friend would just need to sign up in order to view the full profile and communicate"},
					{titleColor: '#fef4e4',color: '#FAD8A4', frontImg : 'images/home/features/white-smart-search.png', frontName : 'smart search', backText : 'Search for a city, country, sport or pastime. We’ll return all profiles that have entered that keyword in their profile. Find a tennis partner in your area or recommendations for a forthcoming trip. Doesn’t have to be dating-related but filter options are available.'},
					{titleColor: '#ccd4dc',color: '#58728D', frontImg : 'images/home/features/white-galleries.png', frontName : 'photo & video galleries', backText : 'Create an extensive gallery to showcase the things you love.'},
					{titleColor: '#f8d6dc',color: '#E9798B', frontImg : 'images/home/features/white-calendar.png', frontName : 'date calendar', backText : 'For added peace of mind, log your dates on your private dashboard calendar to let Happii know who, when and where you’re meeting.'},
					{titleColor: '#fef4e4',color: '#FAD8A4', frontImg : 'images/home/features/white-customised-profile.png', frontName : 'Anti-scamming technology', backText : "We'll intercept any dodgy messages before they reach you and delete the senders account immediately"},
					{titleColor: '#ccd4dc',color: '#58728D', frontImg : 'images/home/features/white-pings.png', frontName : 'pings', backText : 'Share a thought, moment, video, photo or link. It’s up to you what you ping and no one can comment.'},
					{titleColor: '#f8d6dc',color: '#E9798B', frontImg : 'images/home/features/white-travel-map.png', frontName : 'interactive travel map', backText : 'Describe your favourite travel experiences and create photo & video galleries.'},
					{titleColor: '#fef4e4',color: '#FAD8A4', frontImg : 'images/home/features/white-bullshit-detector.png', frontName : 'bullsh*t detector', backText : "If we suspect you've been sent a message by a 'player' playing the numbers game, we'll alert you using this bull icon next to their message"}
				];

			};

			/**
			 * Put a flip on each card
			 */
			vm.setUpFlipCards = function () {

				for(var i = 0; i < vm.cardArray.length; i++){
					$("#card" + i).flip();
				}
			};

			/**
			 * Flip the card
			 *
			 * @param cardId
			 */
			vm.flipCard = function(cardId){

				$(cardId).flip('toggle');
			};

			/**
			 * Move to a certain slide
			 *
			 * @param className
			 * @param slide
			 */
			vm.moveToSlide = function(className, slide){

				$.fn.fullpage.moveTo(className, slide);
				console.log('called move to slide');
			};

			/**
			 * Scroll to the top of the page
			 */
			vm.scrollToPageTop = function () {

				$('html, body').animate({ scrollTop: 0 }, 'fast');
				console.log('scrolling to top');
			};

			/**
			 * Go to the next slick slide
			 */
			vm.slickNextSlide = function () {

				$('.slider').slick('slickNext');
				console.log('calling next slide');
			};

			/**
			 * Go to the prev slick slide
			 */
			vm.slickPrevSlide = function () {

				$('.slider').slick('slickPrev');
			};

			/**
			 * Navigate to a page
			 *
			 * @param location
			 */
			vm.navigateTo = function (location) {

				var path = window.location.origin + window.location.pathname + '#/' + location;
				window.location = path;

				window.location.reload();

				console.log('navigating to header', path);

				//vm.manageSlideFragmentNavigation();

				console.log('navigating to', path);
			};

			/**
			 * Open the contact form
			 */
			vm.openContactForm = function () {

				console.log('called contact form function');
				$state.go('contact');
			};

			/**
			 * Open the mobile menu
			 */
			vm.openNav = function() {
				document.getElementById("mobile-full-menu").style.width = "100%";
			};

			/**
			 * close the mobile menu
			 */
			vm.closeNav = function() {
				document.getElementById("mobile-full-menu").style.width = "0";
			};


			vm.setUpHomePage();



		}
    }

})();




