(function () {

	angular
		.module('app')
		.controller('signInController', signInControllerSetUp);

	function signInControllerSetUp($scope, $http, $q, $state, $stateParams, $timeout, siteService, loginService, profileService, $auth, authService){

		var vm = this;

		vm.profileId = 0;
		vm.profileComplete = false;

		/**
		 *
		 */
		vm.setUpSignInController = function() {

			siteService.setBackgroundImage('images/backgrounds/signIn.jpg');

			profileService.getUserAccountDetails().then(function (data) {

				console.log('account data received', data);

				if(data.user.oauth == true){

					if(data.profile.registered == null){
						$state.go('register');
					}
					else if(data.profile.complete == false){
						$state.go('bulider.welcome');
					}
					else if(data.profile.complete == true){
						$state.go('profile');
					}
				}

				if(data.profile.complete == true){

					vm.profileComplete = true;

				}

			});

			$timeout(function(){

				//console.log('The passed profile image', vm.profileimage);
				var profileObject = profileService.getUserAccountDetails().then(function (data) {

					console.log('header profile service auth', data);
					vm.profileId = data.user.id;

					if(vm.profileComplete == true){
						$state.go('profile-view', {id: vm.profileId});
					}
				});

			}, 100);
		};

		/**
		 * 
		 */
		vm.signIn = function (email, password) {

			loginService.logIn(email, password).then(
				function (user) {

					console.log('Log in status', user);

					loginService.status().then(function (data) {

						console.log('got login status', data);
						vm.getUserAccountDetails();

						vm.error = '';

					}).catch(function(data){

						console.log('could not get status');

					});
				},
				function (error) {

					console.log('Log in error', error);

					vm.error = error.message;
				}
			);
		};


		/**
		 * Get the user account details
		 */
		vm.getUserAccountDetails = function () {

			profileService.getUserAccountDetails().then(function (data) {

				console.log('account data received', data);
				vm.manageSignInStates(data);

			});
		};

		/**
		 *
		 * @param data
		 */
		vm.manageSignInStates = function (data) {

			if(data.profile.complete == null){
				$state.go('builder.welcome');
			}

			if(data.profile.complete == true){
				// $state.go('profile');
				$state.go('profile-view', {id: vm.profileId});
			}
		};

		/**
		 *
		 */
		vm.signInInstagram = function () {

			vm.outhLogin('instagram').then(function (url) {

				window.oauthCallback = function () {
					//window.opener.location.reload();

					profileService.getUserAccountDetails().then(function (data) {

						console.log('account data received', data);

						if(data.user.active == true){

							if(data.profile.registered == null){
								$state.go('register');
							}
							else if(data.profile.complete == false){
								$state.go('bulider.welcome');
							}
							else if(data.profile.complete == true){
								$state.go('profile');
							}
						}

						if(data.profile.complete == true){

							vm.profileComplete = true;

						}

					});
				};
				window.open(url);
			});

		};

		/**
		 *
		 */
		vm.signInFacebook = function () {

			vm.outhLogin('facebook').then(function (url) {

				console.log('url to open', url);

				window.oauthCallback = function () {

					//window.opener.location.reload();
					profileService.getUserAccountDetails().then(function (data) {

						console.log('account data received', data);

						if(data.user.active == true){

							if(data.profile.registered == null){
								$state.go('register');
							}
							else if(data.profile.complete == false){
								$state.go('bulider.welcome');
							}
							else if(data.profile.complete == true){
								$state.go('profile');
							}
						}

						if(data.profile.complete == true){

							vm.profileComplete = true;

						}

					});
				};

				window.open(url);
			});
		};

		vm.outhLogin = function (provider) {

			var defer = $q.defer();

			var url = server_path + 'user/endpoint/' + provider;

			$http.post(url, {}).then(function (response) {

				console.log('outh status ' + provider, response);

				defer.resolve(response.data.url);

			},function (err) {

				defer.reject(err.data);
			});

			return defer.promise;
		};


		// Set up the sign in controller
		vm.setUpSignInController();
	}
})();

