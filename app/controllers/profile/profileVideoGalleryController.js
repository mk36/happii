(function () {

	angular
		.module('app')
		.controller('profileVideoGalleryController', profileVideoGalleryControllerSetUp);

	function profileVideoGalleryControllerSetUp($http, $q, $scope, $state, $stateParams, $timeout, siteService, profileService){

		var vm = this;

		vm.videosFavourite = [];
		vm.profileObj = {};
		vm.newFaveVideo = {name : 'video', src : '', type: 'url'};

		vm.newYoutubeVid = '';
		vm.apiResults = [];

		/**
		 * Watch for changes on the show variable
		 */
		$scope.$watch('vm.apiResults', function(value, old) {}, true);



		vm.setUpProfileVideoGalleryController = function(){

			$timeout(function () {
				//profileFaveVideos
				vm.profileObj = profileService.getProfileObj();

				vm.videosFavourite = vm.profileObj.properties.questions.profileFaveVideos;

				console.log('favourite videos', vm.videosFavourite);
				console.log('profile video gallery controller setup', vm.profileObj);

				vm.setUpVideoMediaPlugin();

			}, 1000);


		};

		/**
		 * Set up the video media plugin
		 */
		vm.setUpVideoMediaPlugin = function () {
			$timeout(function(){
				//Set up the angular videos
				$('.video-fave').mediaelementplayer();
			}, 500);

			vm.setUpYoutubeApi();
		};

		/**
		 * Add favourite video to new video
		 */
		vm.addFavouriteVideo = function () {

			if(vm.newFaveVideo.name.length > 0 && vm.newFaveVideo.src.length > 0){

				if(!vm.videosFavourite.includes(vm.newFaveVideo)){

					//Remove the element from the array
					//_.pull(vm.photos , image);
					vm.videosFavourite.push(vm.newFaveVideo);
					vm.updateVideoFavouriteList();
					console.log('added video to favourite videos');

					$("#video-fave-modal").modal("hide");

					//Reset up the video plugin with the added new videos
					vm.setUpVideoMediaPlugin();
				}
			}

			//Reset the new fave video
			vm.newFaveVideo = {name : 'video', src : '', type: 'url'};


		};

		/**
		 * validate a url using regex
		 *
		 * @returns {boolean}
		 */
		vm.validateUrl = function () {

			var pattern = /\b((?:[a-z][\w\-]+:(?:\/{1,3}|[a-z0-9%])|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}\/)(?:[^\s()<>]|\((?:[^\s()<>]|(?:\([^\s()<>]+\)))*\))+(?:\((?:[^\s()<>]|(?:\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'".,<>?«»“”‘’]))/;

			return pattern.test(vm.newFaveVideo.src);
		};

		/**
		 *  Update the favourite video list
		 */
		vm.updateVideoFavouriteList = function(){

			vm.profileObj.setQuestionValueByQuestionReference('profileFaveVideos', vm.videosFavourite);
			console.log(vm.profileObj);
		};

		/**
		 * Get image path local
		 *
		 * @param imagename
		 * @returns {string}
		 */
		vm.getImagePathLocal = function(imagename){
			return SITE_REL_PATH + 'image/' + imagename;
		};

		/**
		 * Search youtube api
		 */
		vm.searchYoutubeApi = function () {

			vm.apiResults = [];

			var text = vm.newYoutubeVid;

			console.log('searching books api...');

			var query = text.replace(" ", "+");

			// Use the JavaScript client library to create a search.list() API call.
			var request = gapi.client.youtube.search.list({
				q: text,
				part: 'snippet'
			});

			// Send the request to the API server, with callback
			request.execute(vm.onSearchResponse);
		};

		// Set up the youtube api
		vm.setUpYoutubeApi = function() {
			gapi.client.load('youtube', 'v3', function () {

				gapi.client.setApiKey('AIzaSyCmzxxqjMr-y7iCEyVximzCHJXDcVtMsCA');

			});
		};

		// Called automatically with the response of the YouTube API request.
		vm.onSearchResponse = function(response) {

			var responseString = JSON.stringify(response, '', 2);

			console.log('json response for youtube', response.items);

			vm.apiResults = response.items;

			//update the view since scope.watch doesn't want to play nicely
			$scope.$apply();
		};

		/**
		 * Add the new youtube video to the favourites list
		 *
		 * @param videoData
		 */
		vm.addYoutubeVideoToFavouriteList = function (videoData) {

			//Reset the search option
			vm.newYoutubeVid = '';

			//Create a new youtube video object
			vm.newFaveVideo = {name : 'video', src : videoData.id.videoId, type: 'youtube', youtubeObj: videoData};
			vm.videosFavourite.push(vm.newFaveVideo);

			//Save the data
			vm.updateVideoFavouriteList();

		};

		/**
		 * Set up the youtube video iframe which plays
		 * the video
		 *
		 * @param video
		 */
		vm.setUpYoutubeVideoToPlay = function(videoId){

			if (typeof(YT) == 'undefined' || typeof(YT.Player) == 'undefined') {

				//Create a script
				var tag = document.createElement('script');
				tag.src = "https://www.youtube.com/iframe_api";

				var firstScriptTag = document.getElementsByTagName('script')[0];

				firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

				window.onYouTubePlayerAPIReady = function() {

					onYouTubePlayer(videoId);
				};

			}else {
				onYouTubePlayer(videoId);
			}
		};

		var players = [];

		function onYouTubePlayer(videoId) {

			var player = new YT.Player(videoId, {
				height: '100%',
				width: '100%',
				videoId: videoId,
				playerVars: { controls:1, showinfo: 0, rel: 0, showsearch: 0, iv_load_policy: 3 },
				events: {
					'onReady': onPlayerReady
				}

			});

			players.push(player);
		}

		function onPlayerReady(event) {
			event.target.playVideo();
		}

		// var instances = plyr.setup({
		// 	// Output to console
		// 	debug: true
		// });


		vm.setUpVimeo = function () {


			$timeout(function () {
				plyr.setup();

			}, 200);
		};



		vm.setUpProfileVideoGalleryController();
	}
})();

