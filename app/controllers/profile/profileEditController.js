(function () {

	angular
		.module('app')
		.controller('profileEditController', profileEditControllerSetUp);

	function profileEditControllerSetUp($scope, $state, $stateParams, $timeout, siteService, profileService, localStorageService){

		var vm = this;

		vm.profileObj = {};
		vm.registerProfileData = {};

		//The answers to the profile
		vm.profileAnswers = [];
		vm.partnerAnswers = [];

		//Slick sliders
		vm.slickConfigSimilarProfiles = {};

        $scope.$watch('vm.profileObj', function(value, old) {

            //console.log('old images', old, 'new images', value);
        }, true);

        /**
		 * Set up the register controller
         */
		vm.setUpProfileEditController = function(){

            //login
            profileService.getUserAccountDetails().then(function (data) {

                if(data.profile.complete == true){

                    console.log('profile complete true', data);

                    //Update the profile for the local storage object
                    profileService.createProfileObjFromServer(data);
                    vm.profileObj = profileService.getProfileObj();

					vm.registerProfileData = data.data;

                    $timeout(function(){

                        console.log('profile obj', vm.profileObj);

                        //Set up the profile icons
                        vm.setUpProfileAnswers();

                    }, 300);

                }
                else{
                    $state.go('builder.welcome');
                    alertService.triggerAlert('Please Complete the Wizard');
                }

            }).catch(function (data) {

                $state.go('sign-in');
                //alertService.triggerAlert('Please sign in');

            });


		};

		/**
		 * Set up the profile answers to be displayed
		 */
		vm.setUpProfileAnswers = function(){

			var questions = vm.profileObj.properties.questions;

			//User profile answers
			vm.profileAnswers.push({title : 'body type', value: questions.bodyType});
			vm.profileAnswers.push({title : 'children', value: questions.children});
			vm.profileAnswers.push({title : 'drinking', value: questions.drinking});
			vm.profileAnswers.push({title : 'education', value: questions.education});
			vm.profileAnswers.push({title : 'employment', value: questions.employment});
			vm.profileAnswers.push({title : 'height', value: questions.height});
			vm.profileAnswers.push({title : 'religion', value: questions.religion});
			vm.profileAnswers.push({title : 'smoking', value: questions.smoking});

			//Partner profile answers
			profileService.getPartnerAnswers().then(function(data){

				console.log('partner answers returned', data);
				vm.partnerAnswers = data;
			});
		};

		vm.setUpProfileEditController();
	}
})();

