(function () {

	angular
		.module('app')
		.controller('profilePageViewController', profilePageViewControllerSetUp);

	function profilePageViewControllerSetUp($scope, $state, $stateParams, $timeout, siteService, profileService){

		var vm = this;

		vm.profileObj = {};

		//The answers to the profile
		vm.profileAnswers = [];

		//Slick sliders
		vm.slickConfigSimilarProfiles = {};

		//user profile object
		vm.userProfileObj = {};

        /**
		 * Set up the register controller
         */
		vm.setUpProfilePageViewController = function(){

			console.log('profile id', $stateParams.id);

			profileService.createProfileObj();
			vm.userProfileObj = profileService.getProfileObj();

			console.log('user profile', vm.userProfileObj);

			profileService.getProfileDetailsById($stateParams.id).then(function (data) {

				console.log('data received from profile id post', data);

				vm.profileObj = data.data.data;

				if(vm.profileObj.properties == null){

					vm.profileObj.properties = data.data.data;

					//temp bug fix, this whole area needs sorting
					if(vm.profileObj.properties.questions == null){
						vm.profileObj = vm.userProfileObj;
					}

					console.log('not null');
				}

				console.log('profile obj', vm.profileObj);

				$timeout(function(){

					//Set up the profile icons
					vm.setUpProfileAnswers();

					vm.setUpSlickSliders();

					siteService.setSiteWrapperHeight('100vh');
				}, 300);
			});

		};

		/**
		 * Set up the profile answers to be displayed
		 */
		vm.setUpProfileAnswers = function(){

			var questions = vm.profileObj.properties.questions;

			vm.profileAnswers.push({title : 'body type', value: questions.bodyType});
			vm.profileAnswers.push({title : 'children', value: questions.children});
			vm.profileAnswers.push({title : 'drinking', value: questions.drinking});
			vm.profileAnswers.push({title : 'education', value: questions.education});
			vm.profileAnswers.push({title : 'employment', value: questions.employment});
			vm.profileAnswers.push({title : 'height', value: questions.height});
			vm.profileAnswers.push({title : 'religion', value: questions.religion});
			vm.profileAnswers.push({title : 'smoking', value: questions.smoking});

		};



		/**
		 * Set up any slick sliders needed for the page
		 */
		vm.setUpSlickSliders = function(){

			//The similar profiles slider
			vm.slickConfigSimilarProfiles = {
				//enabled: true,
				//autoplay: true,
				draggable: false,
				dots: false,
				arrows: false,
				controls: false,
				infinite: true,
				slidesToShow: 3,
				slidesToScroll: 3,
				mobileFirst: true,
				event: {
					beforeChange: function (event, slick, currentSlide, nextSlide) {

					},
					afterChange: function (event, slick, currentSlide, nextSlide) {

					},
					init: function () {
						console.log('set up slider for rob!!');
					}
				},
				responsive: [
					{
						breakpoint: 1024,
						settings: {
							slidesToShow: 3,
							slidesToScroll: 3
						}
					},
					{
						breakpoint: 480,
						settings: {
							slidesToShow: 2,
							slidesToScroll: 1
						}
					},
					{
						breakpoint: 100,
						settings: {
							slidesToShow: 1,
							slidesToScroll: 1
						}
					}
				]
			};

			console.log('slider setup 345');
		};

		vm.setUpProfilePageViewController();
	}
})();

