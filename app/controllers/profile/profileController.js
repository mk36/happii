(function () {

	angular
		.module('app')
		.controller('profileController', profileControllerSetUp);

	function profileControllerSetUp($scope, $state, $stateParams, $timeout, siteService, profileService, alertService, localStorageService){

		var vm = this;

		vm.sidebar = [
			{name : 'View Profile', image : 'images/icons/profile/sidebar/editProfile.png', link : 'profile-view({id: vm.profileId})'},
			{name : 'Edit Profile', image : 'images/icons/profile/sidebar/profileEdit.png', link : 'profile.profile-edit'},
			{name : 'Photo Gallery', image : 'images/icons/profile/sidebar/photo.png', link : 'profile.photo-gallery'},
			{name : 'Video Gallery', image : 'images/icons/profile/sidebar/video.png', link : 'profile.video-gallery'}
			];

		vm.sidebarComingSoon = [

			{name : 'Activity', image : 'images/icons/profile/sidebar/activity.png', link : 'profile.activity'},
			{name : 'PINGS', image : 'images/icons/profile/sidebar/pings.png', link : 'profile.pings'},
			{name : 'New Search', image : 'images/icons/profile/sidebar/search.png', link : 'profile.search'},
			{name : 'Messages', image : 'images/icons/profile/sidebar/messages.png', link : 'profile.messages'},
			{name : 'Favourites', image : 'images/icons/profile/sidebar/favourites.png', link : 'profile.favourites'},

			{name : 'Date Diary', image : 'images/icons/profile/sidebar/diary.png', link : 'profile.diary'}
		];

		vm.profileObj = {};

		//Store the current state so it can be used to highlight menu items
		vm.currentState = '';

		vm.profileId = '';

		vm.setUpProfileController = function(){

			siteService.setSiteWrapperHeight('100vh');

			//login
			profileService.getUserAccountDetails().then(function (data) {

				console.log('profile cntl data received', data);

				vm.profileId = data.user.id;

				if(data.profile.complete == true){

					//console.log('profile complete true', data);
					//console.log('profile complete true local storage', localStorageService.get('profileBuilderObject'));

					//Update the profile for the local storage object
					profileService.createProfileObjFromServer(data);

					vm.profileObj = profileService.getProfileObj();
					vm.currentState = vm.getCurrentState();

				}
				else{
					$state.go('builder.welcome');
					alertService.triggerAlert('Please Complete the Wizard');
				}

			}).catch(function (data) {

				$state.go('sign-in');
				//alertService.triggerAlert('Please sign in');

			});
		};

		vm.getCurrentState = function(){

			return $state.current.name;
		};


		vm.setUpProfileController();
	}
})();

