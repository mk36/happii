(function () {

	angular
		.module('app')
		.controller('profileGalleryController', profileGalleryControllerSetUp);

	function profileGalleryControllerSetUp($scope, $state, $stateParams, $timeout, siteService, profileService){

		var vm = this;

		vm.profileObj = {};
		vm.photos = [];

		vm.deleteMode = false;

        /**
		 * Set up the register controller
         */
		vm.setUpProfileGalleryController = function(){

			profileService.createProfileObj();

			vm.profileObj = profileService.getProfileObj();

			console.log('profile controller setup', vm.profileObj);

			$timeout(function(){

				vm.photos = vm.profileObj.properties.questions.profileGallery;
			}, 100);

		};

		/**
		 * Get the image path
		 *
		 * @param imageName
		 */
		vm.getImagePath = function(imageObj){

			return window.server_path + 'image/' + imageObj.name;
		};

		/**
		 * Manage the images that you would like to delete
		 *
		 * @param image
		 */
		vm.manageImagesToDelete = function(image){

			if(image.delete == null){
				image.delete = true;
			}
			else{
				image.delete = !image.delete;
			}
		};

		/**
		 * Toggle delete mode
		 */
		vm.toggleDeleteMode = function () {

			vm.deleteMode = !vm.deleteMode;

			//If delete mode has been triggered off, delete the selected photos
			if(vm.deleteMode == false){

				for(var i = 0; i <= vm.photos.length; i++){
					vm.deleteImage(vm.photos[i]);
				}

				//Update the profile object
				vm.updateProfileBuilderObject();
			}

		};

		/**
		 * Delete an image from the photo gallery
		 */
		vm.deleteImage = function(image){

			//var index = _.indexOf(vm.imagesToDelete, image);

			if(vm.photos.includes(image) && image.delete == true){

				//Remove the element from the array
				_.pull(vm.photos , image);
			}
		};

		/**
		 * Update the profile builder object
		 */
		vm.updateProfileBuilderObject = function(){

			vm.profileObj.setQuestionValueByQuestionReference('profileGallery', vm.photos);

			console.log('profile controller setup', vm.profileObj);
		};



		vm.setUpProfileGalleryController();
	}
})();

