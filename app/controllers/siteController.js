/**
 * This controller is accessed via reference through the site service
 */

(function () {

	angular
		.module('app')
		.controller('siteController', siteControllerSetUp);

	function siteControllerSetUp($scope, $state, $stateParams, $timeout, siteService, alertService, localStorageService){

		var siteController = this;


		siteController.alerts = alertService.getAlerts();

		//localStorageService.clearAll();



		console.log('site controller setup');

        /**
		 * Set the background image
		 *
         * @param url
         */
		siteController.setBackgroundImage = function (url) {

			siteController.backgroundImage = url;
        };
	}
})();

