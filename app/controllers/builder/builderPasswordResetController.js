(function () {

	angular
		.module('app')
		.controller('builderPasswordResetController', builderPasswordResetControllerSetUp);

	function builderPasswordResetControllerSetUp($scope, $state, $stateParams, $timeout, siteService, builderService, loginService, profileService, alertService){

		var vm = this;
		vm.name = '';

		vm.email = $stateParams.email;
		vm.code = $stateParams.code;

		vm.password = '';
		vm.passwordRetype = '';



		console.log('builder password controller setup', 'email', vm.email, 'code', vm.code);

        /**
		 * Set up the builder controller
         */
		vm.setUpBuilderPasswordResetController = function(){

			//Set the background image
            siteService.setBackgroundImage('images/backgrounds/wizard/horse.jpg');

			$('#register-password').pwstrength({
				ui: { showVerdictsInsideProgressBar: true }
			});

		};

		/**
		 * Check if the password matches the needed conditions
		 *
		 * returns true if the password is ok
		 *
		 * @returns {boolean}
		 */
		vm.checkPasswordConditions = function(){

			var numberCheck = vm.checkPasswordContainsNumber(vm.password);
			var lengthCheck = vm.checkPasswordValidLength(vm.password);
			var uppercaseCheck =vm.checkPasswordContainsUppercaseCharacter(vm.password);


			if(numberCheck == true && lengthCheck == true && uppercaseCheck == true){

				return true;
			}

			return false;
		};

		/**
		 * Check password contains a number
		 *
		 * @param password
		 * @returns {boolean}
		 */
		vm.checkPasswordContainsNumber = function(password){

			var parts = password.split("");

			for(var i = 0; i < parts.length; i++){

				if(parseInt(parts[i]).toString() != 'NaN'){
					return true;
				}
			}

			return false;
		};

		/**
		 * Check the password is a valid length
		 *
		 * @param password
		 * @returns {boolean}
		 */
		vm.checkPasswordValidLength = function (password) {

			var minLength = 6;

			if(password.length <= minLength){
				return false;
			}

			return true;
		};

		/**
		 * Check the password contains at least one uppercase character
		 *
		 * @param password
		 * @returns {boolean}
		 */
		vm.checkPasswordContainsUppercaseCharacter = function (password) {

			var pattern = /[A-Z]+/;
			return pattern.test(password);
		};

		vm.saveNewPassword = function () {

			loginService.recover(vm.email, vm.code, vm.password).then(function (data) {

				console.log('password recovered', data);
			});
		};

		vm.setUpBuilderPasswordResetController();

	}
})();

