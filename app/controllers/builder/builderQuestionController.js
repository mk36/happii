(function () {

	angular
		.module('app')
		.controller('builderQuestionController', builderQuestionControllerSetUp);

	function builderQuestionControllerSetUp($scope, $state, $stateParams, $timeout, siteService, builderService, loginService){

		var vm = this;

		vm.builderQuestionDots = [];
		vm.activeQuestion = 0;

		var activeQuestionPromise = builderService.getActiveQuestionPromise();

		activeQuestionPromise.then(null, null, function (number) {
			vm.activeQuestion = number;
		});

        /**
		 * Set up the builder controller
         */
		vm.setUpBuilderQuestionController = function(){

			console.log('builder question controller setup');

			//Set the background image
			siteService.setBackgroundImage('images/backgrounds/animals/puffins.jpg');

			//Set up the builder dots
			vm.setUpBuilderDotsArray();
		};

		/**
		 * Set up the amount of dots to be shown
		 * on the builder
		 */
		vm.setUpBuilderDotsArray = function(){

			vm.builderQuestionDots = _.range(1, 9);
			vm.activeQuestion = builderService.getActiveQuestion();

			vm.builderQuestionDots.reverse();
		};

        /**
		 * Sign out of happii
         */
		vm.signOut = function () {

			loginService.logOut().then(function () {

				$state.go('sign-in');
            });
        };

		vm.setUpBuilderQuestionController();

	}
})();

