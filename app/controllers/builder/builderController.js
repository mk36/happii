(function () {

	angular
		.module('app')
		.controller('builderController', builderControllerSetUp);

	function builderControllerSetUp($scope, $state, $stateParams, $timeout, siteService, builderService){

		var vm = this;

        /**
		 * Set up the builder controller
         */
		vm.setUpBuilderController = function(){

            //Create a new profile builder object
            builderService.createProfileBuilderObj();
		};

		vm.setUpBuilderController();

	}
})();

