(function () {

	angular
		.module('app')
		.controller('builderActivateController', builderActivateControllerSetUp);

	function builderActivateControllerSetUp($scope, $state, $stateParams, $timeout, siteService, builderService, loginService, profileService, alertService){

		var vm = this;
		vm.name = '';

		vm.email = $stateParams.email;
		vm.code = $stateParams.code;

		vm.activated = false;



		console.log('builder activate controller setup', 'email', vm.email, 'code', vm.code);

        /**
		 * Set up the builder controller
         */
		vm.setUpBuilderActivateController = function(){

			//Set the background image
            siteService.setBackgroundImage('images/backgrounds/wizard/horse.jpg');

            //Set first name

			loginService.activate(vm.email, vm.code).then(function (data) {

				console.log('activated profile', data);

				vm.activated = true;

			}).catch(function (data) {

				console.log('could not activate profile', data);

				if(data.code == 'ALREADY_ACTIVATED'){
					alertService.triggerAlert('Profile is already activated');

					vm.activated = true;
					$state.go('sign-in');
				}


			});

		};

		vm.setUpBuilderActivateController();

	}
})();

