(function () {

	angular
		.module('app')
		.controller('builderProfileCompleteController', builderProfileCompleteControllerSetUp);

	function builderProfileCompleteControllerSetUp($scope, $state, $stateParams, $timeout, siteService, builderService, profileService, alertService){

		var vm = this;

		vm.profileId = 0;

        /**
		 * Set up the builder controller
         */
		vm.setUpBuilderProfileCompleteController = function(){

            siteService.setBackgroundImage('images/backgrounds/wizard/penguins.jpg');
            builderService.setActiveQuestion(8);

            var builderDetails = builderService.getProfileBuilderObj();

			profileService.saveBuilderCompleteDetails(builderDetails).then(function(data){

				console.log('profile wizzard save compelte', data);

			}).catch(function (data) {

				console.log('couldnt save details', data);
				alertService.triggerAlert('could not save profile question answers, please make sure your not missing a profile photo');
				$state.go('builder.welcome');
			})



			$timeout(function(){

				//console.log('The passed profile image', vm.profileimage);
				var profileObject = profileService.getUserAccountDetails().then(function (data) {

					console.log('header profile service auth', data);
					vm.profileId = data.user.id;
				});

			}, 100);
		};



		vm.setUpBuilderProfileCompleteController();

	}
})();

