(function () {

	angular
		.module('app')
		.controller('builderTravelController', builderTravelControllerSetUp);

	function builderTravelControllerSetUp($scope, $state, $stateParams, $timeout, siteService, builderService){

		var vm = this;

        /**
		 * Set up the builder controller
         */
		vm.setUpBuilderTravelController = function(){

            builderService.setActiveQuestion(5);
            siteService.setBackgroundImage('images/backgrounds/wizard/travel.jpg');
		};

		vm.setUpBuilderTravelController();

	}
})();

