(function () {

	angular
		.module('app')
		.controller('builderTravelPlacesSetController', builderTravelPlacesSetControllerSetUp);

	function builderTravelPlacesSetControllerSetUp($scope, $state, $stateParams, $timeout, siteService, builderService){

		var vm = this;

		//The selected options for travelling
		vm.selectedPlacesBeenOptions = [];
		vm.selectedPlacesGoOptions = [];

		//The placeholder items when a travel option hasn't been filled
		vm.travelBeenPlaceholderItems = [];
		vm.travelGoPlaceholderItems = [];

        /**
		 * Set up the builder controller
         */
		vm.setUpBuilderTravelPlacesSetController = function(){

			$timeout(function(){

				vm.setUpGoogleInput('places-been-input', 'been');
				vm.setUpGoogleInput('places-go-input', 'go');

				vm.setUpPlaceholderList(vm.travelBeenPlaceholderItems, 'place');
				vm.setUpPlaceholderList(vm.travelGoPlaceholderItems, 'place');

                //Repopulate any added location data
                vm.repopulateLocationData();
			}, 100);


		};

		/**
		 * Set up the placeholder lists
		 */
		vm.setUpPlaceholderList = function(model, name){

			for(i = 0; i < 4; i++){

				model.push({title: name + (i + 1)});
			}
		};


		/**
		 * Set up a google places input
		 *
		 * @param inputId
		 * @param modelToUpdate
		 */
		vm.setUpGoogleInput = function(inputId, modelToUpdate){

			var inputSearchPlacesBeen = document.getElementById(inputId);
			var searchPlacesBeenBox = new google.maps.places.SearchBox(inputSearchPlacesBeen);

			//Create a listener for when a valid place is selected
			searchPlacesBeenBox.addListener('places_changed', function() {


					var places = searchPlacesBeenBox.getPlaces();

					places.forEach(function(place) {

						console.log('place', place, 'model to update', modelToUpdate);
						var latLng = place.geometry.location;

						vm.addPlacesToTravelSelected(modelToUpdate, place.name, latLng.lat(), latLng.lng());

						$('#' + inputId).val('');

						$scope.$apply();
					})

			});
		};

		/**
		 * Check if a place can be added to the selected places
		 *
		 * @param model
		 * @param item
		 * @returns {boolean}
		 */
		vm.checkPlacesConditionsToAdd = function(model, item){

			var index = _.findIndex(model, function(place) { return place.name == item.name});

			if(index < 0 && model.length < 4 && item != "") {

				return true;
			}

			return false;
		};

		/**
		 * Add a place to the selected travel model
		 *
		 * @param model
		 * @param name
		 * @param lat
		 * @param lng
		 */
		vm.addPlacesToTravelSelected = function(model, name, lat, lng){

			var modelToUpdate = {};

			if(model == 'been'){
				modelToUpdate = vm.selectedPlacesBeenOptions;
			}
			else{
				modelToUpdate = vm.selectedPlacesGoOptions;
			}

			var location = {

				name : name,
				lat: lat,
				lng: lng
			};

			if(vm.checkPlacesConditionsToAdd(modelToUpdate, location) == true){

				modelToUpdate.push(location);
				console.log('adding new place', modelToUpdate);
			}
		};

		/**
		 * Remove a place from the selected travel model
		 *
		 * @param model
		 * @param item
		 */
		vm.removePlacesItem = function(model, item){

			//Remove the item from the selected option array
			_.pull(model , item);

			console.log('removing place', model);
		};

		/**
		 * Save the selection to the typescript object
		 *
		 */
		vm.saveSelection = function(){

			//Update the profile builder object
			var profileBuilderObj = builderService.getProfileBuilderObj();

			console.log('profile builder object', profileBuilderObj);

			//Save travel been
			profileBuilderObj.setQuestionValueByQuestionReference('travelBeen', vm.selectedPlacesBeenOptions);

			//Save travel go
			profileBuilderObj.setQuestionValueByQuestionReference('travelGo', vm.selectedPlacesGoOptions);

			if(vm.selectedPlacesBeenOptions.length > 0){

				//Go to the places describe state
				$state.transitionTo('builder.questions.travel.places-describe');
			}
			else{
				//Go to the partner questions
				$state.transitionTo('builder.questions.partner.gender');
			}

		};

        /**
         * Repopulate the location data
         *
         */
        vm.repopulateLocationData = function () {

            //Get the profile builder object
            var profileBuilderObj = builderService.getProfileBuilderObj();

            if(profileBuilderObj.properties.questions.travelBeen != null){

                //Repopulate the postcode stored
                vm.selectedPlacesBeenOptions = profileBuilderObj.properties.questions.travelBeen;
            }

            if(profileBuilderObj.properties.questions.travelGo != null){

                //Repopulate the postcode stored
                vm.selectedPlacesGoOptions = profileBuilderObj.properties.questions.travelGo;
            }

        };


		vm.setUpBuilderTravelPlacesSetController();

	}
})();

