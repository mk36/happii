(function () {

	angular
		.module('app')
		.controller('builderTravelPlacesDescribeController', builderTravelPlacesDescribeControllerSetUp);

	function builderTravelPlacesDescribeControllerSetUp($scope, $state, $stateParams, $timeout, $http, siteService, builderService, questionService, authService){

		var vm = this;

		//The selected options for travelling
		vm.selectedPlacesBeenOptions = [];

		//Stores the place description
		vm.activePlace = '';

		vm.socialPhotos = [];
		vm.activeSocialPhoto = {name : '', src : ''};

		vm.showImageEditor = false;

		vm.accessTokens = {
			facebook : '',
			instagram : ''
		};

		vm.photoUploadMessage = '';

        /**
		 * Set up the builder controller
         */
		vm.setUpBuilderTravelPlacesDescribeController = function(){

			$timeout(function(){

				vm.getPlacesBeenOptionsFromTypescript();

				vm.activePlace = vm.selectedPlacesBeenOptions[0];

				vm.setUpDropzone();

			}, 100);
		};

		/**
		 * Get the places where the person has been from the saved
		 * typescript object
		 */
		vm.getPlacesBeenOptionsFromTypescript = function(){

			//Update the profile builder object
			var profileBuilderObj = builderService.getProfileBuilderObj();

			console.log('profile builder object', profileBuilderObj);

			//Save travel been
			vm.selectedPlacesBeenOptions = profileBuilderObj.getQuestionValueByQuestionReference('travelBeen');
		};

		/**
		 * Switch the active place to a new one, saving the existing
		 * place data to typescript object before it happens
		 *
		 * @param place
		 */
		vm.switchActivePlace = function (place) {

			vm.saveActivePlaceObject();
			vm.setActivePlaces(place);
		};

		/**
		 * Set the active place
		 *
		 * @param place
		 */
		vm.setActivePlaces = function(place){

			vm.activePlace = place;
		};

		/**
		 * Save the active place object
		 */
		vm.saveActivePlaceObject = function(){

			//Update the profile builder object
			var profileBuilderObj = builderService.getProfileBuilderObj();

			console.log('profile builder object', profileBuilderObj);

			//Save travel been
			profileBuilderObj.setQuestionValueByQuestionReference('travelBeen', vm.selectedPlacesBeenOptions);

		};


		/**
		 * Save the selection to the typescript object
		 *
		 */
		vm.saveSelection = function(){

			vm.saveActivePlaceObject();

			//Go to the next state
			$state.transitionTo('builder.questions.partner.gender');

		};

		/**
		 * Set up drop zone for drag and drop file uploading
		 */
		vm.setUpDropzone = function(){

			console.log('dropzone set up');

			/**
			 * Dropzone image upload
			 */
			$("div#photo-upload-display").dropzone({ url: window.server_path + "upload/", paramName: "file", // The name that will be used to transfer the file
				maxFilesize: 5, // MB
				//maxFiles: 1,
				//uploadMultiple: false,
				clickable: true,
				acceptedFiles: '.jpeg,.jpg,.png,.gif',
				accept: function(file, done) {

					//console.log('accepted image');

					//Create the image upload object
					var imageUploadObj = {
						form : file,
						fileName : file.name
					};

					//Upload th eimage to the server
					questionService.uploadImageToServer(imageUploadObj, file.name).then(function(data){

						//console.log('image path returned', data.data[0].file_name);

						//vm.profilePhoto.src = window.server_path + 'image/' + data.data[0].file_name;
						//vm.profilePhoto.name = data.data[0].file_name;
						if(vm.activePlace.images == null){

							vm.activePlace.images = [];
						}

						vm.activePlace.images.push(data.data[0].file_name);

						console.log('server image path', window.server_path + 'image/' + data.data[0].file_name);

						vm.photoUploadMessage = 'Uploaded ' + file.name + ' successfully';
					});
				}});

			/**
			 * Dropzone video upload
			 */
			$("div#video-upload-display").dropzone({ url: window.server_path + "upload/", paramName: "file", // The name that will be used to transfer the file
				maxFilesize: 5, // MB
				//maxFiles: 1,
				//uploadMultiple: false,
				clickable: true,
				acceptedFiles: '.mp4,.mkv,.avi',
				accept: function(file, done) {

					//console.log('accepted image');

					//Create the image upload object
					var imageUploadObj = {
						form : file,
						fileName : file.name
					};

					//Upload th eimage to the server
					questionService.uploadImageToServer(imageUploadObj, file.name).then(function(data){

						if(vm.activePlace.videos == null){

							vm.activePlace.videos = [];
						}

						vm.activePlace.videos.push(data.data[0].file_name);

						console.log('server image path', window.server_path + 'image/' + data.data[0].file_name);

                        vm.photoUploadMessage = 'Uploaded ' + file.name + ' successfully';
					});
				}});
		};

		/**
		 *	Call a click event on the dropzone button so the device
		 *  file browser cna be opened
		 */
		vm.triggerDropZoneForImage = function(){

			$("#photo-upload-display").click();
		};

		/**
		 *	Call a click event on the dropzone button so the device
		 *  file browser cna be opened
		 */
		vm.triggerDropZoneForVideo = function(){

			$("#video-upload-display").click();
		};

		/**
		 * Authorise facebook login
		 */
		vm.authFacebook = function(){

			authService.authenticateFacebook().then(function(data){

				vm.accessTokens.facebook = data;

				authService.getFacebookUserPhotos().then(function(data){

					vm.socialPhotos = data;
					console.log('social photos', vm.socialPhotos);

					$("#facebook-pic-modal").modal({backdrop: false});


				}, function (data) {

					console.log('epic fail with getting facebook auth', data);
				});

			});

		};

		/**
		 * Set the social image to edit
		 *
		 * requires the url of the image to edit and the modal they
		 * are contained in to close
		 *
		 * @param image
		 * @param modal
		 */
		vm.saveSocialImage = function(){

			var fileName = vm.activeSocialPhoto.src;

			if(vm.activePlace.images == null){

				vm.activePlace.images = [];
			}

			console.log('social image to upload', vm.activeSocialPhoto.socialImg, 'file name', window.server_path + fileName);

			var obj = {
				base64 : vm.activeSocialPhoto.socialImg
			};

			//Upload the image to the server
			$http.post(window.server_path + 'upload/' + fileName, obj).then(function(data){

				//Add the image to the active travel object
				vm.activePlace.images.push(data.data[0].file_name);

				//Reset the active social image
				vm.activeSocialPhoto = {name : '', src : ''};
			});

		};

		/**
		 * Set the canvas to the chosen facebook image
		 *
		 * @param id
		 */
		vm.setSocialCanvasImage = function(id, url) {

			var img = new Image();

			img.setAttribute('crossOrigin', 'anonymous');

			img.onload = function () {
				var canvas = document.createElement("canvas");
				canvas.width =this.width;
				canvas.height =this.height;

				var ctx = canvas.getContext("2d");
				ctx.drawImage(this, 0, 0);

				var dataURL = canvas.toDataURL("image/png");

				//The image data stored as a base64 to be displayed
				var justData = dataURL.replace(/^data:image\/(png|jpg);base64,/, "");

				vm.activeSocialPhoto.src = id;
				vm.activeSocialPhoto.socialImg = justData;
			};

			img.src = url;
		};



		vm.setUpBuilderTravelPlacesDescribeController();

	}
})();

