(function () {

	angular
		.module('app')
		.controller('builderBasicStatsHeightController', builderBasicStatsHeightControllerSetUp);

	function builderBasicStatsHeightControllerSetUp($scope, $state, $stateParams, $timeout, siteService, builderService){

		var vm = this;

		vm.height = 4.0;
		vm.heightSteps = [];

		vm.slider = {};

		/**
		 * Set up the location from controller
		 */
		vm.setUpHeightController = function(){

			vm.repopulateProfileData();

			//Create the steps array
			vm.createHeightSteps();

			//Set up the slider
			vm.setUpSlider();

			//Set the subquestion
			builderService.setActiveSubQuestion(5);


		};

		/**
		 * Set up the slider
		 */
		vm.setUpSlider = function(){

			vm.slider = {
				value: vm.height,
				options: {
					floor: 4,
					ceil: 7,
					step: 0.1,
					precision: 1,
					showTicks: true,
					stepsArray: vm.heightSteps,
					onChange: function(){
						vm.height = vm.slider.value;
					}
				}

			}

		};

		/**
		 * Create the height steps
		 */
		vm.createHeightSteps = function () {

			vm.heightSteps = [];

			for(var i = 4; i < 7; i++){

				for(var j = 0; j < 12; j++){

					vm.heightSteps.push(i + '.' + j);
				}
			}

		};

		/**
		 * Save the height to the profile builder object
		 */
		vm.saveHeight = function(){

			//Update the profile builder object
			var profileBuilderObj = builderService.getProfileBuilderObj();

			console.log('profile builder object', profileBuilderObj);
			profileBuilderObj.setBasicStatsHeight(vm.height);

			//Go to the complete state state
			$state.transitionTo('builder.questions.basic-stats.body-type');

		};

        /**
         * Repopulate any added profile data
         */
        vm.repopulateProfileData = function () {

            //Update the profile builder object
            var profileBuilderObj = builderService.getProfileBuilderObj();


            if(profileBuilderObj.properties.questions.height != null){
                vm.height = profileBuilderObj.properties.questions.height;
            }
        };



		//Setup the controller
		vm.setUpHeightController();
	}
})();

