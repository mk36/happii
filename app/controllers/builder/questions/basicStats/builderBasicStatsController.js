(function () {

	angular
		.module('app')
		.controller('builderBasicStatsController', builderBasicStatsControllerSetUp);

	function builderBasicStatsControllerSetUp($scope, $state, $stateParams, $timeout, siteService, builderService){

		var vm = this;

		vm.subQuestionDots = [];
		vm.activeSubQuestion = 0;

        var activeSubQuestionPromise = builderService.getActiveSubQuestionPromise();

        activeSubQuestionPromise.then(null, null, function (number) {
            vm.activeSubQuestion = number;
        });

        /**
		 * Set up the builder controller
         */
		vm.setUpBuilderBasicStatsController = function(){

            siteService.setBackgroundImage('images/backgrounds/wizard/shutterstock_527455063.jpg');
            builderService.setActiveQuestion(3);

			//Setup builder dots
			vm.setUpSubDotsArray();
		};

		vm.setUpSubDotsArray = function(){

			vm.subQuestionDots = _.range(1, 10);

			$timeout(function(){

				vm.activeSubQuestion = builderService.getActiveSubQuestion();
			}, 100);


			console.log('dots', vm.subQuestionDots, 'active sub question', vm.activeSubQuestion);

		};

		vm.setUpBuilderBasicStatsController();

	}
})();

