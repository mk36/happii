(function () {

	angular
		.module('app')
		.controller('builderBasicStatsLocationFromController', builderBasicStatsLocationFromControllerSetUp);

	function builderBasicStatsLocationFromControllerSetUp($scope, $state, $stateParams, $timeout, siteService, builderService){

		var vm = this;

		vm.locationFrom = '';
		vm.validAddress = false;
		vm.locationObj = {
			address : '',
			lat: 0,
			lng: 0
		};

		/**
		 * Set up the location from controller
		 */
		vm.setUpLocationFromController = function(){

			var input = document.getElementById('location-from-input');
			var searchBox = new google.maps.places.SearchBox(input);

			//Create a listener for when a valid place is selected
			searchBox.addListener('places_changed', function() {

				var places = searchBox.getPlaces();

				places.forEach(function(place) {

					console.log('place', place);
					var latLng = place.geometry.location;

					vm.setLocationObject(place.formatted_address, latLng.lat(), latLng.lng());
				})

			});

			builderService.setActiveSubQuestion(1);

			//Repopulate any added location data
			vm.repopulateLocationData();
		};

		/**
		 * Set the location object
		 *
		 * @param address
		 * @param lat
		 * @param lng
		 */
		vm.setLocationObject = function(address, lat, lng){

			vm.locationObj.address = address;
			vm.locationObj.lat = lat;
			vm.locationObj.lng = lng;

			//Update the profile builder object
			var profileBuilderObj = builderService.getProfileBuilderObj();

			console.log('profile builder object', profileBuilderObj);
			profileBuilderObj.setBasicStatsLocationFrom(address, lat, lng);

			//The address is valid
			vm.validAddress = true;

			//console.log('account obj', accountObj);

			//Refresh the scope
			$scope.$apply();
		};

		/**
		 * Repopulate the location data
		 *
		 */
		vm.repopulateLocationData = function () {

			//Get the profile builder object
			var profileBuilderObj = builderService.getProfileBuilderObj();

			if(profileBuilderObj.properties.questions.locationFrom.address != null){

                //Repopulate the postcode stored
                vm.locationFrom = profileBuilderObj.properties.questions.locationFrom.address;

                if(vm.locationFrom.length > 0){
                    vm.locationObj = profileBuilderObj.properties.questions.locationFrom;
                    vm.validAddress = true;
                }
			}

		};

		//Setup the controller
		vm.setUpLocationFromController();
	}
})();

