(function () {

	angular
		.module('app')
		.controller('builderProfileDescriptionController', builderProfileDescriptionControllerSetUp);

	function builderProfileDescriptionControllerSetUp($scope, $state, $stateParams, $timeout, siteService, builderService){

		var vm = this;

		vm.wordLimit = 1200;
		vm.profileDescription = '';

        /**
		 * Set up the builder controller
         */
		vm.setUpBuilderProfileDescriptionController = function(){

			siteService.setBackgroundImage('images/backgrounds/wizard/shutterstock_529413469.jpg');
			builderService.setActiveQuestion(2);

            vm.repopulateProfileData();
		};



		/**
		 * Get the characters left that the user can enter
		 *
		 * @returns {Number}
		 */
		vm.getCharactersLeft = function(){

			var curLength = vm.profileDescription.length;
			var charactersLeft = vm.wordLimit - curLength;

			return charactersLeft;
		};

		/**
		 * Save the builder profile description to the profile builder
		 * object
		 */
		vm.saveBuilderProfileDescription = function(){

			var profileBuilderObj = builderService.getProfileBuilderObj();
			profileBuilderObj.setProfileDescription(vm.profileDescription);

			console.log('profile description object',profileBuilderObj);

			//Go to the location state
			$state.transitionTo('builder.questions.basic-stats.location-from');
		};

        /**
         * Repopulate any added profile data
         */
        vm.repopulateProfileData = function () {
            var profileBuilderObj = builderService.getProfileBuilderObj();


            if(profileBuilderObj.properties.profileDescription != null){
                vm.profileDescription = profileBuilderObj.properties.profileDescription;
            }
        };

		vm.setUpBuilderProfileDescriptionController();

	}
})();

