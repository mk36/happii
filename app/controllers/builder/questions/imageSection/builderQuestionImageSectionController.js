(function () {

	angular
		.module('app')
		.controller('builderQuestionImageSectionController', builderQuestionImageSectionControllerSetUp);

	function builderQuestionImageSectionControllerSetUp($scope, $state, $stateParams, $timeout, siteService, builderService){

		var vm = this;

        /**
		 * Set up the builder controller
         */
		vm.setUpBuilderQuestionImageSectionController = function(){

            siteService.setBackgroundImage('images/backgrounds/wizard/shutterstock_289166405.jpg');
            builderService.setActiveQuestion(1);
		};

		vm.setUpBuilderQuestionImageSectionController();

	}
})();

