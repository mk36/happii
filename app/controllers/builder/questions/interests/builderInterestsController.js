(function () {

	angular
		.module('app')
		.controller('builderInterestsController', builderInterestsControllerSetUp);

	function builderInterestsControllerSetUp($scope, $state, $stateParams, $timeout, siteService, builderService){

		var vm = this;

        /**
		 * Set up the builder controller
         */
		vm.setUpBuilderInterestsController = function(){

            builderService.setActiveQuestion(4);
		};

		vm.setUpBuilderInterestsController();

	}
})();

