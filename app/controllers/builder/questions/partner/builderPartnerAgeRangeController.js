(function () {

	angular
		.module('app')
		.controller('builderPartnerAgeRangeController', builderPartnerAgeRangeControllerSetUp);

	function builderPartnerAgeRangeControllerSetUp($scope, $state, $stateParams, $timeout, siteService, builderService){

		var vm = this;

        vm.selectedAgeRange = {
        	min : 18,
			max : 70
		};

        vm.gender = '';

		vm.slider = {};

        /**
		 * Set up the partner age range controller
         */
		vm.setUpBuilderPartnerAgeRangeController = function(){

			//Set the background image
           // siteService.setBackgroundImageSrc('url(images/backgrounds/animals/puffins.png)');
            vm.repopulateProfileData();

			vm.setUpGender();
			vm.setUpSlider();

			//Set the sub dots
			builderService.setActiveSubQuestion(2);
		};

		/**
		 * Set up the gender
		 */
		vm.setUpGender = function(){

			//Update the profile builder object
			var profileBuilderObj = builderService.getProfileBuilderObj();

			//Save travel been
			var gender = profileBuilderObj.getQuestionValueByQuestionReference('partnerGender');

			if(gender == 'male'){
				vm.gender = 'He';
			}
			else{
				vm.gender = 'She';
			}
		};

		/**
		 * Set up the slider
		 */
		vm.setUpSlider = function(){

			vm.slider = {
				//value: [18, 35],
				min: 18,
				max: 70,
				options: {
					floor: 18,
					ceil: 70,
					//step: 1,
					//precision: 1,
					showTicks: true,
					onChange: function(){

						vm.selectedAgeRange.min = vm.slider.min;
						vm.selectedAgeRange.max = vm.slider.max;
					}
				}
			}

		};

		/**
		 * Save the Age range object to the typescript builder object
		 */
		vm.saveAgeRangeObject = function(){

			//Update the profile builder object
			var profileBuilderObj = builderService.getProfileBuilderObj();

			console.log('profile builder object', profileBuilderObj);

			//Save travel been
			profileBuilderObj.setQuestionValueByQuestionReference('partnerAgeRange', vm.selectedAgeRange);

			//Go to the complete state state
			$state.transitionTo('builder.questions.partner.location');
		};

        /**
         * Repopulate any added profile data
         */
        vm.repopulateProfileData = function () {

            //Update the profile builder object
            var profileBuilderObj = builderService.getProfileBuilderObj();

            // console.log('profile object', profileBuilderObj);

            if(profileBuilderObj.properties.questions.partnerAgeRange != null){
                vm.selectedAgeRange = profileBuilderObj.properties.questions.partnerAgeRange;
            }
        };


		//Set up the controller
		vm.setUpBuilderPartnerAgeRangeController();
	}
})();

