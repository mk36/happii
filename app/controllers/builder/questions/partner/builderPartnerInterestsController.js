(function () {

	angular
		.module('app')
		.controller('builderPartnerInterestsController', builderPartnerInterestsControllerSetUp);

	function builderPartnerInterestsControllerSetUp($scope, $state, $stateParams, $timeout, siteService, builderService){

		var vm = this;

        /**
		 * Set up the register controller
         */
		vm.setUpBuilderPartnerInterestsController = function(){

			//Set the background image
            siteService.setBackgroundImage('images/backgrounds/wizard/shutterstock_527455063.jpg');
            builderService.setActiveQuestion(7);

		};

		vm.setUpBuilderPartnerInterestsController();

	}
})();

