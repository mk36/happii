(function () {

	angular
		.module('app')
		.controller('builderPartnerHeightController', builderPartnerHeightControllerSetUp);

	function builderPartnerHeightControllerSetUp($scope, $state, $stateParams, $timeout, siteService, builderService){

		var vm = this;

        vm.selectedHeight = {
        	min : 4,
			max : 7
		};

		vm.slider = {};

		vm.heightSteps = [];

        /**
		 * Set up the partner age range controller
         */
		vm.setUpBuilderPartnerHeightController = function(){

			//Create the steps array
			vm.createHeightSteps();

            vm.repopulateProfileData();

			//Set the background image
           // siteService.setBackgroundImageSrc('url(images/backgrounds/animals/puffins.png)');
			vm.setUpSlider();

			//Set the sub dots
			builderService.setActiveSubQuestion(7);

		};

		/**
		 * Set up the slider
		 */
		vm.setUpSlider = function(){

			vm.slider = {
				//value: [18, 35],
				min: 4,
				max: 7,
				options: {
					floor: 4,
					ceil: 7,
					step: 0.1,
					precision: 1,
					showTicks: true,
					stepsArray: vm.heightSteps,
					onChange: function(){

						vm.selectedHeight.min = vm.slider.min;
						vm.selectedHeight.max = vm.slider.max;
					}
				}
			};

		};

		/**
		 * Save the Age range object to the typescript builder object
		 */
		vm.saveHeightObject = function(){

			//Update the profile builder object
			var profileBuilderObj = builderService.getProfileBuilderObj();

			console.log('profile builder object', profileBuilderObj);

			//Save travel been
			profileBuilderObj.setQuestionValueByQuestionReference('partnerHeight', vm.selectedHeight);

			//Go to the complete state state
			$state.transitionTo('builder.questions.partner.education');
		};

		/**
		 * Create the height steps
		 */
		vm.createHeightSteps = function () {

			vm.heightSteps = [];

			for(var i = 4; i < 7; i++){

				for(var j = 0; j < 12; j++){

					vm.heightSteps.push(i + '.' + j);
				}
			}

		};

        /**
         * Repopulate any added profile data
         */
        vm.repopulateProfileData = function () {

            //Update the profile builder object
            var profileBuilderObj = builderService.getProfileBuilderObj();

            if(profileBuilderObj.properties.questions.partnerHeight != null){
                vm.selectedHeight = profileBuilderObj.properties.questions.partnerHeight;
            }
        };


		//Set up the controller
		vm.setUpBuilderPartnerHeightController();
	}
})();

