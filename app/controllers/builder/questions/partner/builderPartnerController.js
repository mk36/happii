(function () {

	angular
		.module('app')
		.controller('builderPartnerController', builderPartnerControllerSetUp);

	function builderPartnerControllerSetUp($scope, $state, $stateParams, $timeout, siteService, builderService){

		var vm = this;

		vm.subQuestionDots = [];
		vm.activeSubQuestion = 0;

        var activeSubQuestionPromise = builderService.getActiveSubQuestionPromise();

        activeSubQuestionPromise.then(null, null, function (number) {
            vm.activeSubQuestion = number;
        });

        /**
		 * Set up the register controller
         */
		vm.setUpBuilderPartnerController = function(){

			$timeout(function () {
				//Set the background image
				siteService.setBackgroundImage('images/backgrounds/wizard/shutterstock_400220989.jpg');
			}, 200);

            builderService.setActiveQuestion(6);

            //Set up the dots array
			vm.setUpSubDotsArray();

		};

		vm.setUpSubDotsArray = function(){

			vm.subQuestionDots = _.range(1, 12);

			$timeout(function(){

				vm.activeSubQuestion = builderService.getActiveSubQuestion();
			}, 100);


			console.log('dots', vm.subQuestionDots, 'active sub question', vm.activeSubQuestion);

		};

		vm.setUpBuilderPartnerController();

	}
})();

