(function () {

	angular
		.module('app')
		.controller('builderPartnerLocationController', builderPartnerLocationControllerSetUp);

	function builderPartnerLocationControllerSetUp($scope, $state, $stateParams, $timeout, siteService, builderService, registerService){

		var vm = this;

       	vm.location = {
       		name : '',
			lat : 0,
			lng : 0,
			miles : 5
		};

		vm.slider = {};

        /**
		 * Set up the partner age range controller
         */
		vm.setUpBuilderPartnerLocationController = function(){

			//Set the background image
           // siteService.setBackgroundImageSrc('url(images/backgrounds/animals/puffins.png)');

			vm.setUpGooglePlacesInput();
			vm.setUpSlider();

			//Set the sub dots
			builderService.setActiveSubQuestion(3);

            vm.repopulateLocationData();

		};

		vm.setUpGooglePlacesInput = function(){

			var input = document.getElementById('location-partner-input');


			//todo remove options for live version without london postcode limit
			var londonBounds = new google.maps.LatLngBounds(new google.maps.LatLng(51.332757,-0.475159),new google.maps.LatLng(51.680219,0.191574));

			var options = {
				bounds: londonBounds,
				types:['geocode']
			};

			var searchBox = new google.maps.places.Autocomplete(input, options);

			google.maps.event.addListener(searchBox, 'place_changed',function(){
				console.log('hello I have changed', searchBox);

				var place = searchBox.getPlace();
				var latLng = place.geometry.location;
				vm.setLocationObject(place.formatted_address, latLng.lat(), latLng.lng());

			});
		};

		/**
		 * Set the location object
		 *
		 * @param name
		 * @param lat
		 * @param lng
		 */
		vm.setLocationObject = function(name, lat, lng){

			vm.location.name = name;
			vm.location.lat = lat;
			vm.location.lng = lng;

			console.log('location name', vm.location.name);

			$scope.$apply();
		};

		/**
		 * Set up the slider
		 */
		vm.setUpSlider = function(){

			vm.slider = {
				value: 5,
				options: {
					floor: 5,
					ceil: 50,
					showTicks: true,
					onChange: function(){

						vm.location.miles = vm.slider.value;
					}
				}
			}

		};

		/**
		 * Save the Age range object to the typescript builder object
		 */
		vm.savePartnerLocationObject = function(){

			//Update the profile builder object
			var profileBuilderObj = builderService.getProfileBuilderObj();

			console.log('profile builder object', profileBuilderObj);

			//Save travel been
			profileBuilderObj.setQuestionValueByQuestionReference('partnerLocation', vm.location);

			//Go to the complete state state
			$state.transitionTo('builder.questions.partner.children');
		};

        /**
         * Repopulate the location data
         *
         */
        vm.repopulateLocationData = function () {

            //Get the profile builder object
            var profileBuilderObj = builderService.getProfileBuilderObj();

            if(profileBuilderObj.properties.questions.partnerLocation != null){

                //Repopulate the postcode stored
                vm.location = profileBuilderObj.properties.questions.partnerLocation;

                if(vm.location.length > 0){
                    vm.locationObj = profileBuilderObj.properties.questions.partnerLocation;
                    vm.validAddress = true;
                }
            }

        };


		//Set up the controller
		vm.setUpBuilderPartnerLocationController();
	}
})();

