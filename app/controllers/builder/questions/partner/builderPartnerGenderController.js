(function () {

	angular
		.module('app')
		.controller('builderPartnerGenderController', builderPartnerGenderControllerSetUp);

	function builderPartnerGenderControllerSetUp($scope, $state, $stateParams, $timeout, siteService, builderService){

		var vm = this;

        vm.selectedGender = '';

		console.log('BuilderPartner gender controller setup');

        /**
		 * Set up the register controller
         */
		vm.setUpBuilderPartnerGenderController = function(){

			//Set the background image
			siteService.setBackgroundImage('images/backgrounds/wizard/shutterstock_459968980.jpg');

			//set sub question
			builderService.setActiveQuestion(6);
			builderService.setActiveSubQuestion(1);

			vm.repopulateProfileData();

		};

        /**
         * Set the selected gender
         * @param gender
         */
        vm.setSelectedGender = function(gender){

        	console.log('set gender', gender);

            //Set the gender locally
            vm.selectedGender = gender;

			// BG Image
			if (gender === "male") {
				//siteService.setBackgroundImage('images/backgrounds/animals/lion.jpg');
			} else {
				//siteService.setBackgroundImage('images/backgrounds/animals/lioness.jpg');
			}

            //Update the gender object and the local storage object
			vm.saveGenderObject();
        };

		/**
		 * Save the gender object
		 */
		vm.saveGenderObject = function(){

			//Update the profile builder object
			var profileBuilderObj = builderService.getProfileBuilderObj();

			console.log('profile builder object', profileBuilderObj);

			//Save travel been
			profileBuilderObj.setQuestionValueByQuestionReference('partnerGender', vm.selectedGender);
		};

        /**
         * Repopulate any added profile data
         */
        vm.repopulateProfileData = function () {

            //Update the profile builder object
            var profileBuilderObj = builderService.getProfileBuilderObj();

           // console.log('profile object', profileBuilderObj);

            if(profileBuilderObj.properties.questions.partnerGender != null){
                vm.selectedGender = profileBuilderObj.properties.questions.partnerGender;
            }
        };


		//Set up the controller
		vm.setUpBuilderPartnerGenderController();
	}
})();

