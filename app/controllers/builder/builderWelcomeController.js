(function () {

	angular
		.module('app')
		.controller('builderWelcomeController', builderWelcomeControllerSetUp);

	function builderWelcomeControllerSetUp($scope, $state, $stateParams, $timeout, siteService, builderService, loginService, profileService, alertService){

		var vm = this;
		vm.name = '';


        /**
		 * Set up the builder controller
         */
		vm.setUpBuilderWelcomeController = function(){

			//Set the background image
            siteService.setBackgroundImage('images/backgrounds/wizard/horse.jpg');

            profileService.getUserAccountDetails().then(function (data) {

            	console.log('got user account details');

            	vm.name = data.data.firstName;


			}).catch(function (data) {
				console.log('failed to get user account details');

				$state.go('sign-in');
				alertService.triggerAlert('Could not get account details, please sign in');
			})


		};

		vm.setUpBuilderWelcomeController();

	}
})();

