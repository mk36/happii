(function () {

    angular
        .module('app')
        .directive("headerDirective", headerDirectiveSetUp);


    function headerDirectiveSetUp($timeout){


        var directive = {

            templateUrl : 'app/views/directives/header.html',

            controller: headerDirectiveController,

            controllerAs: 'vm',
            bindToController: true,

            scope: {
				//iconname : '='
            }
        };

        return directive;



        /**
         *	Set up controller
         */
        function headerDirectiveController ($scope, $state, questionService, builderService, profileService, loginService) {

            var vm = this;

            vm.signInText = 'Sign In';

			$scope.$watch('vm.signInText', function(value, old) {

				console.log('old value', old, 'new value', value);
			}, true);

         	vm.setUpHeaderDirective = function () {

         		console.log('header directive setup');
				vm.checkUserhasLoggedIn();
			};

         	vm.checkUserhasLoggedIn = function () {

         		profileService.getUserAccountDetails().then(function (data) {

         			console.log('profile service data', data);
         			if(data.user.email){

						vm.signInText = 'Sign Out';

						console.log('sign in text', vm.signInText);
					}
					else{
         				vm.signInText = 'Sign In';
					}
				});
			};

			/**
			 * Manage what happens when the user clicks
			 * on the sign in/out button
			 */
			vm.manageSignInOutBtnClick = function () {

				console.log('attempting to sign in / out');

				if(vm.signInText == 'Sign Out'){
					loginService.logOut().then(function (data) {

						$state.go('sign-in');
					});
				}
				else{
					$state.go('sign-in');
				}
			};

			/**
			 * Navigate to a page
			 *
			 * @param location
			 */
			vm.navigateTo = function (location) {

				var path = window.location.origin + window.location.pathname + '#/' + location;
				window.location = path;
			};


			vm.setUpHeaderDirective();

        }
    }

})();




