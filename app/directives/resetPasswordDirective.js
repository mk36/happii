(function () {

    angular
        .module('app')
        .directive("resetPasswordDirective", resetPasswordDirectiveSetUp);


    function resetPasswordDirectiveSetUp($timeout){


        var directive = {

            templateUrl : 'app/views/directives/resetPasswordModal.html',

            controller: resetPasswordController,

            controllerAs: 'vm',
            bindToController: true,

            scope: {
				//iconname : '='
            }
        };

        return directive;



        /**
         *	Set up controller
         */
        function resetPasswordController ($scope, $state, questionService, builderService, loginService) {

            var vm = this;

            vm.email = '';

            vm.resetPassword = function () {

				loginService.request(vm.email);
			};


			/**
			 * Check if the email is valid
			 * @returns {boolean}
			 */
			vm.checkEmailIsValid = function(){

				var pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

				return pattern.test(vm.email);

			};



        }
    }

})();




