(function () {

	angular
		.module('app')
		.directive("fancyCheckbox", fancyCheckboxController);


	function fancyCheckboxController($timeout){

		var directive = {

			template : '<div class="directive-fancy-checkbox"><div class="checkbox" ng-class="{\'ticked\': checked}"><div class="checkbox-tick" ng-show="checked"></div></div><ng-transclude></ng-transclude></div>',
			transclude: true,
			link: fancyCheckbox,
			scope: {
				blur: "="
			}
		};

		return directive;



		/**
		 *	Set up controller
		 */
		function fancyCheckbox (scope, elem, attrs, $timeout) {


			var checkbox =  elem.find('input')[0];

			scope.checkbox = checkbox;
			scope.checked = checkbox.checked;

			scope.$watch('checkbox.checked', function (nv, ov) {

				scope.checked = checkbox.checked;
			});


		}
	}

})();




