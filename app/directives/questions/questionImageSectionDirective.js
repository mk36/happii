(function () {

    angular
        .module('app')
        .directive("questionImageSectionDirective", questionImageSectionSetUp);


    function questionImageSectionSetUp($timeout){


        var directive = {

            templateUrl : 'app/views/directives/questionImageSection.html',

            controller: questionImageSectionController,

            controllerAs: 'vm',
            bindToController: true,

            scope: {

                title: '=',
                subtext: '=',
                dotno: '=',
                navigateto: '=',
                navigatefrom: '=',
                apiref: '=',
                galleryno: '=',
                crop: '=',
                aspectratio: '=',
                allowedformat: '='
            }
        };

        return directive;



        /**
         *	Set up controller
         */
        function questionImageSectionController ($scope, $state, $stateParams, $timeout, siteService, builderService, questionService, $auth, authService, loginService) {

            var vm = this;

            vm.profileImageContent = {name : '', type : ''};
            vm.showImageEditor = false;

            vm.socialPhotos = [];
            vm.activeSocialPhoto = {name : '', type : ''};
            vm.accessTokens = {
                facebook : '',
                instagram : ''
            };

            vm.galleryPhotos = [];
            vm.galleryVideos = [];
            vm.photoError = '';

            vm.photoMode = 'upload';

            vm.dots = ['1','2','3','4', '5'];

            vm.reuploadText = 'Reupload photo';

            vm.newFaveVideo = {

                name : 'video',
                src : ''
            };


            /**
             * Authorise facebook login
             */
            vm.authFacebook = function(){

                console.log('called oauth facebook');



				authService.outhLogin('facebook').then(function (url) {

					//console.log('instagram data received', data);
					window.oauthCallback = function () {

						authService.outhSocialUser('facebook').then(function (userData) {

							console.log('facebook user data', userData);

							authService.getFacebookUserPhotos(userData.data.token).then(function (data) {

							   console.log('facebook photo sucess', data);

								var photoData = data;

							   //Iterate through data and add type
							   for(var i = 0; i < photoData.length; i++){

							       photoData[i].type = 'facebook';
							   }

							   vm.socialPhotos = photoData;
							   console.log('social photos', vm.socialPhotos);

							   $("#facebook-pic-modal").modal({backdrop: false});
							});
						});
					};

					window.open(url);

				});

            };

            /**
             * Authorise facebook login
             */
            vm.authInstagram = function(){

				authService.outhLogin('instagram').then(function (url) {

					//console.log('instagram data received', data);
					window.oauthCallback = function () {

						authService.outhSocialUser('instagram').then(function (userData) {

							console.log('instagram user data', userData);

							authService.getInstagramUserPhotos(userData.data.token).then(function (data) {

								console.log('instagram photo success', data);

								var photoData = data;

								//Iterate through data and add type
								for(var i = 0; i < photoData.length; i++){

									photoData[i].type = 'instagram';
								}

								vm.socialPhotos = photoData;
								console.log('social photos', vm.socialPhotos);

								$("#facebook-pic-modal").modal({backdrop: false});
							});

						});
					};

					window.open(url);

				});


            };

            /**
             * Set up the builder controller
             */
            vm.setUpBuilderQuestionImageSectionProfilePhotoController = function(){

                if(vm.apiref == 'profileVideos'){
                    vm.reuploadText = 'Reupload video';
                }

                siteService.setBackgroundImage('images/backgrounds/wizard/shutterstock_289166405.jpg');

                $timeout(function(){

                    vm.setUpDropzone();

                    vm.repopulateImageContent();

                }, 100);

            };

            /**
             * Set up drop zone for drag and drop file uploading
             */
            vm.setUpDropzone = function(){

                console.log('dropzone set up');

                /**
                 * Dropzone image upload
                 */
                $("button#photo-upload-dropzone").dropzone({ url: window.server_path + "upload/", paramName: "file", // The name that will be used to transfer the file
                    maxFilesize: 5, // MB
                    //maxFiles: 1,
                    //uploadMultiple: false,
                    clickable: true,
                    acceptedFiles: vm.allowedformat,
                    reject: function (file) {

                        console.log('rejected file', file);
                        vm.photoError = 'File too large, please upload a smaller file';
                    },
                    accept: function(file, done) {

                        //console.log('accepted image');
                        vm.photoError = '';

                        var trimmedName = file.name.replace(/ /g,'');

                        //Create the image upload object
                        var imageUploadObj = {
                            form : file,
                            fileName : trimmedName
                        };

                        //Upload th eimage to the server
                        questionService.uploadImageToServer(imageUploadObj, file.name).then(function(data){

                            //console.log('image path returned', data.data[0].file_name);

                            vm.profileImageContent.name = data.data[0].file_name;
                            vm.profileImageContent.type = 'server';

                            vm.showImageEditor = true;
                            //console.log('server image path', vm.profileImageContent);

                            //If this is a gallery upload then upload straight to the gallery
                            if(vm.apiref == 'profileGallery'){
                                vm.updateBuilderGalleryObject(vm.profileImageContent);
                            }

                            if(vm.apiref == 'profileVideos'){
                                vm.updateBuilderGalleryObject(vm.profileImageContent);
                            }
                        });
                    }});
            };

            /**
             *	Call a click event on the dropzone button so the device
             *  file browser cna be opened
             */
            vm.triggerDropZoneForProfileImage = function(){

                $("#photo-upload-display").click();
            };

            /**
             * Set the social image to edit
             *
             * requires the url of the image to edit and the modal they
             * are contained in to close
             *
             * @param image
             * @param modal
             */
            vm.setSocialImageToEdit = function(image, modal){

                //close the modal
                $(modal).modal("hide");

                //Update the active profile photo
                vm.profileImageContent = image;

                //Reset the active social image
                vm.activeSocialPhoto = {name : ''};

                //Show the image editor
                vm.showImageEditor = true;
            };

            /**
             * Update the builder typescript object
             * @param imageObj
             * @param option
             */
            vm.updateBuilderObject = function(imageObj, option){

                console.log('callback image edit called double', imageObj);

                //Update the profile builder object
                var profileBuilderObj = builderService.getProfileBuilderObj();

                console.log('profile builder object', profileBuilderObj, 'option', option);

                profileBuilderObj.setQuestionValueByQuestionReference(vm.apiref, imageObj);

                //Update the builder gallery
                vm.updateBuilderGalleryObject(imageObj, 'image');
            };

            /**
             * update the builder gallery object
             *
             * @param imageObj
             */
            vm.updateBuilderGalleryObject = function(imageObj, type){

                console.log('update builder gallery object', imageObj);

                //set the date uploaded
				var date = new Date();
				date = date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();
				imageObj.date = date;

                //Update the profile builder object
                var profileBuilderObj = builderService.getProfileBuilderObj();

                //Change the builder mode to confirm
                vm.photoMode = 'confirm';

                if(type == 'image'){
                    //Add the image object to the gallery
                    vm.galleryPhotos.push(imageObj);

                    //Save the gallery
                    profileBuilderObj.setQuestionValueByQuestionReference('profileGallery', vm.galleryPhotos);
                }
                else{
                    //Add the image object to the gallery
                    vm.galleryVideos.push(imageObj);

                    //Save the gallery
                    profileBuilderObj.setQuestionValueByQuestionReference('profileVideos', vm.galleryVideos);
                }

            };

            /**
             * Add favourite video to new video
             */
            vm.addFavouriteVideo = function () {

                if(vm.newFaveVideo.src.length > 0){

                    vm.updateBuilderGalleryObject(vm.newFaveVideo, 'video');
                    vm.newFaveVideo = {name : '', src : '', type: 'url'};
                }

                //Reset the new fave video
                vm.newFaveVideo = '';
            };

            /**
             * Get the image path
             *
             * @param imageName
             */
            vm.getImagePath = function(imageObj){

                var path = '';

                if(imageObj.type == 'server'){

                    path = window.server_path + 'image/' + imageObj.name;
                }
                else if (imageObj.type == 'facebook'){

                    path = imageObj.src;
                }

                return path;
            };

            /**
             * Repopulate the photo data
             */
            vm.repopulateImageContent = function () {

                //Get the profile builder object
                var profileBuilderObj = builderService.getProfileBuilderObj();

                //console.log('repopulate profile builder object', profileBuilderObj);
                vm.galleryPhotos = profileBuilderObj.properties.questions.profileGallery;
                vm.galleryVideos = profileBuilderObj.properties.questions.profileVideos;

                //If you are setting a gallery photo, get the required stuff
                if(vm.galleryno == 2 || vm.galleryno == 3){

                    if(vm.galleryPhotos[vm.galleryno] != null){
                        vm.profileImageContent = vm.galleryPhotos[vm.galleryno];
                        vm.photoMode = 'confirm';
                    }
                }
                else{
                    vm.profileImageContent = profileBuilderObj.properties.questions[vm.apiref];

                    if(profileBuilderObj.properties.questions[vm.apiref].name != null){
                        vm.photoMode = 'confirm';
                    }
                }


            };



            vm.setUpBuilderQuestionImageSectionProfilePhotoController();


        }
    }

})();




