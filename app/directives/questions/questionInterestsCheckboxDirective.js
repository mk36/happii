(function () {

    angular
        .module('app')
        .directive("questionInterestsCheckboxDirective", questionInterestsCheckboxDirective);


    function questionInterestsCheckboxDirective($timeout){


        var directive = {

            templateUrl : 'app/views/directives/questionInterestsCheckbox.html',

            controller: questionInterestsCheckboxController,

            controllerAs: 'vm',
            bindToController: true,

            scope: {

                title: '=',
                subtitle: '=',
				backgroundimage: '=',
                columns: '=',
                questionref: '=',
                navigateto: '=',
                navigatefrom: '=',
                apiref: '=',
				minvaluesneeded: '=',
				maxvaluesneeded: '='
            }
        };

        return directive;


        /**
         *	Set up controller
         */
        function questionInterestsCheckboxController ($scope, $state, questionService, builderService, siteService) {

            var vm = this;

			vm.dataOptions = [];
			vm.selectedOptions = [];

			/**
			 * Set up the builder controller
			 */
			vm.setUpBuilderCheckboxController = function(){

				$timeout(function(){

					siteService.setBackgroundImage(vm.backgroundimage);
					vm.setUpQuestionDataToDisplay();

					vm.repopulateProfileData();

				}, 100);


			};

			/**
			 * Set up the data to show in the radio buttons
			 */
			vm.setUpQuestionDataToDisplay = function(){

				questionService.getQuestionData(vm.apiref).then(function(data){

					//console.log('got question data', data);

					//vm.dataOptions = data.data.options;
                    vm.createKeysFromData(data.data.options);

					//console.log('data to display', vm.dataToDisplay);
				}, function(){
					console.log('failed to get question data');
				});
			};

            /**
			 * Create keys from the data
			 *
             * @param data
             */
			vm.createKeysFromData = function (data) {

				for(var i = 0; i < data.length; i++){

					var obj = {
						name : data[i],
						key : data[i].substring(0, 4)
                	};

                    vm.dataOptions.push(obj);
				}
            };


			/**
			 * Check if a spare column is needed to be added
			 *
			 * fixes the awkward 5 column layout
			 *
			 * @param index
			 * @returns {boolean}
			 */
			vm.checkIfNeedsSpareColumns = function(index){

				var startArray =[0, 5, 10, 15, 20, 25, 30, 35, 40];

				var index = _.indexOf(startArray, index);

				if(index > -1) {

					return true;
				}

				return false;
			};

			/**
			 * Set the checkbox option in the selectedCheckbox array
			 *
			 * Removes it from the array if the checkbox is unchecked
			 *
			 * @param value
			 */
			vm.setCheckBoxOption = function(value){

				console.log('value', value);

				if(vm.selectedOptions.includes(value)){

					//Remove the checked element from the selected option array
					_.pull(vm.selectedOptions , value);

				}
				else{
					//Add the checked element to the selected option array
					vm.selectedOptions.push(value);
				}

				console.log('selected personality options', vm.selectedOptions);
			};

			/**
			 * Save the checkbox details
			 */
			vm.saveCheckboxDetails = function(){

				//Update the profile builder object
				var profileBuilderObj = builderService.getProfileBuilderObj();

				console.log('profile builder object', profileBuilderObj);

				profileBuilderObj.setQuestionValueByQuestionReference(vm.questionref, vm.selectedOptions);

				//Go to the next state
				$state.transitionTo(vm.navigateto);
			};

            /**
			 * Add any existing profile data which has been set
             */
			vm.repopulateProfileData = function () {

                var profileBuilderObj = builderService.getProfileBuilderObj();
                vm.selectedOptions = profileBuilderObj.properties.questions[vm.questionref];

                console.log('chosen options', vm.selectedOptions);

               $timeout(function () {
                   vm.reTickCheckBoxes();
               }, 500);
            };

            /**
			 * Retick the checkboxes
             */
			vm.reTickCheckBoxes = function () {

                for(var i = 0; i < vm.selectedOptions.length; i++){

                    var input = $('#' + 'input' + vm.selectedOptions[i].substring(0, 4));
                    $(input).prop('checked', true);
                }
            };

			vm.setUpBuilderCheckboxController();
        }
    }

})();




