(function () {

	angular
		.module('app')
		.directive("questionInterestsFavouriteDirective", questionInterestsFavouriteSetUp);


	function questionInterestsFavouriteSetUp($timeout){


		var directive = {

			templateUrl : 'app/views/directives/questionInterestsFavourites.html',

			controller: questionInterestsFavouriteController,

			controllerAs: 'vm',
			bindToController: true,

			scope: {

				title: '=',
				backgroundimage: '=',
				innertitle: '=',
				questionref: '=',
				navigateto: '=',
				navigatefrom: '=',
				apiref: '=',
				listplaceholder: '='
			}
		};

		return directive;



		/**
		 *	Set up controller
		 */
		function questionInterestsFavouriteController ($scope, $state, $q, $http, questionService, builderService, siteService) {

			var vm = this;

			vm.dataToDisplay = {};
			vm.selectedOptions = [];
			vm.selectedItem = '';

			vm.apiResults = [];

			//vm.apiResults
			$scope.$watch('vm.apiResults', function(value, old) {}, true);

			vm.favouritePlaceholderItems = [];

			vm.setUpQuestionInterestsFavouriteDirective = function(){

				$timeout(function(){
					//console.log('title passed', vm.title, 'icon', vm.icon, 'columns', vm.columns, 'question ref', vm.questionref);

					vm.setUpQuestionDataToDisplay();
					vm.setUpFavouritePlaceholderItems();

					siteService.setBackgroundImage(vm.backgroundimage);

					vm.repopulateProfileData();

				}, 100);
			};

			/**
			 * Manage any Api's that need to be setup
			 */
			vm.manageAPiSearch = function () {


				if(vm.apiref == 'books'){
					vm.searchBooksApi(vm.selectedItem);
				}
				else if(vm.apiref == 'music'){
					vm.searchMusicApi(vm.selectedItem);
				}
				else if(vm.apiref == 'viewings'){
					vm.searchFilmApi(vm.selectedItem);
				}
				else if(vm.apiref == 'sport'){
					vm.searchSportApi(vm.selectedItem);
				}

			};

			/**
			 * Set up the favourite items placeholder
			 */
			vm.setUpFavouritePlaceholderItems = function(){

				vm.favouritePlaceholderItems = [{title: vm.listplaceholder + " 1"},
					{title: vm.listplaceholder + " 2"},
					{title: vm.listplaceholder + " 3"},
					{title: vm.listplaceholder + " 4" },
					{title: vm.listplaceholder + " 5"}];
			};

			/**
			 * Set up the data to show in the radio buttons
			 */
			vm.setUpQuestionDataToDisplay = function(){

				questionService.getQuestionData(vm.questionref).then(function(data){

					//console.log('got question data', data);

					vm.dataToDisplay = data.data.sports;

					//console.log('data to display', vm.dataToDisplay);
				}, function(){
					console.log('failed to get question data');
				});
			};

			/**
			 * Add a favourite item to the selected options array
			 *
			 * @param item
			 */
			vm.addFavouriteItem = function(item){

				var index = _.indexOf(vm.selectedOptions, item);


				console.log('selectedOptions', vm.selectedOptions, 'item passed', item);

				if(index < 0 && vm.selectedOptions.length < 5 && item != "" && vm.checkIfItemExists(item) == false) {

					vm.selectedOptions.push(item);
				}

				//Reset selected item
				vm.selectedItem = '';
			};

			/**
			 * Check if an item exists in the array
			 *
			 * @param item
			 * @returns {boolean}
			 */
			vm.checkIfItemExists = function (item) {

				for(var i = 0; i < vm.selectedOptions.length; i++){

					if(vm.apiref == 'books'){
						if(vm.selectedOptions[i].title == item.title){
							return true;
						}
					}
					else if(vm.apiref == 'sport'){
						if(vm.selectedOptions[i].name == item.name){
							return true;
						}
					}
					else{
						if(vm.selectedOptions[i].id == item.id){
							return true;
						}
					}
				}

				return false;
			};

			/**
			 * If the enter key was pressed, add the item to the list
			 *
			 * @param e
			 */
			vm.checkEnterKey = function(e){

				if (e.keyCode == 13) {
					vm.addFavouriteItem(vm.selectedItem);
				}

			};

			/**
			 * Remove a favourite item from the selected options array
			 * @param item
			 */
			vm.removeFavouriteItem = function(item){

				//Remove the item from the selected option array
				_.pull(vm.selectedOptions , item);
			};



			/**
			 * Save the selection to the typescript object
			 *
			 */
			vm.saveSelection = function(){

				//console.log('selected', vm.selected);

				//Update the profile builder object
				var profileBuilderObj = builderService.getProfileBuilderObj();

				console.log('profile builder object', profileBuilderObj);

				profileBuilderObj.setQuestionValueByQuestionReference(vm.questionref, vm.selectedOptions);

				//Go to the next state
				$state.transitionTo(vm.navigateto);

			};

			/**
			 * Repopulate any added profile data
			 */
			vm.repopulateProfileData = function () {

				var profileBuilderObj = builderService.getProfileBuilderObj();

				if(profileBuilderObj.properties.questions[vm.questionref] != null){
					vm.selectedOptions = profileBuilderObj.properties.questions[vm.questionref];
				}
			};

			/**
			 * Search the books api
			 */
			vm.searchBooksApi = function (text) {

				console.log('searching books api...');

				var query = text.replace(" ", "+");

				// Set the api variable
				//var googleAPI = "https://www.googleapis.com/books/v1/volumes?q=harry+potter";
				var googleAPI = "https://www.googleapis.com/books/v1/volumes?q=" + query;

				// Make a ajax call to get the json data as response.
				$.getJSON(googleAPI, function (response) {

					// In console, you can see the response objects
					console.log("JSON Data: ", response.items);
					vm.apiResults = response.items;

					$scope.$apply();

				});
			};

			/**
			 * Search the books api
			 */
			vm.searchMusicApi = function (text) {

				console.log('searching music api...');

				var query = text.replace(" ", "+");

				// Set the api variable
				var api = "https://api.spotify.com/v1/search?q=" + query + "&type=track";

				// Make a ajax call to get the json data as response.
				$.getJSON(api, function (response) {

					// In console, you can see the response objects
					console.log("JSON Data: ", response.tracks.items);
					vm.apiResults = response.tracks.items;

					$scope.$apply();
				});
			};

			/**
			 * Search the books api
			 */
			vm.searchFilmApi = function (text) {

				console.log('searching film api...');

				var query = text.replace(" ", "+");

				// Set the api variable
				var api = "https://api.themoviedb.org/3/search/movie?api_key=fd7ca7a1608af4290c2ba21a4bebfc69&query=" + query;

				//movie/id/videos

				// Make a ajax call to get the json data as response.
				$.getJSON(api, function (response) {

					// In console, you can see the response objects
					console.log("JSON Data: ", response);
					//console.log("JSON Data: ", response.Search);
					vm.apiResults = response.results;

				});
			};

			/**
			 * Search the books api
			 */
			vm.searchSportApi = function (text) {

				console.log('searching sport api...', vm.dataToDisplay);

				var results = [];

				for(var i = 0; i < vm.dataToDisplay.length; i++){

					var curText = vm.dataToDisplay[i].name.toLowerCase();

					if(curText.includes(text)){

						results.push(vm.dataToDisplay[i]);
					}
				}

				vm.apiResults = results;

				console.log('api results', vm.apiResults);
			};

			/**
			 * Set up the trailer to watch
			 */
			vm.watchFilmTrailer = function (video) {

				console.log('trailer', video);

				var videoId = video.id;

				console.log('searching film api...');

				// Set the api variable
				var api = "https://api.themoviedb.org/3/movie/" + videoId + "/videos?api_key=fd7ca7a1608af4290c2ba21a4bebfc69";

				// Make a ajax call to get the json data as response.
				$.getJSON(api, function (response) {

					// In console, you can see the response objects
					console.log("JSON Data: ", response);

					vm.setUpTrailerToPlay(response.results[0].key);

				});




			};

			vm.setUpTrailerToPlay = function (videoId) {

				if (typeof(YT) == 'undefined' || typeof(YT.Player) == 'undefined') {

					//Create a script
					var tag = document.createElement('script');
					tag.src = "https://www.youtube.com/iframe_api";

					var firstScriptTag = document.getElementsByTagName('script')[0];

					firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

					window.onYouTubePlayerAPIReady = function() {

						onYouTubePlayer(videoId);
					};

				}else {
					onYouTubePlayer(videoId);
				}
			};

			vm.player = {};

			function onYouTubePlayer(videoId) {

				vm.player = new YT.Player('player', {
					height: '100%',
					width: '100%',
					videoId: videoId,
					playerVars: { controls:1, showinfo: 0, rel: 0, showsearch: 0, iv_load_policy: 3 },
					events: {
						'onReady': onPlayerReady
					}

				});
			}

			function onPlayerReady(event) {
				event.target.playVideo();
			}


			vm.setUpQuestionInterestsFavouriteDirective();

		}
	}

})();