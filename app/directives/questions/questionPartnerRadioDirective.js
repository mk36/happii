(function () {

    angular
        .module('app')
        .directive("questionPartnerRadioDirective", questionPartnerRadioSetUp);


    function questionPartnerRadioSetUp($timeout){


        var directive = {

            templateUrl : 'app/views/directives/questionPartnerRadioDirective.html',

            controller: questionPartnerRadioController,

            controllerAs: 'vm',
            bindToController: true,

            scope: {

                title: '=',
                icon: '=',
                columns: '=',
                questionref: '=',
                navigateto: '=',
                navigatefrom: '=',
                apiref: '=',
				subquestion: '='
            }
        };

        return directive;



        /**
         *	Set up controller
         */
        function questionPartnerRadioController ($state, questionService, builderService) {

            var vm = this;

            vm.dataToDisplay = {};
			vm.selected = '';

            vm.setUpQuestionPartnerDirective = function(){

                $timeout(function(){
					//console.log('title passed', vm.title, 'icon', vm.icon, 'columns', vm.columns, 'question ref', vm.questionref);

					vm.setUpQuestionDataToDisplay();

                    vm.repopulateProfileData();

                }, 100);

				//Set the subquestion
				builderService.setActiveSubQuestion(_.parseInt(vm.subquestion));
            };

			/**
             * Set up the data to show in the radio buttons
			 */
			vm.setUpQuestionDataToDisplay = function(){

				questionService.getQuestionData(vm.apiref).then(function(data){

					//console.log('got question data', data);

					vm.dataToDisplay = data.data.options;

					//console.log('data to display', vm.dataToDisplay);
				}, function(){
					console.log('failed to get question data');
				});
            };

			/**
             * Save the radio selection to the typescript object
             *
			 */
			vm.saveRadioSelection = function(){

			    //console.log('selected', vm.selected);

				//Update the profile builder object
				var profileBuilderObj = builderService.getProfileBuilderObj();

				console.log('profile builder object', profileBuilderObj);

				profileBuilderObj.setQuestionValueByQuestionReference(vm.questionref, vm.selected);

			};

            /**
             * Repopulate any added profile data
             */
            vm.repopulateProfileData = function () {

                var profileBuilderObj = builderService.getProfileBuilderObj();

                if(profileBuilderObj.properties.questions[vm.questionref] != null){
                    vm.selected = profileBuilderObj.properties.questions[vm.questionref];
                }
            };


			vm.setUpQuestionPartnerDirective();

        }
    }

})();




