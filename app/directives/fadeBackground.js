(function () {

	angular
		.module('app')
		.directive("fadeBackground", fadeBackgroundSetUp);


	function fadeBackgroundSetUp(){

		var directive = {

			template :
				'<div class="directive-fade-background">' +
				'	<div class="image-wrap">' +
				'		<div class="image-front" ng-style="vm.imageFront"></div>' +
				'		<div class="image-back" ng-style="vm.imageBack"></div>' +
				'	</div>' +
				'	<div class="inner-wrap"><ng-transclude></ng-transclude></div>' +
				'</div>',

			controller: fadeBackgroundController,
			controllerAs: 'vm',
			bindToController: true,
			transclude: true,
			scope: {
				blur: "="
			}
		};

		return directive;



		/**
		 *	Set up controller
		 */
		function fadeBackgroundController ($timeout, siteService, $q) {

			var vm = this,
				stack = [],			// Images to change to
				delay = 100,		// Delay in transition state reset to stop clipping
				fadeTime = 1000;	// Image transition speed

			// Need to do this as no access to scope variables yet!
			$timeout(function () {

				// Set defaults
				vm.imageFront = {
					filter: "blur("+vm.blur+"px)",
					transition: 'opacity '+fadeTime+'ms',
					opacity: 0,
					backgroundImage: ''
				};

				vm.imageBack = {
					filter: "blur("+vm.blur+"px)",
					transition: 'opacity 0ms',
					opacity: 1,
					backgroundImage: ''
				};
			}, 0);


			// Get promise listener
			var promise = siteService.getBackgroundImageNotify();

			// Handle change of image
			promise.then(null, null, function (image) {

				//console.log('Detected change!', image);

				// Add image to stack
				stack.push(image);

				// Run stack only if 1 item (so it was empty!)
				if (stack.length === 1) {
					runStack();
				}
			});

			/**
			 *
			 */
			function runStack () {

				//
				if (stack.length > 0) {
					var image = stack.shift();
					$timeout(function () {
						transitionToImage(image, function () {
							runStack();
						})
					}, 0);
				}
			}

			/**
			 *
			 */
			function transitionToImage (image, complete) {

				// Set hidden front to new image, fade up
				vm.imageFront.backgroundImage = 'url('+image+')';
				vm.imageFront.opacity = 1;

				// On completion, set back to match front
				$timeout(function () {
					vm.imageBack.backgroundImage = vm.imageFront.backgroundImage;

					// Hide front again to await a new change
					vm.imageFront.opacity = 0;

					//console.log('Resolved!', image);

				}, fadeTime + delay);
			}

		}
	}

})();




