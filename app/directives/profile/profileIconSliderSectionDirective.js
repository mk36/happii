(function () {

    angular
        .module('app')
        .directive("profileSlidingIconSectionDirective", profilePastTimeSectionSetUp);


    function profilePastTimeSectionSetUp($timeout){


        var directive = {

            templateUrl : 'app/views/profile/sections/iconSliderSection.html',

            controller: profileSlidingIconSectionController,

            controllerAs: 'vm',
            bindToController: true,

            scope: {
				pasttimes : '=',
				slidericon : '=',
				title : '=',

				editmode : '=',
				edittitleshow : '=',
				profile : '=',
				questionref : '=',
				min : '='
            }
        };

        return directive;



        /**
         *	Set up controller
         */
        function profileSlidingIconSectionController ($scope, $state, questionService, builderService) {

            var vm = this;

			vm.slickConfig = {};
			
			vm.selectedOptions = [];

			vm.dataOptions = [];

			vm.errorMsg = '';

			/**
			 * Set up the image edit directive
			 */
			vm.setUpProfileSlidingIconSectionDirective = function(){

            	$timeout(function(){

					vm.setUpQuestionDataToDisplay();

				}, 100);

				vm.setUpSlickSlider();
            };

			/**
			 * Set up the slick slider
			 */
			vm.setUpSlickSlider = function(){

				//The love slick settings
				vm.slickConfig = {
					enabled: true,
					//autoplay: true,
					draggable: false,
					arrows: false,
					infinite: true,
					slidesToShow: 5,
					slidesToScroll: 5,
					method: {},
					event: {
						beforeChange: function (event, slick, currentSlide, nextSlide) {

						},
						afterChange: function (event, slick, currentSlide, nextSlide) {

						},
						init: function () {

						}
					},
					responsive: [{
					breakpoint: 480,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1
					}
				},
					{
						breakpoint: 1024,
						settings: {
							slidesToShow: 1,
							slidesToScroll: 1
						}
					}]
				};
			};

			/**
			 * Update the profile past time property
			 */
			vm.updateProfile = function(){

				console.log('updating profile...');

				if(vm.min){

					//console.log('min amount', vm.min);

					if(vm.selectedOptions.length >= vm.min){

						vm.profile.properties.questions[vm.questionref] = vm.selectedOptions;
						vm.profile.update();

						//reset selected
						vm.selectedOptions = [];

						vm.editmode = false;
					}
					else{
						vm.errorMsg = 'You need to select at least ' + vm.min;
					}

				}
				else{
					vm.profile.properties.questions[vm.questionref] = vm.selectedOptions;
					vm.profile.update();

					//reset selected
					vm.selectedOptions = [];

					vm.editmode = false;
				}


			};


			/**
			 * Set the checkbox option in the selectedCheckbox array
			 *
			 * Removes it from the array if the checkbox is unchecked
			 *
			 * @param value
			 */
			vm.setCheckBoxOption = function(value){

				//console.log('value', value);

				if(vm.selectedOptions.includes(value)){

					//Remove the checked element from the selected option array
					_.pull(vm.selectedOptions , value);

				}
				else{
					//Add the checked element to the selected option array
					vm.selectedOptions.push(value);
				}

				//console.log('selected personality options', vm.selectedOptions);
			};

			/**
			 * Set up the data to show in the radio buttons
			 */
			vm.setUpQuestionDataToDisplay = function(){

				questionService.getQuestionData(vm.questionref).then(function(data){

					//console.log('got question data', data);

					vm.dataOptions = data.data.options;

					//console.log('data to display', vm.dataToDisplay);
				}, function(){
					console.log('failed to get question data');
				});
			};

			vm.setUpProfileSlidingIconSectionDirective();

        }
    }

})();




