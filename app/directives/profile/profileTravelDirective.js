(function () {

    angular
        .module('app')
        .directive("profileTravelDirective", profileTravelSetUp);


    function profileTravelSetUp($timeout){


        var directive = {

            templateUrl : 'app/views/profile/sections/travel/travel.html',

            controller: profileTravelController,

            controllerAs: 'vm',
            bindToController: true,

            scope: {
            	profileobj : '=',
				travelbeen : '=',
				travelgo : '=',
				edittitleshow : '='
            }
        };

        return directive;


        /**
         *	Set up controller
         */
        function profileTravelController ($scope, $state, questionService, builderService) {

            var vm = this;

			vm.selectedPlace = {};
			vm.deviceType = '';

			/**
			 * Watch for changes on the show variable
			 */
			$scope.$watch('vm.travelbeen', function(value, old) {

			}, true);

			$scope.$watch('vm.travelgo', function(value, old) {

			}, true);

			$scope.$watch('vm.profileobj', function(value, old) {

				console.log('profile object updated');

				//reinitialise the travel map
				vm.setUpProfileTravelDirective();

			}, true);


			/**
			 * Set up the image edit directive
			 */
			vm.setUpProfileTravelDirective = function(){

				vm.setDeviceType();

            	$timeout(function(){

					//Set up the map for travel
					vm.setUpGoogleMapForTravel();

				}, 100);

            	vm.selectedPlace = vm.travelbeen[0];

            };

			/**
			 * Set the device type
			 */
			vm.setDeviceType = function(){

				if(window.innerWidth >  1199){

					vm.deviceType = 'desktop';
				}
				else if(window.innerWidth <  1200 && window.innerWidth >= 768 ){

					vm.deviceType = 'tablet';
				}
				else if(window.innerWidth <  768 ){

					vm.deviceType = 'mobile';
				}
			};

			/**
			 * Set up the google map for travel
			 */
			vm.setUpGoogleMapForTravel = function(){

				var zoom = 2;
				var minZoom = 2;

				if(vm.deviceType == 'mobile'){
					zoom = 0;
					minZoom = 0;
				}

				var map = new google.maps.Map(document.getElementById('travel-map'), {
					zoom: zoom,
					minZoom: minZoom,
					center: new google.maps.LatLng(52.3555, 1.1743),
					disableDefaultUI: true,
					styles: [ { "elementType": "labels", "stylers": [ { "visibility": "off" } ] }, { "featureType": "administrative", "elementType": "geometry", "stylers": [ { "visibility": "off" } ] }, { "featureType": "administrative.land_parcel", "stylers": [ { "visibility": "off" } ] }, { "featureType": "administrative.neighborhood", "stylers": [ { "visibility": "off" } ] }, { "featureType": "landscape", "elementType": "geometry.fill", "stylers": [ { "color": "#F0F0F0" } ] }, { "featureType": "poi", "stylers": [ { "visibility": "off" } ] }, { "featureType": "road", "stylers": [ { "visibility": "off" } ] }, { "featureType": "road", "elementType": "labels.icon", "stylers": [ { "visibility": "off" } ] }, { "featureType": "transit", "stylers": [ { "visibility": "off" } ] }, { "featureType": "water", "elementType": "geometry.fill", "stylers": [ { "color": "#ffffff" } ] } ]

				});

				//Set the map markers
				vm.setTravelMapMarkers(map);
			};

			/**
			 * Set the travel map markers
			 *
			 * @param map
			 */
			vm.setTravelMapMarkers = function(map) {

				var travelBeenIcons = vm.travelbeen;
				var travelGoIcons = vm.travelgo;

				//Set the icons on the map
				vm.placesMarkersOnTravelMap(travelBeenIcons, 'images/icons/places-pink.png', map, 'been');
				vm.placesMarkersOnTravelMap(travelGoIcons, 'images/icons/places-blue.png', map, 'go');
			};

			/**
			 * Place icons on the travel map
			 *
			 * requires a list of locations to place on the map
			 * requires a url of the image for the marker
			 * @param locations
			 * @param iconImage
			 */
			vm.placesMarkersOnTravelMap = function(locations, iconImage, map, type){

				var image = {
					url: iconImage,
					// This marker is 20 pixels wide by 32 pixels high.
					size: new google.maps.Size(20, 32),
					// The origin for this image is (0, 0).
					origin: new google.maps.Point(0, 0),
					// The anchor for this image is the base of the flagpole at (0, 32).
					anchor: new google.maps.Point(0, 32)
				};

				// Shapes define the clickable region of the icon. The type defines an HTML
				// <area> element 'poly' which traces out a polygon as a series of X,Y points.
				// The final coordinate closes the poly by connecting to the first coordinate.
				var shape = {
					coords: [1, 1, 1, 20, 18, 20, 18, 1],
					type: 'poly'
				};

				//Stores all of the markers
				var markers = [];

				console.log('marker locations passed', locations);

				//For each location, place an icon
				for (var i = 0; i < locations.length; i++) {
					var icon = locations[i];

					var marker = new google.maps.Marker({
						position: {lat: icon.lat, lng: icon.lng},
						map: map,
						icon: image,
						shape: shape,
						title: icon.description,
						zIndex: 4
					});

					markers.push({marker : marker, place : locations[i]});
				}

				//Set up click events for the travel been markers
				if(type == 'been'){
					vm.setUpMarkerClickEvents(markers);
				}
			};

			/**
			 * Set up the google map markers click events
			 *
			 * @param markers
			 */
			vm.setUpMarkerClickEvents = function(markers){

				for(var i = 0; i < markers.length; i ++){

					//Set the place (has to be done for google to recognise it as params are awkward
					markers[i].marker.place = markers[i].place;

					google.maps.event.addListener(markers[i].marker, 'click', function() {

						vm.openMapPopUp(this.place);
					});

					google.maps.event.addListener(markers[i].marker, 'mouseover', function() {

						//console.log(this);

						//this.icon.size = new google.maps.Size(42, 68);
						this.setIcon = "imageB.png";
					});
				}

			};

			/**
			 * Open the map pop up
			 *
			 * requires th eplace object that you want to view
			 *
			 * @param place
			 */
			vm.openMapPopUp = function(place){

				//Set the selected place
				vm.selectedPlace = place;

				//console.log('place', place);

				//Show the modal
				$("#map-modal").modal({show: true, backdrop: false});

				$scope.$apply();
			};


			vm.setUpProfileTravelDirective();

        }
    }

})();




