(function () {

    angular
        .module('app')
        .directive("mapPopUpDirective", mapPopUpSetUp);


    function mapPopUpSetUp($timeout){


        var directive = {

            templateUrl : 'app/views/profile/sections/travel/mapPopUp.html',

            controller: mapPopUpController,

            controllerAs: 'vm',
            bindToController: true,

            scope: {
				place : '='
            }
        };

        return directive;


        /**
         *	Set up controller
         */
        function mapPopUpController ($scope, $state, questionService, builderService) {

            var vm = this;

			vm.slickConfig = {};
			vm.show = false;
			vm.serverPath = window.server_path;


			$scope.$watch('vm.place', function(value, old) {

				vm.show = false;

				$timeout(function(){

					vm.show = true;
					console.log('slider being set up again');
				}, 200);


			}, true);

			/**
			 * Set up the image edit directive
			 */
			vm.setUpMapPopUpDirective = function(){

            	$timeout(function(){

            		console.log('place object', vm.place);

					vm.setUpSlickSlider();
				}, 500);
            };

			/**
			 * Set up the slick slider
			 */
			vm.setUpSlickSlider = function(){

				//The love slick settings
				vm.slickConfig = {
					enabled: true,
					//autoplay: true,
					draggable: false,
					arrows: false,
					infinite: true,
					slidesToShow: 1,
					slidesToScroll: 1,
					method: {},
					event: {
						beforeChange: function (event, slick, currentSlide, nextSlide) {

						},
						afterChange: function (event, slick, currentSlide, nextSlide) {

						},
						init: function () {

						}
					}
				};
			};

			/**
			 * Get the server url for the image
			 *
			 * @param imageName
			 * @returns {string}
			 */
			vm.getMapImageUrl = function(imageName){

				//console.log('path for image', window.server_path + 'image/' + imageName);
				return window.server_path + 'image/' + imageName;
			};

			/**
			 * Closes the map modal
			 */
			vm.closeMapModal = function(){

				//Show the modal
				$("#map-modal").modal("hide");
			};

			vm.setUpMapPopUpDirective();

        }
    }

})();




