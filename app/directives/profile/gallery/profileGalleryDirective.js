(function () {

    angular
        .module('app')
        .directive("profileGalleryDirective", profileGallerySetUp);


    function profileGallerySetUp($timeout){


        var directive = {

            templateUrl : 'app/views/directives/profileGalleryDirective.html',

            controller: profileGalleryController,

            controllerAs: 'vm',
            bindToController: true,

            scope: {

				questionref : '=',
				type : '='
            }
        };

        return directive;



        /**
         *	Set up controller
         */
        function profileGalleryController ($scope, $state, questionService, builderService, profileService) {

			var vm = this;

			vm.profileObj = {};
			vm.media = [];

			vm.deleteMode = false;

			/**
			 * Set up the register controller
			 */
			vm.setUpProfileGalleryController = function(){

				profileService.createProfileObj();

				vm.profileObj = profileService.getProfileObj();

				console.log('profile controller setup', vm.profileObj);

				$timeout(function(){

					vm.media = vm.profileObj.properties.questions[vm.questionref];
					//vm.media = vm.profileObj.properties.questions.profileVideos;

					if(vm.type == 'image'){
						vm.setUpImageBoxes();
					}

				}, 100);

			};

			/**
			 * Set up the image boxes
			 */
			vm.setUpImageBoxes = function () {


			};

			/**
			 * Get the image path (server)
			 *
			 * @param imageName
			 */
			vm.getImagePath = function(imageObj){

				return window.server_path + 'image/' + imageObj.name;
			};

			/**
			 * Get image path local
			 *
			 * @param imagename
			 * @returns {string}
			 */
			vm.getImagePathLocal = function(imagename){
				return SITE_REL_PATH + 'image/' + imagename;
			};

			/**
			 * Toggle delete mode
			 */
			vm.toggleDeleteMode = function () {

				vm.deleteMode = !vm.deleteMode;

				//If delete mode has been triggered off, delete the selected photos
				if(vm.deleteMode == false){

					for(var i = 0; i <= vm.media.length; i++){
						vm.deleteImage(vm.media[i]);
					}

					//Update the profile object
					vm.updateProfileBuilderObject();
				}

			};

			/**
			 * Delete an image from the photo gallery
			 */
			vm.deleteImage = function(image){

				//var index = _.indexOf(vm.imagesToDelete, image);

				var r = confirm("Are you sure you want to delete this " + vm.type + '?');

				if (r == true) {
					//Remove the element from the array
					_.pull(vm.media , image);
					vm.updateProfileBuilderObject();

				} else {

				}



			};

			/**
			 * Update the profile builder object
			 */
			vm.updateProfileBuilderObject = function(){

				//vm.profileObj.setQuestionValueByQuestionReference('profileGallery', vm.media);
				vm.profileObj.setQuestionValueByQuestionReference(vm.questionref, vm.media);

				console.log('profile controller setup', vm.profileObj);
			};


			/**
			 * Get the server url for the video
			 *
			 * @param image
			 * @returns {string}
			 */
			vm.getVideoUrl = function(video){

				return window.server_path + 'video/' + video.name;
			};



			vm.setUpProfileGalleryController();


		}
    }

})();





