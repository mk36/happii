(function () {

    angular
        .module('app')
        .directive("profileFooterDirective", profileFooterSetUp);


    function profileFooterSetUp($timeout){


        var directive = {

            templateUrl : 'app/views/profile/sections/footer.html',

            controller: profileFooterController,

            controllerAs: 'vm',
            bindToController: true,

            scope: {
				//profileimage : '='
            }
        };

        return directive;



        /**
         *	Set up controller
         */
        function profileFooterController ($scope, $state, questionService, builderService) {

            var vm = this;

			/**
			 * Set up the image edit directive
			 */
			vm.setUpProfileFooterDirective = function(){

				$('#profile-footer').css('opacity', 0);

            	$timeout(function(){

					$('#profile-footer').css('opacity', 1);
				}, 1000);

            };

			vm.setUpProfileFooterDirective();

        }
    }

})();




