(function () {

    angular
        .module('app')
        .directive("profileManageButtonsDirective", profileManageButtonsSetUp);


    function profileManageButtonsSetUp($timeout){


        var directive = {

            templateUrl : 'app/views/profile/sections/manageButtons.html',

            controller: profileManageButtonsController,

            controllerAs: 'vm',
            bindToController: true,

            scope: {
				profileimage : '='
            }
        };

        return directive;



        /**
         *	Set up controller
         */
        function profileManageButtonsController ($scope, $state, questionService, builderService) {

            var vm = this;

			/**
			 * Set up the image edit directive
			 */
			vm.setUpProfileManageButtonsDirective = function(){

            	$timeout(function(){

					//console.log('The passed profile image', vm.profileimage);

				}, 100);

            };

			vm.setUpProfileManageButtonsDirective();

        }
    }

})();




