(function () {

    angular
        .module('app')
        .directive("profileActionIconsDirective", profileActionIconsSetUp);


    function profileActionIconsSetUp($timeout){


        var directive = {

            templateUrl : 'app/views/profile/sections/actionIcons.html',

            controller: profileActionIconsController,

            controllerAs: 'vm',
            bindToController: true,

            scope: {
				name : '=',
				age : '=',
				location : '=',
				photos : '=',
				videos : '='
            }
        };

        return directive;



        /**
         *	Set up controller
         */
        function profileActionIconsController ($scope, $state, questionService, builderService) {

            var vm = this;

            vm.mediaPopUpMode = 'photo';

			/**
			 * Set up the image edit directive
			 */
			vm.setUpProfileActionIconsDirective = function(){

            	$timeout(function(){

					//console.log('The passed profile image', vm.profileimage);
					console.log('action icon location', vm.location);
				}, 100);

            };


			vm.getAgeFromDob = function (dob) {


				var age = calcAge(dob);

				//console.log('age passed', dob, 'calc age', age);

				return age;
			};

			function calcAge(dateString) {
				var birthday = +new Date(dateString);
				return ~~((Date.now() - birthday) / (31557600000));
			}

			vm.setUpProfileActionIconsDirective();

        }
    }

})();




