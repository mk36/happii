(function () {

    angular
        .module('app')
        .directive("profileCoverPhotoDirective", profileCoverPhotoSetUp);


    function profileCoverPhotoSetUp($timeout){


        var directive = {

            templateUrl : 'app/views/profile/sections/coverPhoto.html',

            controller: profileCoverPhotoController,

            controllerAs: 'vm',
            bindToController: true,

            scope: {
				profilecoverphoto : '='
            }
        };

        return directive;



        /**
         *	Set up controller
         */
        function profileCoverPhotoController ($scope, $state, questionService, builderService) {

            var vm = this;

			/**
			 * Set up the image edit directive
			 */
			vm.setUpProfileCoverPhotoDirective = function(){

            	$timeout(function(){

					console.log('cover photo passed', vm.profilecoverphoto);
				}, 100);

            };

			/**
			 * Get the image path
			 *
			 * @param imageName
			 */
			vm.getImagePath = function(imagename){

				return window.server_path + 'image/' + imagename;
			};

			vm.getImagePathLocal = function(imagename){
				return SITE_REL_PATH + 'image/' + imagename;
			};

			vm.setUpProfileCoverPhotoDirective();

        }
    }

})();




