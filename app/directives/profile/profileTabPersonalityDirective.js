(function () {

    angular
        .module('app')
        .directive("profileTabPersonalityDirective", profileTabPersonalitySetUp);


    function profileTabPersonalitySetUp($timeout){


        var directive = {

            templateUrl : 'app/views/profile/sections/tabPersonality.html',

            controller: profileTabPersonalityController,

            controllerAs: 'vm',
            bindToController: true,

            scope: {
				personality : '=',
				profile : '='
            }
        };

        return directive;



        /**
         *	Set up controller
         */
        function profileTabPersonalityController ($scope, $state, questionService, builderService) {

            var vm = this;

			//The personality/profile tabbed content
			vm.tabPersonalityContent = {name : 'profile', list : ['hello', 'goodbye']};

			/**
			 * Set up the image edit directive
			 */
			vm.setUpProfileTabPersonalityDirective = function(){

            	$timeout(function(){

					//console.log('The passed profile image', vm.profileimage);

				}, 100);

				vm.setUpPersonalityTabbedContent();

            };

			/**
			 * Set up the personality tabbed content
			 */
			vm.setUpPersonalityTabbedContent = function(){

				var tabbedContent = {
					name : 'traits',
					list : vm.personality
				};

				vm.tabPersonalityContent = tabbedContent;
			};

			/**
			 * Set the tabbed content for the personality results
			 *
			 * @param name
			 */
			vm.setPersonalityTabbedContent = function(name){

				var tabbedContent = {
					name : name,
					list : []
				};

				//questions
				if(name == 'profile'){

					tabbedContent.list = vm.profile;
				}
				else if(name == 'traits'){
					tabbedContent.list = vm.personality;
				}

				vm.tabPersonalityContent = tabbedContent;
			};

			vm.setUpProfileTabPersonalityDirective();

        }
    }

})();




