(function () {

    angular
        .module('app')
        .directive("profileDescriptionDirective", profileDescriptionSetUp);


    function profileDescriptionSetUp($timeout){


        var directive = {

            templateUrl : 'app/views/profile/sections/description.html',

            controller: profileDescriptionController,

            controllerAs: 'vm',
            bindToController: true,

            scope: {
				editmode : '=',
				edittitleshow : '=',
				profile : '='
            }
        };

        return directive;



        /**
         *	Set up controller
         */
        function profileDescriptionController ($scope, $state, questionService, builderService) {

            var vm = this;

            vm.description = '';

			/**
			 * Set up the image edit directive
			 */
			vm.setUpProfileDescriptionDirective = function(){

            	$timeout(function(){

					//console.log('The passed profile image', vm.profileimage);
					vm.description = vm.profile.properties.profileDescription
				}, 100);

            };

			/**
			 * Update the profile description
			 */
			vm.updateProfileDescription = function(){

				console.log('updating profile description', vm.profile);

				vm.profile.properties.profileDescription = vm.description;
				vm.profile.update();

				vm.editmode = false;
			};

			vm.setUpProfileDescriptionDirective();

        }
    }

})();




