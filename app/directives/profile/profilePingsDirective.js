(function () {

    angular
        .module('app')
        .directive("profilePingsDirective", profilePingsSetUp);


    function profilePingsSetUp($timeout){


        var directive = {

            templateUrl : 'app/views/profile/sections/pings.html',

            controller: profilePingsController,

            controllerAs: 'vm',
            bindToController: true,

            scope: {
				//profileimage : '='
            }
        };

        return directive;



        /**
         *	Set up controller
         */
        function profilePingsController ($scope, $state, questionService, builderService, profileService) {

            var vm = this;


			//Pings to show
			vm.pings = [];

			/**
			 * Set up the image edit directive
			 */
			vm.setUpProfilePingsDirective = function(){

            	$timeout(function(){

					//console.log('The passed profile image', vm.profileimage);

					//Set up the pings to display
					vm.setUpPings();

				}, 100);

            };

			/**
			 * Set up the pings
			 */
			vm.setUpPings = function(){

				profileService.getAllPings().then(function(data){

					//console.log('pings received', data);
					vm.pings = data.pings;
				});
			};



			vm.setUpProfilePingsDirective();

        }
    }

})();




