(function () {

    angular
        .module('app')
        .directive("profileMainPhotoDirective", profileMainPhotoSetUp);


    function profileMainPhotoSetUp($timeout){


        var directive = {

            templateUrl : 'app/views/profile/sections/mainPhoto.html',

            controller: profileMainPhotoController,

            controllerAs: 'vm',
            bindToController: true,

            scope: {
				profilemainphoto : '='
            }
        };

        return directive;



        /**
         *	Set up controller
         */
        function profileMainPhotoController ($scope, $state, questionService, builderService) {

            var vm = this;

			/**
			 * Set up the image edit directive
			 */
			vm.setUpProfileMainPhotoDirective = function(){

            	$timeout(function(){

					//console.log('cover photo passed', vm.profilecoverphoto);
				}, 100);

            };

			/**
			 * Get the image path
			 *
			 * @param imageName
			 */
			vm.getImagePath = function(imagename){

				return window.server_path + 'image/' + imagename;
			};

			vm.setUpProfileMainPhotoDirective();

        }
    }

})();




