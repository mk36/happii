(function () {

    angular
        .module('app')
        .directive("profileFavouriteListDirective", profileFavouriteListSetUp);


    function profileFavouriteListSetUp($timeout, authService){


        var directive = {

            templateUrl : 'app/views/profile/sections/favouriteList.html',

            controller: profileFavouriteListController,

            controllerAs: 'vm',
            bindToController: true,

            scope: {

				icon : '=',
				listdata : '=',
				questionref : '=',
				title : '=',
				editmode : '=',
				edittitleshow : '=',
				profile : '='
            }
        };

        return directive;



        /**
         *	Set up controller
         */
        function profileFavouriteListController ($scope, $state, questionService, builderService) {

            var vm = this;

			vm.selectedOptions = {};
			vm.newItem = '';

			vm.slickConfig = {};
			vm.dataToDisplay = [];
			vm.apiResults = [];

			vm.sliderActive = true;

			//used to play youtube trailers
			vm.player = null;

			$scope.$watch('vm.apiResults', function(value, old) {}, true);


			/**
			 * Set up the image edit directive
			 */
			vm.setUpProfileFavouriteListDirective = function(){

            	$timeout(function(){

					vm.selectedOptions = vm.listdata;

				}, 100);

				vm.setUpSportsApi();
				vm.setUpSlickSlider();
            };

			/**
			 * Set up the sports api
			 */
			vm.setUpSportsApi = function () {

				questionService.getQuestionData('sport').then(function(data){

					vm.dataToDisplay = data.data.sports;

				}, function(){
					console.log('failed to get question data');
				});
			};

			/**
			 * Update the profile past time property
			 */
			vm.updateProfile = function(){

				vm.profile.properties.questions[vm.questionref] = vm.selectedOptions;

				vm.profile.update();

				//reset selected
				vm.selectedOptions = [];
			};

			/**
			 * Add new item
			 *
			 * @param item
			 */
			vm.addNewItem = function(item){

				console.log('id', item.id);

				if(vm.selectedOptions.length < 6 && vm.checkIfItemExists(item) == false){

					vm.selectedOptions.push(item);
				}

				vm.newItem = '';

				//reset up the slick slider
				vm.resetSlider();
			};

			/**
			 * Check if an item exists in the array
			 *
			 * @param item
			 * @returns {boolean}
			 */
			vm.checkIfItemExists = function (item) {

				for(var i = 0; i < vm.selectedOptions.length; i++){

					if(vm.questionref == 'books'){
						if(vm.selectedOptions[i].title == item.title){
							return true;
						}
					}
					else if(vm.questionref == 'sport'){
						if(vm.selectedOptions[i].name == item.name){
							return true;
						}
					}
					else{
						if(vm.selectedOptions[i].id == item.id){
							return true;
						}
					}
				}

				return false;
			};

			/**
			 * Remove an item from the list
			 * @param item
			 */
			vm.removeItem = function (item) {

				//Remove the item from the selected option array
				_.pull(vm.selectedOptions , item);

				//reset up the slick slider
				vm.resetSlider();
			};

			/**
			 * Reset the slider
			 */
			vm.resetSlider = function () {

				console.log('slider reset from function');

				vm.newOptions = vm.selectedOptions;

				vm.listdata = [];

				$timeout(function () {

					vm.listdata = vm.newOptions;
				}, 200);
			};

			/**
			 * Set up the slick slider
			 */
			vm.setUpSlickSlider = function(){

				//The love slick settings
				vm.slickConfig = {

					enabled: true,
					//autoplay: true,
					draggable: false,
					arrows: false,
					infinite: true,
					slidesToShow: 1,
					slidesToScroll: 1,
					method: {},
					event: {
						beforeChange: function (event, slick, currentSlide, nextSlide) {

						},
						afterChange: function (event, slick, currentSlide, nextSlide) {

						},
						init: function () {

						}
					},
					mobileFirst: true,
					responsive: [{
						breakpoint: 480,
						settings: {
							slidesToShow: 1,
							slidesToScroll: 1
						}
					},
						{
							breakpoint: 1024,
							settings: {
								slidesToShow: 1,
								slidesToScroll: 1
							}
						}]
				};
			};

			/**
			 * Manage any Api's that need to be setup
			 */
			vm.manageAPiSearch = function () {


				if(vm.questionref == 'books'){
					vm.searchBooksApi(vm.newItem);
				}
				else if(vm.questionref == 'music'){
					vm.searchMusicApi(vm.newItem);
				}
				else if(vm.questionref == 'viewings'){
					vm.searchFilmApi(vm.newItem);
				}
				else if(vm.questionref == 'sport'){
					vm.searchSportApi(vm.newItem);
				}

			};

			/**
			 * Search the books api
			 */
			vm.searchBooksApi = function (text) {

				console.log('searching books api...');

				var query = text.replace(" ", "+");

				// Set the api variable
				//var googleAPI = "https://www.googleapis.com/books/v1/volumes?q=harry+potter";
				var googleAPI = "https://www.googleapis.com/books/v1/volumes?q=" + query;

				// Make a ajax call to get the json data as response.
				$.getJSON(googleAPI, function (response) {

					// In console, you can see the response objects
					console.log("JSON Data: ", response.items);
					vm.apiResults = response.items;

					$scope.$apply();
				});
			};

			/**
			 * Search the books api
			 */
			vm.searchMusicApi = function (text) {

				// authService.authSpotify().then(function (data) {
				//
				// 	console.log('auth spotify reply');
				// });


				console.log('searching music api...');

				var query = text.replace(" ", "+");

				// Set the api variable
				var api = "https://api.spotify.com/v1/search?q=" + query + "&type=track";

				// Make a ajax call to get the json data as response.
				$.getJSON(api, function (response) {

					// In console, you can see the response objects
					console.log("JSON Data: ", response.tracks.items);
					vm.apiResults = response.tracks.items;

					$scope.$apply();

				});
			};

			/**
			 * Search the books api
			 */
			vm.searchFilmApi = function (text) {

				console.log('searching film api...');

				var query = text.replace(" ", "+");

				// Set the api variable
				var api = "https://api.themoviedb.org/3/search/movie?api_key=fd7ca7a1608af4290c2ba21a4bebfc69&query=" + query;

				//movie/id/videos

				// Make a ajax call to get the json data as response.
				$.getJSON(api, function (response) {

					// In console, you can see the response objects
					console.log("JSON Data: ", response);
					//console.log("JSON Data: ", response.Search);
					vm.apiResults = response.results;

					$scope.$apply();
				});
			};

			/**
			 * Search the books api
			 */
			vm.searchSportApi = function (text) {

				console.log('searching sport api...', vm.dataToDisplay);

				var results = [];

				for(var i = 0; i < vm.dataToDisplay.length; i++){

					var curText = vm.dataToDisplay[i].name.toLowerCase();

					if(curText.includes(text)){

						results.push(vm.dataToDisplay[i]);
					}
				}

				vm.apiResults = results;

				$scope.$apply();

				console.log('api results', vm.apiResults);
			};

			/**
			 * Set up the trailer to watch
			 */
			vm.watchFilmTrailer = function (video) {

				console.log('trailer', video);

				var videoId = video.id;

				console.log('searching film api...');

				// Set the api variable
				var api = "https://api.themoviedb.org/3/movie/" + videoId + "/videos?api_key=fd7ca7a1608af4290c2ba21a4bebfc69";

				// Make a ajax call to get the json data as response.
				$.getJSON(api, function (response) {

					// In console, you can see the response objects
					console.log("JSON Data: ", response);

					if(response.results[0].key){
						vm.setUpTrailerToPlay(response.results[0].key);
					}


				});




			};

			vm.setUpTrailerToPlay = function (videoId) {

				$("#watch-trailer-modal").modal({backdrop: false});

				if (typeof(YT) == 'undefined' || typeof(YT.Player) == 'undefined') {

					//Create a script
					var tag = document.createElement('script');
					tag.src = "https://www.youtube.com/iframe_api";

					var firstScriptTag = document.getElementsByTagName('script')[0];

					firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

					window.onYouTubePlayerAPIReady = function() {

						onYouTubePlayer(videoId);
					};

				}else {

					onYouTubePlayer(videoId);

				}

			};



			function onYouTubePlayer(videoId) {

				if(vm.player == null){

					vm.player = new YT.Player('player', {
						height: '100%',
						width: '100%',
						videoId: videoId,
						playerVars: { controls:1, showinfo: 0, rel: 0, showsearch: 0, iv_load_policy: 3 },
						events: {
							'onReady': onPlayerReady
						}

					});
				}
				else{
					//play the video with the new id
					vm.player.loadVideoById(videoId);
				}

				console.log('player', vm.player);

				$('#stop-video-btn').on('click', function () {

					vm.player.stopVideo();
				});
			}


			/**
			 * Stop the video from playing
			 */
			vm.stopVideo = function() {

				vm.player.stopVideo();
			};


			function onPlayerReady(event) {
				event.target.playVideo();
			}

			vm.audio = {};

			/**
			 * Play spotify song
			 *
			 * @param song
			 */
			vm.playSpotifySong = function (track) {

				console.log('track', track);

				if(vm.audio.currentTime){
					//audio has already been set up

					if(vm.audio.name == track){

						//pause the audio if the same track has been clicked on
						vm.audio.pause();

						//reset the name so the same track can be played again
						vm.audio.name = '';

						//console.log('same track has been selected, song paused');
					}
					else{
						vm.audio.name = track;
						vm.audio.src = track.preview_url;
						vm.audio.play();

						//console.log('different track selected, change the song');
					}
				}
				else{
					//create a new audio object
					vm.audio = new Audio();
					vm.audio.name = track;
					vm.audio.src = track.preview_url;
					vm.audio.play();

					//console.log('creating new audio obj');
				}


				console.log('playing song', track, 'object for audio', vm.audio);
			};

			vm.setUpProfileFavouriteListDirective();

        }
    }

})();




