(function () {

    angular
        .module('app')
        .directive("profileSimilarProfilesDirective", profileSimilarProfilesSetUp);


    function profileSimilarProfilesSetUp($timeout){


        var directive = {

            templateUrl : 'app/views/profile/sections/similarProfiles.html',

            controller: profileSimilarProfilesController,

            controllerAs: 'vm',
            bindToController: true,

            scope: {
				//profileimage : '='
            }
        };

        return directive;



        /**
         *	Set up controller
         */
        function profileSimilarProfilesController ($scope, $state, questionService, builderService, profileService) {

            var vm = this;

			//Similar profiles to show
			vm.similarProfiles = [];

			//The slick slider settings
			vm.slickConfigSimilar = {};

			/**
			 * Set up the image edit directive
			 */
			vm.setUpProfileSimilarProfilesDirective = function(){

            	$timeout(function(){

            		vm.setUpSlickSlider();
				}, 100);

				//Set up similar profiles
				vm.setUpSimilarProfiles();

            };

			/**
			 * Set up slick slider
			 */
			vm.setUpSlickSlider = function(){

				//The love slick settings
				vm.slickConfigSimilar = {
					enabled: true,
					//autoplay: true,
					draggable: false,
					arrows: true,
					infinite: true,
					slidesToShow: 3,
					slidesToScroll: 3,
					method: {},
					event: {
						beforeChange: function (event, slick, currentSlide, nextSlide) {

						},
						afterChange: function (event, slick, currentSlide, nextSlide) {

						},
						init: function () {

						}
					},
					responsive: [{
					breakpoint: 480,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1
					}
					},
					{
						breakpoint: 1024,
						settings: {
							slidesToShow: 1,
							slidesToScroll: 1
						}
					}]
				};
			};

			/**
			 * Set up the similar profiles
			 */
			vm.setUpSimilarProfiles = function () {


				profileService.getSimilarProfiles().then(function(data){

					console.log('got similar profiles', data);
					vm.similarProfiles = data.similar_profiles;
				});
			};

			vm.setUpProfileSimilarProfilesDirective();

        }
    }

})();




