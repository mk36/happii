(function () {

    angular
        .module('app')
        .directive("lookingForModalDirective", lookingForModalSetUp);


    function lookingForModalSetUp($timeout){


        var directive = {

            templateUrl : 'app/views/profile/sections/lookingForModal.html',

            controller: lookingForModalController,

            controllerAs: 'vm',
            bindToController: true,

            scope: {
				details : '=',
				type : '='
            }
        };

        return directive;


        /**
         *	Set up controller
         */
        function lookingForModalController ($scope, $state, questionService, profileService) {

            var vm = this;

			vm.selectedOptions = [];

			//Range slider
			vm.slider = {};

			//Selected range
			vm.selectedRange = {
				min : 0,
				max : 0
			};


			$scope.$watch('vm.details', function(value, old) {

				//console.log('old image', old, 'new image', value);
				if(vm.details.type == 'range'){

					console.log('setting up slider');
					vm.setUpSlider();
				}

			}, true);


			/**
			 * Set up the image edit directive
			 */
			vm.setUpLookingForModalDirective = function(){

            	$timeout(function(){

					//console.log('The passed profile image', vm.profileimage);

				}, 100);

            };

			/**
			 * Set the checkbox option in the selectedCheckbox array
			 *
			 * Removes it from the array if the checkbox is unchecked
			 *
			 * @param value
			 */
			vm.setCheckBoxOption = function(value){

				console.log('value', value);

				if(vm.selectedOptions.includes(value)){

					//Remove the checked element from the selected option array
					_.pull(vm.selectedOptions , value);

				}
				else{
					//Add the checked element to the selected option array
					vm.selectedOptions.push(value);
				}

				console.log('selected personality options', vm.selectedOptions);
			};


			/**
			 * Set up the slider
			 */
			vm.setUpSlider = function(){

				vm.slider = {
					//value: [18, 35],
					min: vm.details.value.min,
					max: vm.details.value.max,
					options: {
						floor: vm.details.value.min,
						ceil: vm.details.value.max,
						//step: 1,
						//precision: 1,
						showTicks: true,
						onChange: function(){

							vm.selectedRange.min = vm.slider.min;
							vm.selectedRange.max = vm.slider.max;
						}
					}
				}

			};

			/**
			 * Save the looking for details
			 *
			 * @param questionRef
			 */
			vm.saveLookingForDetails = function(){

				//Update the profile builder object
				var profileBuilderObj = profileService.getProfileObj();

				console.log('profile builder object', profileBuilderObj);

				if(vm.details.type == 'radio'){
					profileBuilderObj.setQuestionValueByQuestionReference(vm.details.questionRef, vm.selectedOptions[0]);
				}
				else if(vm.details.type == 'checkbox'){
					profileBuilderObj.setQuestionValueByQuestionReference(vm.details.questionRef, vm.selectedOptions);
				}
				else if(vm.details.type == 'range'){
					profileBuilderObj.setQuestionValueByQuestionReference(vm.details.questionRef, vm.selectedRange);
				}


				//Empty the selected options
				vm.selectedOptions = [];
			};

			vm.setUpLookingForModalDirective();

        }
    }

})();




