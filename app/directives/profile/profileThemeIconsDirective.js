(function () {

    angular
        .module('app')
        .directive("profileThemeIconsDirective", profileThemeIconsSetUp);


    function profileThemeIconsSetUp($timeout){


        var directive = {

            templateUrl : 'app/views/profile/sections/themeIcons.html',

            controller: profileThemeIconsController,

            controllerAs: 'vm',
            bindToController: true,

            scope: {
				profile : '='
            }
        };

        return directive;



        /**
         *	Set up controller
         */
        function profileThemeIconsController ($scope, $state, questionService, builderService, loginService) {

            var vm = this;

            vm.themeColors = [
            	{name : '', value : '#58728d'},
            	{name : '', value : '#706481'},
            	{name : '', value : '#e87a8b'},
            	{name : '', value : '#fde4be'},
            	{name : '', value : '#c7e6e0'},
            	{name : '', value : '#9ab8bb'},
            	{name : '', value : '#8e9190'},
            	{name : '', value : '#e3e0d1'}

            	];

            vm.name = '';
            vm.age = '';
            vm.location = {};

			/**
			 * Set up the image edit directive
			 */
			vm.setUpProfileThemeIconsDirective = function(){

            	$timeout(function(){

					console.log('The passed profile obj theme', vm.profile);

					//vm.name = vm.profile.properties.questions;
					//vm.age = '';
					//vm.location = {};
					loginService.status().then(function (data) {

						console.log('data from login', data);
					});

				}, 100);

            };

			vm.openImageEditModal = function(){

				console.log('triggered open');
				$("#image-upload-modal").modal({backdrop: false});
			};

			vm.setUpProfileThemeIconsDirective();

        }
    }

})();




