(function () {

    angular
        .module('app')
        .directive("profileTravelEditDescribeDirective", profileTravelEditDescribeSetUp);


    function profileTravelEditDescribeSetUp($timeout){


        var directive = {

            templateUrl : 'app/views/profile/sections/travel/travelEditDescribe.html',

            controller: profileTravelEditDescribeController,

            controllerAs: 'vm',
            bindToController: true,

            scope: {
            	profileobj : '=',
				travelbeen : '='
            }
        };

        return directive;


        /**
         *	Set up controller
         */
        function profileTravelEditDescribeController ($http, $scope, $state, questionService, profileService, authService) {

            var vm = this;

			//Stores the place description
			vm.activePlace = '';

			vm.socialPhotos = [];
			vm.activeSocialPhoto = {name : '', src : ''};

			vm.showImageEditor = false;

			vm.accessTokens = {
				facebook : '',
				instagram : ''
			};

			vm.imageUploadedMsg = '';

			/**
			 * Watch for changes on the show variable
			 */
			$scope.$watch('vm.travelbeen', function(value, old) {}, true);
			$scope.$watch('vm.travelgo', function(value, old) {}, true);
			$scope.$watch('vm.profileobj', function(value, old) {}, true);

			/**
			 * Set up the image edit directive
			 */
			vm.setUpProfileTravelEditDescribeDirective = function(){

            	$timeout(function(){

					vm.activePlace = vm.travelbeen[0];

					vm.setUpDropzone();

				}, 100);

            };

			/**
			 * Switch the active place to a new one, saving the existing
			 * place data to typescript object before it happens
			 *
			 * @param place
			 */
			vm.switchActivePlace = function (place) {

				vm.saveActivePlaceObject();
				vm.setActivePlaces(place);
			};

			/**
			 * Set the active place
			 *
			 * @param place
			 */
			vm.setActivePlaces = function(place){

				vm.activePlace = place;
			};

			/**
			 * Save the active place object
			 */
			vm.saveActivePlaceObject = function(){

				//Update the profile builder object
				var profileBuilderObj = vm.profileobj;

				console.log('profile builder object', profileBuilderObj);

				//Save travel been
				profileBuilderObj.setQuestionValueByQuestionReference('travelBeen', vm.travelbeen);

				//reset up google maps

			};


			/**
			 * Save the selection to the typescript object
			 *
			 */
			vm.saveSelection = function(){

				vm.saveActivePlaceObject();

				//Close the modal
				$("#travel-edit-describe-modal").modal('hide');
			};

			/**
			 * Set up drop zone for drag and drop file uploading
			 */
			vm.setUpDropzone = function(){

				console.log('dropzone set up');

				/**
				 * Dropzone image upload
				 */
				$("div#photo-travel-upload-display").dropzone({ url: window.server_path + "upload/", paramName: "file", // The name that will be used to transfer the file
					maxFilesize: 5, // MB
					//maxFiles: 1,
					//uploadMultiple: false,
					clickable: true,
					acceptedFiles: '.jpeg,.jpg,.png,.gif',
					accept: function(file, done) {

						console.log('accepted image');

						//Create the image upload object
						var imageUploadObj = {
							form : file,
							fileName : file.name
						};

						//Upload th eimage to the server
						questionService.uploadImageToServer(imageUploadObj, file.name).then(function(data){

							if(vm.activePlace.images == null){

								vm.activePlace.images = [];

								//Tell the user the photo has been uploaded
								vm.imageUploadedMsg = 'Photo ' + data.data[0].file_name + ' uploaded';
							}

							vm.activePlace.images.push(data.data[0].file_name);

							console.log('active place', vm.activePlace );
						});
					}});

				/**
				 * Dropzone video upload
				 */
				$("div#video-travel-upload-display").dropzone({ url: window.server_path + "upload/", paramName: "file", // The name that will be used to transfer the file
					maxFilesize: 5, // MB
					//maxFiles: 1,
					//uploadMultiple: false,
					clickable: true,
					acceptedFiles: '.mp4,.mkv,.avi',
					accept: function(file, done) {

						console.log('accepted video');

						//Create the image upload object
						var imageUploadObj = {
							form : file,
							fileName : file.name
						};

						//Upload th eimage to the server
						questionService.uploadImageToServer(imageUploadObj, file.name).then(function(data){

							if(vm.activePlace.videos == null){

								vm.activePlace.videos = [];
							}

							vm.activePlace.videos.push(data.data[0].file_name);

							vm.imageUploadedMsg = 'Video ' + data.data[0].file_name + ' uploaded';

							console.log('server image path', window.server_path + 'image/' + data.data[0].file_name);
						});
					}});
			};

			/**
			 *	Call a click event on the dropzone button so the device
			 *  file browser cna be opened
			 */
			vm.triggerDropZoneForImage = function(){

				$("#photo-travel-upload-display").click();
			};

			/**
			 *	Call a click event on the dropzone button so the device
			 *  file browser cna be opened
			 */
			vm.triggerDropZoneForVideo = function(){

				$("#video-travel-upload-display").click();
			};

			/**
			 * Authorise facebook login
			 */
			vm.authFacebook = function(){

				authService.authenticateFacebook().then(function(data){

					vm.accessTokens.facebook = data;

					authService.getFacebookUserPhotos().then(function(data){

						vm.socialPhotos = data;
						console.log('social photos', vm.socialPhotos);

						$("#facebook-pic-modal").modal({backdrop: false});


					}, function (data) {

						console.log('epic fail with getting facebook auth', data);
					});

				});

			};

			/**
			 * Set the social image to edit
			 *
			 * requires the url of the image to edit and the modal they
			 * are contained in to close
			 *
			 * @param image
			 * @param modal
			 */
			vm.saveSocialImage = function(){

				var fileName = vm.activeSocialPhoto.src;

				if(vm.activePlace.images == null){

					vm.activePlace.images = [];
				}

				console.log('social image to upload', vm.activeSocialPhoto.socialImg);

				var obj = {
					base64 : vm.activeSocialPhoto.socialImg
				};

				//Upload the image to the server
				$http.post(window.server_path + 'upload/' + fileName, obj).then(function(data){

					console.log('travel facebook image uploaded', data.data[0]);

					//Add the image to the active travel object
					vm.activePlace.images.push(data.data[0].file_name);

					//Reset the active social image
					vm.activeSocialPhoto = {name : '', src : ''};
				});

				//Close the social modal
				$('#facebook-pic-modal').modal("hide");

			};

			/**
			 * Set the canvas to the chosen facebook image
			 *
			 * @param id
			 */
			vm.setSocialCanvasImage = function(id, url) {

				var img = new Image();

				img.setAttribute('crossOrigin', 'anonymous');

				img.onload = function () {
					var canvas = document.createElement("canvas");
					canvas.width =this.width;
					canvas.height =this.height;

					var ctx = canvas.getContext("2d");
					ctx.drawImage(this, 0, 0);

					var dataURL = canvas.toDataURL("image/png");

					//The image data stored as a base64 to be displayed
					var justData = dataURL.replace(/^data:image\/(png|jpg);base64,/, "");

					vm.activeSocialPhoto.src = id;
					vm.activeSocialPhoto.socialImg = justData;
				};

				img.src = url;
			};

			vm.setUpProfileTravelEditDescribeDirective();

        }
    }

})();




