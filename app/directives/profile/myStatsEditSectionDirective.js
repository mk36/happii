(function () {

    angular
        .module('app')
        .directive("myStatsEditSectionDirective", myStatsEditSectionSetUp);


    function myStatsEditSectionSetUp($timeout){


        var directive = {

            templateUrl : 'app/views/profile/sections/myStatsEditSection.html',

            controller: myStatsEditSectionController,

            controllerAs: 'vm',
            bindToController: true,

            scope: {

				profile : '=',
				editmode : '=',
				edittitleshow : '=',
				icon : '=',
				title : '=',
				profiletype : '=',
				ownregisterinfo : '='
            }
        };

        return directive;



        /**
         *	Set up controller
         */
        function myStatsEditSectionController ($scope, $state, questionService, builderService) {

            var vm = this;

			vm.activeDetails = {};

			vm.statsQuestions = [];


			vm.children = {questionRef : 'children', values : [], iconRef : 'children', title : 'children', type : 'radio', min : '1', answer : ''};
			vm.bodyType = {questionRef : 'bodyType', values : [], iconRef : 'body type', title : 'body type', type : 'radio', min : '1', answer : ''};
			vm.drinking = {questionRef : 'drinking', values : [], iconRef : 'drinking', title : 'drinking', type : 'radio', min : '1', answer : ''};
			vm.employment = {questionRef : 'employment', values : [], iconRef : 'employability', title : 'employment', type : 'radio', min : '1', answer : ''};
			vm.education = {questionRef : 'education', values : [], iconRef : 'education', title : 'education', type : 'radio', min : '1', answer : ''};
			vm.religion = {questionRef : 'religion', values : [], iconRef : 'religion', title : 'religion', type : 'radio', min : '1', answer : ''};
			vm.smoking = {questionRef : 'smoking', values : [], iconRef : 'smoking', title : 'smoking', type : 'radio', min : '1', answer : ''};


			vm.gender = { questionRef : 'partnerGender', values : ['male', 'female'], iconRef : 'male', title : 'sex', type : 'radio', min : '1', answer : '' };
			vm.height = { questionRef : 'partnerHeight', value : {min : 3, max : 7}, iconRef : 'height', title : 'height', type : 'range', min : '1', answer : ''};
			vm.ageRange = { questionRef : 'partnerAgeRange', value : {min : 18, max : 70}, iconRef : 'age', title : 'age range', type : 'range', min : '1', answer : ''};

			vm.from = {title : 'from', answer : ''};
			vm.lives = {title : 'lives', answer : ''};
			vm.within = {title : 'within', answer : ''};


			/**
			 * Set up the image edit directive
			 */
			vm.setUpMyStatsEditSectionDirective = function(){

				vm.setUpBasicStats();

				vm.setUpAlternativeQuestionAnswer();
            };

			/**
			 * Set up the basic stats (radio button questions)
			 *
			 * requires if the data is for the person or the
			 * partner they desire
			 *
			 * @param type
			 */
			vm.setUpBasicStats = function(type){

				//Children
				vm.setUpQuestionAnswer(vm.children, 'children', 'children', 'partnerChildren');

				//body type
				vm.setUpQuestionAnswer(vm.bodyType, 'bodyType', 'bodyType', 'partnerBodyType');

				//drinking
				vm.setUpQuestionAnswer(vm.drinking, 'drinking', 'drinking', 'partnerDrinking');

				//employment
				vm.setUpQuestionAnswer(vm.employment, 'employment', 'employment', 'partnerEmployment');

				//religion
				vm.setUpQuestionAnswer(vm.religion, 'religion', 'religion', 'partnerReligion');

				//smoking
				vm.setUpQuestionAnswer(vm.smoking, 'smoking', 'smoking', 'partnerSmoking');

				//education
				vm.setUpQuestionAnswer(vm.education, 'education', 'education', 'partnerEducation');

			};

			/**
			 * Set up the question answer for a basic stats question
			 *
			 * @param localQuestion
			 * @param questionData
			 * @param ownRef
			 * @param partnerRef
			 */
			vm.setUpQuestionAnswer = function (localQuestion, questionData, ownRef, partnerRef) {


				questionService.getQuestionData(questionData).then(function(data){

					localQuestion.values = data.data.options;

					//Set the profile answer depending on if it is partner or own data
					if(vm.profiletype == 'own'){
						localQuestion.answer = vm.profile.properties.questions[ownRef];
					}
					else{
						localQuestion.answer = vm.profile.properties.questions[partnerRef];
					}

					vm.statsQuestions.push(localQuestion);
				});
			};

			vm.setUpAlternativeQuestionAnswer = function () {

				vm.setUpAgeRange();
				vm.setUpHeight();

				vm.setUpLocationFrom();

				vm.setUpGender();

				//Set the profile answer depending on if it is partner or own data
				if(vm.profiletype == 'own'){

					vm.lives.answer = vm.ownregisterinfo.location.address;

					vm.statsQuestions.push(vm.lives);
				}
				else{


				}
			};

			/**
			 * Set up gender
			 */
			vm.setUpGender = function () {

				//Set the profile answer depending on if it is partner or own data
				if(vm.profiletype == 'own'){
					vm.gender.answer = vm.ownregisterinfo.gender;
				}
				else{
					vm.gender.answer = vm.profile.properties.questions.partnerGender;
				}

				vm.statsQuestions.push(vm.gender);
			};

			/**
			 * Set up location from
			 */
			vm.setUpLocationFrom = function () {

				//Set the profile answer depending on if it is partner or own data
				if(vm.profiletype == 'own'){
					vm.from.answer = vm.profile.properties.questions.locationFrom.address;
				}
				else{
					vm.from.answer = vm.profile.properties.questions.partnerLocation.name;
					vm.within.answer = vm.profile.properties.questions.partnerLocation.miles + ' miles';

					vm.statsQuestions.push(vm.within);
				}

				vm.statsQuestions.push(vm.from);
			};

			/**
			 * Set up height
			 */
			vm.setUpHeight = function () {

				//Set the profile answer depending on if it is partner or own data
				if(vm.profiletype == 'own'){
					vm.height.answer = vm.profile.properties.questions.height;
				}
				else{
					var height = vm.profile.properties.questions.partnerHeight;
					vm.height.answer = 'Between ' + height.min + ' and ' + height.max;
				}

				vm.statsQuestions.push(vm.height);
			};

			/**
			 * Set up age range question
			 */
			vm.setUpAgeRange = function () {

				//Set the profile answer depending on if it is partner or own data
				if(vm.profiletype == 'own'){
					vm.ageRange.answer = getAge(vm.ownregisterinfo.dob);
				}
				else{
					var range = vm.profile.properties.questions.partnerAgeRange;
					vm.ageRange.answer = 'Between ' + range.min + ' and ' + range.max;
				}

				vm.statsQuestions.push(vm.ageRange);
			};

			/**
			 * Get the age
			 *
			 * @param dateString
			 * @returns {number}
			 */
			function getAge(dateString)
			{
				var today = new Date();
				var birthDate = new Date(dateString);
				var age = today.getFullYear() - birthDate.getFullYear();
				var m = today.getMonth() - birthDate.getMonth();
				if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate()))
				{
					age--;
				}
				return age;
			}

			/**
			 * Set active details
			 *
			 * @param details
			 */
			vm.setActiveDetails = function(details){

				vm.activeDetails = details;

				if(vm.edittitleshow == true){
					//show the edit modal

				}
			};



			vm.setUpMyStatsEditSectionDirective();

        }
    }

})();




