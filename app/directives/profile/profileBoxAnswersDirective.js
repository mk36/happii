(function () {

    angular
        .module('app')
        .directive("profileBoxAnswersDirective", profileBoxAnswersSetUp);


    function profileBoxAnswersSetUp($timeout){


        var directive = {

            templateUrl : 'app/views/profile/profileBoxAnswers.html',

            controller: profileBoxAnswersController,

            controllerAs: 'vm',
            bindToController: true,

            scope: {
				icon : '=',
				datatoshow : '=',
				title : '=',
				columns : '=',
				edittitleshow : '=',
				editmode : '=',
				questionref : '=',
				min : '=',
				profile : '='
            }
        };

        return directive;


        /**
         *	Set up controller
         */
        function profileBoxAnswersController ($scope, $state, questionService, builderService) {

            var vm = this;

			vm.errorMsg = '';
			vm.selectedOptions = [];

			vm.dataOptions = [];


            vm.setUpProfileBoxAnswers = function () {

				$timeout(function(){

					vm.setUpQuestionDataToDisplay();

				}, 100);

            	console.log('data to show', vm.datatoshow);
			};

			//Edit code below

			/**
			 * Set up the data to show in the radio buttons
			 */
			vm.setUpQuestionDataToDisplay = function(){

				questionService.getQuestionData(vm.questionref).then(function(data){

					//console.log('got question data', data);

					vm.dataOptions = data.data.options;

					//console.log('data to display', vm.dataToDisplay);
				}, function(){
					console.log('failed to get question data');
				});
			};

			/**
			 * Update the profile past time property
			 */
			vm.updateProfile = function(){

				if(vm.min){

					//console.log('min amount', vm.min);

					if(vm.selectedOptions.length >= vm.min){

						vm.profile.properties.questions[vm.questionref] = vm.selectedOptions;
						vm.profile.update();

						//reset selected
						vm.selectedOptions = [];

						vm.editmode = false;
					}
					else{
						vm.errorMsg = 'You need to select at least ' + vm.min;
					}

				}

			};

			/**
			 * Set the checkbox option in the selectedCheckbox array
			 *
			 * Removes it from the array if the checkbox is unchecked
			 *
			 * @param value
			 */
			vm.setCheckBoxOption = function(value){

				//console.log('value', value);

				if(vm.selectedOptions.includes(value)){

					//Remove the checked element from the selected option array
					_.pull(vm.selectedOptions , value);

				}
				else{
					//Add the checked element to the selected option array
					vm.selectedOptions.push(value);
				}

				//console.log('selected personality options', vm.selectedOptions);
			};

			vm.setUpProfileBoxAnswers();
        }
    }

})();




