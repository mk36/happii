(function () {

    angular
        .module('app')
        .directive("profileHeaderDirective", profileHeaderSetUp);


    function profileHeaderSetUp($timeout){


        var directive = {

            templateUrl : 'app/views/profile/sections/header.html',

            controller: profileHeaderController,

            controllerAs: 'vm',
            bindToController: true,

            scope: {
				profileimage : '=',
				page : '='
            }
        };

        return directive;



        /**
         *	Set up controller
         */
        function profileHeaderController ($scope, $state, questionService, builderService, loginService, profileService) {

            var vm = this;

            vm.profileId = 0;
            vm.deviceType = '';

			/**
			 * Set up the image edit directive
			 */
			vm.setUpProfileHeaderDirective = function(){

            	$timeout(function(){

					//console.log('The passed profile image', vm.profileimage);
					var profileObject = profileService.getUserAccountDetails().then(function (data) {

						console.log('header profile service auth', data);
						vm.profileId = data.user.id;
					});

					if(window.innerWidth <  768 ){

						vm.deviceType = 'mobile';
					}

				}, 100);

            };

			/**
			 * Get the image path
			 *
			 * @param imageName
			 */
			vm.getImagePath = function(imagename){

				return window.server_path + 'image/' + imagename;
			};

			vm.returnToOwnProfile = function(){

				//Go to the next state
				$state.transitionTo('profile.profile-edit');
			};

			vm.showMobileMenu = function(){


				var currentWidth = $('#profile-sidebar-wrapper').css('width');
				var widthToAnimate = '0px';

				console.log('showing mobile menu', currentWidth);

				if(currentWidth == '200px'){
					widthToAnimate = '0px';
					$('#mobile-profile-menu').removeClass('active')

				}
				else{
					widthToAnimate = '200px';
					$('#mobile-profile-menu').addClass('active')
				}

				$('#profile-sidebar-wrapper').animate({
					width : widthToAnimate
				});

				console.log('showing mobile menu', currentWidth);
			};

			/**
			 * Log out of your profile
			 */
			vm.logOut = function () {
				loginService.logOut().then(function () {

					$state.go('sign-in');
				});


			};

			vm.setUpProfileHeaderDirective();


		}
    }

})();





