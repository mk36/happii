(function () {

    angular
        .module('app')
        .directive("profileTravelEditDirective", profileTravelEditSetUp);


    function profileTravelEditSetUp($timeout){


        var directive = {

            templateUrl : 'app/views/profile/sections/travel/travelEdit.html',

            controller: profileTravelEditController,

            controllerAs: 'vm',
            bindToController: true,

            scope: {
            	profileobj : '=',
				travelbeen : '=',
				travelgo : '='
            }
        };

        return directive;


        /**
         *	Set up controller
         */
        function profileTravelEditController ($scope, $state, questionService, profileService) {

            var vm = this;

			//The placeholder items when a travel option hasn't been filled
			vm.travelBeenPlaceholderItems = [];
			vm.travelGoPlaceholderItems = [];

			/**
			 * Watch for changes on the show variable
			 */
			$scope.$watch('vm.travelbeen', function(value, old) {

			}, true);

			$scope.$watch('vm.travelgo', function(value, old) {

			}, true);

			$scope.$watch('vm.profileobj', function(value, old) {
				//vm.setUpProfileTravelEditDirective();
			}, true);

			/**
			 * Set up the image edit directive
			 */
			vm.setUpProfileTravelEditDirective = function(){

            	$timeout(function(){

					vm.setUpGoogleInput('places-been-input', vm.travelbeen);
					vm.setUpGoogleInput('places-go-input', vm.travelgo);

					vm.setUpPlaceholderList(vm.travelBeenPlaceholderItems, 'place');
					vm.setUpPlaceholderList(vm.travelGoPlaceholderItems, 'place');

					//console.log('been', vm.travelbeen, 'go', vm.travelgo, 'profile object', vm.profileobj);

				}, 100);

            };

			/**
			 * Set up the placeholder lists
			 */
			vm.setUpPlaceholderList = function(model, name){

				for(i = 0; i < 4; i++){

					model.push({title: name + (i + 1)});
				}
			};


			/**
			 * Set up a google places input
			 *
			 * @param inputId
			 * @param modelToUpdate
			 */
			vm.setUpGoogleInput = function(inputId, modelToUpdate){

				var inputSearchPlacesBeen = document.getElementById(inputId);
				var searchPlacesBeenBox = new google.maps.places.SearchBox(inputSearchPlacesBeen);

				//Create a listener for when a valid place is selected
				searchPlacesBeenBox.addListener('places_changed', function() {


					var places = searchPlacesBeenBox.getPlaces();

					places.forEach(function(place) {

						console.log('place', place);
						var latLng = place.geometry.location;

						vm.addPlacesToTravelSelected(modelToUpdate, place.name, latLng.lat(), latLng.lng());

						$('#' + inputId).val('');

						$scope.$apply();
					})

				});
			};

			/**
			 * Check if a place can be added to the selected places
			 *
			 * @param model
			 * @param item
			 * @returns {boolean}
			 */
			vm.checkPlacesConditionsToAdd = function(model, item){

				var index = _.findIndex(model, function(place) { return place.name == item.name});

				if(index < 0 && model.length < 4 && item != "") {

					return true;
				}

				return false;
			};

			/**
			 * Add a place to the selected travel model
			 *
			 * @param model
			 * @param name
			 * @param lat
			 * @param lng
			 */
			vm.addPlacesToTravelSelected = function(model, name, lat, lng){

				var location = {

					name : name,
					lat: lat,
					lng: lng
				};

				if(vm.checkPlacesConditionsToAdd(model, location) == true){

					model.push(location);
					console.log('adding new place', model);
				}
			};

			/**
			 * Remove a place from the selected travel model
			 *
			 * @param model
			 * @param item
			 */
			vm.removePlacesItem = function(model, item){

				//Remove the item from the selected option array
				_.pull(model , item);

				console.log('removing place', model);
			};

			/**
			 * Save the selection to the typescript object
			 *
			 */
			vm.saveSelection = function(){

				//Update the profile builder object
				var profileBuilderObj = vm.profileobj;

				console.log('profile builder object', profileBuilderObj);

				//Save travel been
				profileBuilderObj.setQuestionValueByQuestionReference('travelBeen', vm.travelbeen);

				//Save travel go
				profileBuilderObj.setQuestionValueByQuestionReference('travelGo', vm.travelgo);

				if(vm.travelbeen.length > 0){

					//Go to the places describe
					vm.goToTravelDescribeSection();
				}
				else{

				}

			};

			/**
			 * Go to the travel describe section
			 *
			 */
			vm.goToTravelDescribeSection = function(){

				//Open the describe modal
				$("#travel-edit-describe-modal").modal({show: true, backdrop: false});

				//Close this modal
				$("#travel-edit-modal").modal('hide');
			};




			vm.setUpProfileTravelEditDirective();

        }
    }

})();




