(function () {

    angular
        .module('app')
        .directive("profileSlidingIconSectionLookingDirective", profilePastTimeSectionSetUp);


    function profilePastTimeSectionSetUp($timeout){


        var directive = {

            templateUrl : 'app/views/profile/sections/iconSliderSectionLooking.html',

            controller: profileSlidingIconSectionController,

            controllerAs: 'vm',
            bindToController: true,

            scope: {
				partnerdata : '=',
				slidericon : '=',
				title : '=',
				profile : '=',
				questionref : '='
            }
        };

        return directive;



        /**
         *	Set up controller
         */
        function profileSlidingIconSectionController ($scope, $state, questionService, builderService) {

            var vm = this;

			//new stuff
			vm.activeDetails = {};

			vm.slickConfig = {};

			vm.lookingForQuestions = [];

			vm.children = {questionRef : 'partnerChildren', values : [], iconRef : 'children', title : 'children', type : 'radio', min : '1'};
			vm.bodyType = {questionRef : 'partnerBodyType', values : [], iconRef : 'body type', title : 'body type', type : 'radio', min : '1'};
			vm.drinking = {questionRef : 'partnerDrinking', values : [], iconRef : 'drinking', title : 'drinking', type : 'radio', min : '1'};
			vm.employment = {questionRef : 'partnerEmployment', values : [], iconRef : 'employability', title : 'employment', type : 'radio', min : '1'};
			vm.religion = {questionRef : 'partnerReligion', values : [], iconRef : 'religion', title : 'religion', type : 'radio', min : '1'};
			vm.smoking = {questionRef : 'partnerSmoking', values : [], iconRef : 'smoking', title : 'smoking', type : 'radio', min : '1'};


			vm.gender = { questionRef : 'partnerGender', values : ['male', 'female'], iconRef : 'male', title : 'gender', type : 'radio', min : '1' };
			vm.height = { questionRef : 'partnerHeight', value : {min : 3, max : 7}, iconRef : 'height', title : 'height', type : 'range', min : '1'};
			vm.ageRange = { questionRef : 'partnerAgeRange', value : {min : 18, max : 70}, iconRef : 'age', title : 'age range', type : 'range', min : '1'};
			vm.pastTimes = { questionRef : 'partnerPastTimes', values : [], iconRef : 'male', title : 'past times', type : 'checkbox', min : '1'};
			vm.personality = { questionRef : 'personality', values : [], iconRef : 'male', title : 'personality', type : 'checkbox', min : '1'};

			/**
			 * Set up the image edit directive
			 */
			vm.setUpProfileSlidingIconSectionDirective = function(){

				$timeout(function(){

					vm.setUpSlickSlider();
				}, 400);

				vm.setUpLookingForData();
            };

			/**
			 * Set up the looking for data
			 */
			vm.setUpLookingForData = function () {

				vm.lookingForQuestions.push(vm.ageRange);
				vm.lookingForQuestions.push(vm.height);

				vm.setUpBasicStats();
				vm.setUpPastTimes();
				vm.setUpPersonality();

			};

			/**
			 * Set up past times
			 */
			vm.setUpPastTimes = function(){

				//Children
				questionService.getQuestionData('pastTimes').then(function(data){

					vm.pastTimes.values = data.data.options;
					vm.lookingForQuestions.push(vm.pastTimes);
				});
			};

			/**
			 * Set up personality
			 */
			vm.setUpPersonality = function(){

				//Children
				questionService.getQuestionData('personality').then(function(data){

					vm.personality.values = data.data.options;
					vm.lookingForQuestions.push(vm.personality);
				});
			};

			/**
			 * Set up the basic stats
			 */
			vm.setUpBasicStats = function(){

				//Children
				questionService.getQuestionData('children').then(function(data){

					vm.children.values = data.data.options;
					vm.lookingForQuestions.push(vm.children);
				});

				//body type
				questionService.getQuestionData('bodyType').then(function(data){

					vm.bodyType.values = data.data.options;
					vm.lookingForQuestions.push(vm.bodyType);
				});

				//drinking
				questionService.getQuestionData('drinking').then(function(data){

					vm.drinking.values = data.data.options;
					vm.lookingForQuestions.push(vm.drinking);
				});

				//employment
				questionService.getQuestionData('employment').then(function(data){

					vm.employment.values = data.data.options;
					vm.lookingForQuestions.push(vm.employment);
				});

				//religion
				questionService.getQuestionData('religion').then(function(data){

					vm.religion.values = data.data.options;
					vm.lookingForQuestions.push(vm.religion);
				});

				//smoking
				questionService.getQuestionData('smoking').then(function(data){

					vm.smoking.values = data.data.options;
					vm.lookingForQuestions.push(vm.smoking);
				});

			};

			/**
			 * Set active details
			 *
			 * @param details
			 */
			vm.setActiveDetails = function(details){

				vm.activeDetails = details;
			};

			/**
			 * Set up the slick slider
			 */
			vm.setUpSlickSlider = function(){

				//The love slick settings
				vm.slickConfig = {
					enabled: true,
					//autoplay: true,
					draggable: false,
					arrows: false,
					infinite: true,
					slidesToShow: 5,
					slidesToScroll: 5,
					method: {},
					event: {
						beforeChange: function (event, slick, currentSlide, nextSlide) {

						},
						afterChange: function (event, slick, currentSlide, nextSlide) {

						},
						init: function () {

						}
					},
					responsive: [{
						breakpoint: 480,
						settings: {
							slidesToShow: 1,
							slidesToScroll: 1
						}
					},
						{
							breakpoint: 1024,
							settings: {
								slidesToShow: 1,
								slidesToScroll: 1
							}
						}]
				};
			};

			vm.setUpProfileSlidingIconSectionDirective();

        }
    }

})();




