(function () {

    angular
        .module('app')
        .directive("photoPopUpDirective", photoPopUpSetUp);


    function photoPopUpSetUp($timeout){


        var directive = {

            templateUrl : 'app/views/profile/sections/photo/photoPopUp.html',

            controller: photoPopUpController,

            controllerAs: 'vm',
            bindToController: true,

            scope: {
				photos : '=',
				videos : '=',
				username : '=',
				mode : '='
            }
        };

        return directive;


        /**
         *	Set up controller
         */
        function photoPopUpController ($scope, $state, questionService, builderService) {

            var vm = this;

			vm.slickConfig = {};
			vm.show = true;
			vm.currentSlide = 1;

			//
			vm.media = [];
			vm.backTitle = '';


			$scope.$watch('vm.photos', function(value, old) {

				vm.show = false;

				$timeout(function(){

					vm.show = true;
					//console.log('slider being set up again');
				}, 200);

			}, true);
			$scope.$watch('vm.videos', function(value, old) {

				vm.show = false;

				$timeout(function(){

					vm.show = true;
					//console.log('slider being set up again');
				}, 200);

			}, true);
			$scope.$watch('vm.mode', function(value, old) {

				$timeout(function(){

					vm.manageMediaMode();
				}, 200);

			}, true);

			/**
			 * Set up the image edit directive
			 */
			vm.setUpPhotoPopUpDirective = function(){

            	$timeout(function(){

            		console.log('photo object', vm.photos);

					vm.setUpSlickSlider();
				}, 500);
            };

			/**
			 * Manage whether to show videos or photos
			 */
			vm.manageMediaMode = function(){

				vm.media = [];

				if(vm.mode == 'photo'){
					vm.media = vm.photos;
					vm.backTitle = 'Videos';
				}
				else if(vm.mode == 'video'){
					vm.media = vm.videos;
					vm.backTitle = 'Photos';
				}

				console.log('media mode changed', vm.media);
			};

			/**
			 * Switch the media mode
			 */
			vm.switchMediaMode = function(){

				if(vm.mode == 'photo'){
					vm.mode = 'video';
				}
				else if(vm.mode == 'video'){
					vm.mode = 'photo';
				}

				vm.manageMediaMode();
			};

			/**
			 * Set up the slick slider
			 */
			vm.setUpSlickSlider = function(){

				//The love slick settings
				vm.slickConfig = {
					enabled: true,
					//autoplay: true,
					draggable: false,
					arrows: false,
					infinite: true,
					slidesToShow: 1,
					slidesToScroll: 1,
					method: {},
					event: {
						beforeChange: function (event, slick, currentSlide, nextSlide) {

						},
						afterChange: function (event, slick, currentSlide, nextSlide) {

							vm.currentSlide = currentSlide;
							console.log('current slide', vm.currentSlide, 'slicks slide', currentSlide);
						},
						init: function () {

						}
					}
				};
			};

			/**
			 * Get the server url for the image
			 *
			 * @param image
			 * @returns {string}
			 */
			vm.getPhotoImageUrl = function(image){

				//console.log('path for image', window.server_path + 'image/' + imageName);
				return window.server_path + 'image/' + image.name;
			};

			/**
			 * Get the server url for the video
			 *
			 * @param image
			 * @returns {string}
			 */
			vm.getVideoUrl = function(video){

				//console.log('path for image', window.server_path + 'image/' + imageName);
				return window.server_path + 'video/' + video.name;
			};

			/**
			 * Closes the map modal
			 */
			vm.closePhotoModal = function(){

				//Show the modal
				$("#photo-modal").modal("hide");
			};

			vm.setUpPhotoPopUpDirective();

        }
    }

})();




