(function () {

    angular
        .module('app')
        .directive("iconLookup", iconLookupSetUp);


    function iconLookupSetUp($timeout){


        var directive = {

            templateUrl : 'app/views/directives/iconLookup.html',

            controller: iconLookupController,

            controllerAs: 'vm',
            bindToController: true,

            scope: {
				iconname : '='
            }
        };

        return directive;



        /**
         *	Set up controller
         */
        function iconLookupController ($scope, $state, questionService, builderService) {

            var vm = this;

            vm.iconSrc = '';

            //Dictionart of key, value pairs for icons
            vm.iconList = [

				//Past times
				{name : 'Reading', src : 'images/icons/profile/loveIcons/READING.png'},
				{name : 'Tv', src : 'images/icons/profile/loveIcons/TV.png'},
				{name : 'Movies', src : 'images/icons/profile/loveIcons/MOVIES.png'},
				{name : 'Theatre', src : 'images/icons/profile/loveIcons/THEATRE.png'},
				{name : 'Gigs/Music', src : 'images/icons/profile/loveIcons/MUSIC.png'},
				{name : 'Dog walking', src : 'images/icons/profile/loveIcons/DOG_WALKING.png'},
				{name : 'Partying', src : 'images/icons/profile/loveIcons/partying.png'},
				{name : 'Computers/Tech', src : 'images/icons/profile/loveIcons/COMPUTERS.png'},
				{name : 'Gardening', src : 'images/icons/profile/loveIcons/GARDENING.png'},
				{name : 'Socialising', src : 'images/icons/profile/loveIcons/SOCIALISING.png'},
				{name : 'Galleries & exhibitions', src : 'images/icons/profile/loveIcons/GALLERY.png'},
				{name : 'Cycling', src : 'images/icons/profile/loveIcons/CYCLING.png'},
				{name : 'Foodie/cooking', src : 'images/icons/profile/loveIcons/FOODIE.png'},
				{name : 'Eating out', src : 'images/icons/profile/loveIcons/EATING_OUT.png'},
				{name : 'Writing', src : 'images/icons/profile/loveIcons/WRITING.png'},
				{name : 'Volunteer work', src : 'images/icons/profile/loveIcons/VOLUNTEER.png'},
				{name : 'Playing instruments', src : 'images/icons/profile/loveIcons/PLAYING_INSTRUMENT.png'},
				{name : 'Photography', src : 'images/icons/profile/loveIcons/PHOTGRAPHY.png'},
				{name : 'Walking/Hiking', src : 'images/icons/profile/loveIcons/WALKING.png'},
				{name : 'Sport', src : 'images/icons/profile/loveIcons/SPORT.png'},
				{name : 'Travel', src : 'images/icons/profile/loveIcons/TRAVEL.png'},
				{name : 'Festivals', src : 'images/icons/profile/loveIcons/FESTIVALS.png'},
				{name : 'Sewing/knitting', src : 'images/icons/profile/loveIcons/SEWING.png'},
				{name : 'Any', src : 'images/icons/profile/Traits/AnyTrait.png'},
				{name : 'Any', src : 'images/icons/profile/Traits/AnyActivity.png'},


				//Traits
				{name : 'adventurous', src : 'images/icons/profile/Traits/Adventurous.png'},
				{name : 'age', src : 'images/icons/profile/Traits/Age.png'},
				{name : 'articulate', src : 'images/icons/profile/Traits/Articulate.png'},
				{name : 'blunt', src : 'images/icons/profile/Traits/Blunt.png'},
				{name : 'body type', src : 'images/icons/profile/Traits/Body_type.png'},
				{name : 'bohemian', src : 'images/icons/profile/Traits/Bohemian.png'},
				{name : 'children', src : 'images/icons/profile/Traits/Children.png'},
				{name : 'confident', src : 'images/icons/profile/Traits/Confident.png'},
				{name : 'crazy', src : 'images/icons/profile/Traits/Crazy.png'},
				{name : 'creative', src : 'images/icons/profile/Traits/Creative.png'},
				{name : 'debonair', src : 'images/icons/profile/Traits/Debonair.png'},
				{name : 'dramatic', src : 'images/icons/profile/Traits/Dramatic.png'},
				{name : 'drinking', src : 'images/icons/profile/Traits/Drinking.png'},
				{name : 'eccentric', src : 'images/icons/profile/Traits/Eccentric.png'},
				{name : 'education', src : 'images/icons/profile/Traits/Education.png'},
				{name : 'employability', src : 'images/icons/profile/Traits/Employability.png'},
				{name : 'employment', src : 'images/icons/profile/Traits/Employability.png'},
				{name : 'faithful', src : 'images/icons/profile/Traits/Faithful.png'},
				{name : 'female', src : 'images/icons/profile/Traits/Female.png'},
				{name : 'height', src : 'images/icons/profile/Traits/Height.png'},
				{name : 'impulsive', src : 'images/icons/profile/Traits/Impulsive.png'},
				{name : 'intense', src : 'images/icons/profile/Traits/Intense.png'},
				{name : 'laid back', src : 'images/icons/profile/Traits/Laid back.png'},
				{name : 'lazy', src : 'images/icons/profile/Traits/Lazy.png'},
				{name : 'location', src : 'images/icons/profile/Traits/Location.png'},
				{name : 'male', src : 'images/icons/profile/Traits/Male.png'},
				{name : 'mellow', src : 'images/icons/profile/Traits/Mellow.png'},
				{name : 'outspoken', src : 'images/icons/profile/Traits/Outspoken.png'},
				{name : 'perfectionist', src : 'images/icons/profile/Traits/Perfectionist.png'},
				{name : 'political', src : 'images/icons/profile/Traits/Political.png'},
				{name : 'positive', src : 'images/icons/profile/Traits/Positive.png'},
				{name : 'punctual', src : 'images/icons/profile/Traits/Punctual.png'},
				{name : 'quiet', src : 'images/icons/profile/Traits/Quiet.png'},
				{name : 'quirky', src : 'images/icons/profile/Traits/Quirky.png'},
				{name : 'rebellious', src : 'images/icons/profile/Traits/Rebellious.png'},
				{name : 'reliable', src : 'images/icons/profile/Traits/Reliable.png'},
				{name : 'religion', src : 'images/icons/profile/Traits/Religion.png'},
				{name : 'risk taker', src : 'images/icons/profile/Traits/Risk taker.png'},
				{name : 'sentimental', src : 'images/icons/profile/Traits/Sentimental.png'},
				{name : 'shy', src : 'images/icons/profile/Traits/Shy.png'},
				{name : 'smoking', src : 'images/icons/profile/Traits/Smoking.png'},
				{name : 'sociable', src : 'images/icons/profile/Traits/Sociable.png'},
				{name : 'sporty', src : 'images/icons/profile/Traits/Sporty.png'},
				{name : 'tidy', src : 'images/icons/profile/Traits/Tidy.png'},
				{name : 'traditional', src : 'images/icons/profile/Traits/Traditional.png'},
				{name : 'Well-read', src : 'images/icons/profile/Traits/Traditional.png'}
			];

			/**
			 * Set up the image edit directive
			 */
			vm.setUpIconLookupDirective = function(){

				vm.getIconByName(vm.iconname);
            };

			/**
			 * Get the icon by the name passed
			 *
			 * @param name
			 */
			vm.getIconByName = function(name){

				for(var i = 0; i < vm.iconList.length; i++){

					if(vm.iconList[i].name.toLowerCase() == name.toString().toLowerCase()){
						//Set the icon src
						vm.iconSrc = vm.iconList[i].src;
					}
				}

				//console.log('icon passed', vm.iconname, 'icon src', vm.iconSrc);
			};

			vm.setUpIconLookupDirective();

        }
    }

})();




