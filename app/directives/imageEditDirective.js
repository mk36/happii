(function () {

    angular
        .module('app')
        .directive("imageEditDirective", imageEditSetUp);


    function imageEditSetUp($timeout){


        var directive = {

            templateUrl : 'app/views/directives/imageEditDirective.html',

            controller: imageEditController,

            controllerAs: 'vm',
            bindToController: true,

            scope: {
				imageobj : '=',
				show : '=',
				aspectratio : '=',
				callback : '='
            }
        };

        return directive;



        /**
         *	Set up controller
         */
        function imageEditController ($scope, $state, questionService, builderService) {

            var vm = this;

            vm.cropper = {};

			/**
			 * Watch for changes on the show variable
			 */
			$scope.$watch('vm.show', function(value, old) {

				$timeout(function(){

					//If the image edit is show, intitialise the cropper
					if(vm.show == true){
						//Set up cropper
						vm.setUpCropper();
					}


				}, 1000);

			}, true);

			$scope.$watch('vm.imageobj', function(value, old) {

				//console.log('old image', old, 'new image', value);

			}, true);

			/**
			 * Set up the image edit directive
			 */
			vm.setUpImageEditDirective = function(){

            	$timeout(function(){

					//console.log('url of image to edit', vm.imageobj, 'aspect ratio', vm.aspectratio);

				}, 100);

            };

			/**
			 * Set up the image cropper feature
			 */
			vm.setUpCropper = function(){

				var image = document.getElementById('image-to-crop');

				window.Cropper;

				vm.cropper = new Cropper(image, {
					aspectRatio: _.parseInt(vm.aspectratio),
					cropBoxResizable : false,
					crop: function(e) {

					},
					//autoCrop: false,
					ready: function () {
						// Do something here
						// ...
						console.log('cropper set up');

						// And then
						this.cropper.crop();
					}
				});

				//

			};

			/**
			 * Save the cropped image to the server
			 */
			vm.saveCroppedImage = function(){

				vm.cropper.getCroppedCanvas().toBlob(function (blob) {
					var formData = new FormData();

					formData.append('croppedImage', blob);

					//Create the image upload object
					var imageUploadObj = {
						form : blob,
						fileName : vm.imageobj.name
					};

					//console.log('image obj to upload', imageUploadObj);

					//Upload the image to the server
					questionService.uploadImageToServer(imageUploadObj, vm.imageobj.name).then(function(data){

						//console.log('path to server image', window.server_path + 'image/' + data.data[0].file_name);

						//Update the cropped image
						vm.imageobj = {
							name : data.data[0].file_name,
							type : 'server'
						};

						if(vm.aspectratio.toString() == '1'){
							//Send the image object to the callback
							vm.callback(vm.imageobj, 'profileImage');
						}
						else if(vm.aspectratio.toString() == '3'){
							vm.callback(vm.imageobj, 'coverPhoto');
						}


						//Hide the directive
						vm.show = false;

						//Reset the cropper object
						vm.cropper.destroy();
					})
				});

			};

			/**
			 * Get the image path
			 *
			 * @param imageName
			 */
			vm.getImagePath = function(imageObj){

				var path = '';

				if(imageObj.type == 'server'){

					path = window.server_path + 'image/' + imageObj.name;
				}
				else if (imageObj.type == 'facebook'){

					path = imageObj.src;
				}

				return path;
			};


			vm.setUpImageEditDirective();

        }
    }

})();




