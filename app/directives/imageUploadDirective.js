(function () {

    angular
        .module('app')
        .directive("imageUploadDirective", imageUploadSetUp);


    function imageUploadSetUp($timeout){


        var directive = {

            templateUrl : 'app/views/profile/sections/photo/imageUploadDirective.html',

            controller: imageUploadController,

            controllerAs: 'vm',
            bindToController: true,

            scope: {
				images : '=',
				type : '='
            }
        };

        return directive;


        /**
         *	Set up controller
         */
        function imageUploadController ($http, $scope, $state, questionService, profileService, authService) {

            var vm = this;

			vm.selectedPhoto = {name : ''};
			vm.profileAspectRatio = '';

			vm.showImageEditor = false;

			vm.galleryPhotos = [];

			vm.socialPhotos = [];
			vm.activeSocialPhoto = {name : ''};
			vm.accessTokens = {
				facebook : ''
			};

            vm.activeMode = '';

			$scope.$watch('vm.images', function(value, old) {

				//console.log('old images', old, 'new images', value);

			}, true);

			/**
			 * Set up the image edit directive
			 */
			vm.setUpImageUploadDirective = function(){

            	$timeout(function(){

					//console.log('images passed', vm.images);
					vm.setUpDropzone();

				}, 100);

            };

			/**
			 * Set up drop zone for drag and drop file uploading
			 */
			vm.setUpDropzone = function(){

				/**
				 * Dropzone image upload
				 */
				$("div#photo-upload-display").dropzone({ url: window.server_path + "upload/", paramName: "file", // The name that will be used to transfer the file
					maxFilesize: 5, // MB
					clickable: true,
					accept: function(file, done) {

						//Create the image upload object
						var imageUploadObj = {
							form : file,
							fileName : file.name
						};

						//Upload th eimage to the server
						questionService.uploadImageToServer(imageUploadObj, file.name).then(function(data){

							//console.log('image path returned', data.data[0].file_name);
							var date = new Date();
							date = date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();

							console.log('date', date);

							//Add the photo to the photo gallery
							var photo = {name : data.data[0].file_name, type : 'server', date : date};
							vm.images.push(photo);

							//Update the photo gallery to typescript
							vm.updateBuilderObjectGallery();

						})
					}});
			};

			/**
			 * turn the cover/profile photo mode on
			 */
			vm.setActivePhotoMode = function(mode, aspectRatio){

				if(vm.selectedPhoto.name != ""){
					vm.activeMode = mode;
					vm.profileAspectRatio = aspectRatio;

					vm.showImageEditor = true;
				}

				console.log('active mode', vm.activeMode, 'aspect ratio', vm.profileAspectRatio);
			};

			/**
			 * Set the selected photo
			 * @param photo
			 */
			vm.setSelectedPhoto = function(photo){
				vm.selectedPhoto = photo;
			};

			/**
			 * Update the photo gallery
			 */
			vm.updateBuilderObjectGallery = function(){

				var profileObj = profileService.getProfileObj();

				if(vm.type == 'video'){
					//console.log('profile builder object', profileObj);
					profileObj.setQuestionValueByQuestionReference('profileVideos', vm.images);
				}
				else{
					//console.log('profile builder object', profileObj);
					profileObj.setQuestionValueByQuestionReference('profileGallery', vm.images);
				}

			};

			/**
			 *	Call a click event on the dropzone button so the device
			 *  file browser cna be opened
			 */
			vm.triggerDropZoneForGalleryImage = function(){

				$("#photo-upload-display").click();
			};

			/**
			 * Get the image path
			 *
			 * @param imageName
			 */
			vm.getImagePath = function(imageObj){

				var path = '';

				if(imageObj.type == 'server'){

					path = window.server_path + 'image/' + imageObj.name;
				}
				else if (imageObj.type == 'facebook'){

					path = imageObj.src;
				}

				return path;
			};

			/**
			 *
			 * Social upload functions
			 */

			/**
			 * Authorise facebook login
			 */
			vm.authFacebook = function(){

				authService.authenticateFacebook('facebook').then(function(data){

					vm.accessTokens.facebook = data;

					authService.getFacebookUserPhotos().then(function(data){

						var photoData = data;

						//Iterate through data and add type
						for(var i = 0; i < photoData.length; i++){

							photoData[i].type = 'facebook';
						}

						vm.socialPhotos = photoData;

						//console.log('social photos', vm.socialPhotos);

						$("#facebook-pic-modal").modal({backdrop: false});
					});

				});

			};

			/**
			 * Set the social image to save
			 *
			 * @param image
			 * @param modal
			 */
			vm.setSocialImageToSave = function(modal){

				//close the modal
				$(modal).modal("hide");

				var fileName = vm.activeSocialPhoto.name;

				//console.log('social image to upload', vm.activeSocialPhoto.socialImg, 'file name', window.server_path + fileName);

				var obj = {
					base64 : vm.activeSocialPhoto.socialImg
				};

				//Upload the image to the server
				$http.post(window.server_path + 'upload/' + fileName, obj).then(function(data){

					//Add the photo to the photo gallery
					var photo = {name : data.data[0].file_name, type : 'server'};

					//console.log('photo adding', photo);

					//Add the image to the photos object
					vm.images.push(photo);

					//Reset the active social image
					vm.activeSocialPhoto = {name : '', src : ''};

					//Update the builder object for typescript
					vm.updateBuilderObjectGallery();
				});

			};

			/**
			 * Set the canvas to the chosen facebook image
			 *
			 * @param id
			 */
			vm.setSocialCanvasImage = function(id, url) {

				var img = new Image();

				img.setAttribute('crossOrigin', 'anonymous');

				img.onload = function () {
					var canvas = document.createElement("canvas");
					canvas.width =this.width;
					canvas.height =this.height;

					var ctx = canvas.getContext("2d");
					ctx.drawImage(this, 0, 0);

					var dataURL = canvas.toDataURL("image/png");

					//The image data stored as a base64 to be displayed
					var justData = dataURL.replace(/^data:image\/(png|jpg);base64,/, "");

					vm.activeSocialPhoto.name = id;
					vm.activeSocialPhoto.socialImg = justData;
				};

				img.src = url;
			};

			/**
			 * Update the builder typescript object (callback from image edit)
			 * @param imageObj
			 * @param option
			 */
			vm.updateBuilderObject = function(imageObj, option){

				//get buiilder obj ...
				//Update the profile builder object
				var profileBuilderObj = profileService.getProfileObj();

				//profileImage or profileGallery
				if(option == 'profileImage'){

					profileBuilderObj.setQuestionValueByQuestionReference('profileImage', imageObj);
				}

				if(option == 'coverPhoto'){
					profileBuilderObj.setQuestionValueByQuestionReference('profileCoverImage', imageObj);
				}

				//Save the gallery
				vm.updateBuilderObjectGallery();
			};

			vm.setUpImageUploadDirective();

        }
    }

})();




