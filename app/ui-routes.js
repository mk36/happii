app.config(function($stateProvider, $urlRouterProvider) {


    //
    // For any unmatched url, redirect to /state1
    $urlRouterProvider.otherwise("/home");
    //
    // Now set up the states
    $stateProvider

        .state('home', {
            url: "/home",
            templateUrl: "app/views/home/home.html",
            controller: "homeController as vm"
        })

		.state('sign-in', {
			url: "/sign-in",
			templateUrl: "app/views/home/signIn.html",
			controller: "signInController as vm"
		})

		.state('safety-guidelines', {
			url: "/safety-guidelines",
			templateUrl: "app/views/home/safetyGuidelines.html"
			//controller: "signInController as vm"
		})
		.state('terms', {
			url: "/terms",
			templateUrl: "app/views/home/terms.html"
			//controller: "signInController as vm"
		})
		.state('privacy', {
			url: "/privacy",
			templateUrl: "app/views/home/privacy.html"
			//controller: "signInController as vm"
		})
		.state('contact', {
			url: "/contact",
			templateUrl: "app/views/home/contact.html"
			//controller: "signInController as vm"
		})


		//Register states
        .state('register', {
            url: "/register",
            templateUrl: "app/views/register/register.html",
            controller: "registerController as vm",
            redirectTo: 'register.gender'
        })
        .state('register.gender', {
            url: "/gender",
            templateUrl: "app/views/register/questions/gender.html",
            controller: "registerGenderController as vm"
        })
        .state('register.dob', {
            url: "/dob",
            templateUrl: "app/views/register/questions/dob.html",
            controller: "registerDOBController as vm"
        })
        .state('register.location', {
            url: "/location",
            templateUrl: "app/views/register/questions/location.html",
            controller: "registerLocationController as vm"
        })
        .state('register.personal-details', {
            url: "/personal-details",
            templateUrl: "app/views/register/questions/personalDetails.html",
            controller: "registerPersonalDetailsController as vm"
        })
		.state('register.complete', {
			url: "/complete",
			templateUrl: "app/views/register/complete.html",
			controller: "registerCompleteController as vm"
		})


        //Builder states
		.state('builder', {
			url: "/builder",
			templateUrl: "app/views/builder/builder.html",
			controller: "builderController as vm",
            redirectTo: 'builder.welcome'
		})
		.state('builder.welcome', {
			url: "/welcome?email&code",
			templateUrl: "app/views/builder/welcome.html",
			controller: "builderWelcomeController as vm"
		})
		.state('builder.activate', {
			url: "/activate?email&code",
			templateUrl: "app/views/builder/activate.html",
			controller: "builderActivateController as vm"
		})
		.state('builder.password-reset', {
			url: "/password-reset?email&code",
			templateUrl: "app/views/builder/passwordReset.html",
			controller: "builderPasswordResetController as vm"
		})
		.state('builder.questions', {
			url: "/questions",
			templateUrl: "app/views/builder/builder-questions.html",
			controller: "builderQuestionController as vm",
            redirectTo: 'builder.questions.image-section'
		})

		// Image section questions (gallery and profile picture)
		.state('builder.questions.image-section', {
			url: "/image-section",
			templateUrl: "app/views/builder/questions/imageSection/imageSection.html",
			controller: "builderQuestionImageSectionController as vm"
		})
		.state('builder.questions.image-section.intro', {
			url: "/intro",
			templateUrl: "app/views/builder/questions/imageSection/intro.html"
		})
		.state('builder.questions.image-section.profile-photo', {
			url: "/profile-photo",
			templateUrl: "app/views/builder/questions/imageSection/profilePhoto.html"
		})
        .state('builder.questions.image-section.profile-cover-photo', {
            url: "/profile-cover-photo",
            templateUrl: "app/views/builder/questions/imageSection/profileCoverPhoto.html"
        })
        .state('builder.questions.image-section.profile-gallery-photo-1', {
            url: "/profile-gallery-photo-1",
            templateUrl: "app/views/builder/questions/imageSection/profileGalleryPhoto1.html"
        })
        .state('builder.questions.image-section.profile-gallery-photo-2', {
            url: "/profile-gallery-photo-2",
            templateUrl: "app/views/builder/questions/imageSection/profileGalleryPhoto2.html"
        })
		.state('builder.questions.image-section.profile-video', {
			url: "/profile-video",
			templateUrl: "app/views/builder/questions/imageSection/profileVideo.html"
		})
		.state('builder.questions.image-section.outro', {
			url: "/outro",
			templateUrl: "app/views/builder/questions/imageSection/outro.html"
		})

		//Profile Description question
		.state('builder.questions.profile-description', {
			url: "/profile-description",
			templateUrl: "app/views/builder/questions/ProfileDescriptionBuilder/profileDescriptionBuilder.html",
			controller: "builderProfileDescriptionController as vm"
		})

		//Basic stats questions
		.state('builder.questions.basic-stats', {
			url: "/basic-stats",
			templateUrl: "app/views/builder/questions/builderBasicStats/builderBasicStats.html",
			controller: "builderBasicStatsController as vm"
		})
		.state('builder.questions.basic-stats.location-from', {
			url: "/location-from",
			templateUrl: "app/views/builder/questions/builderBasicStats/locationFrom.html",
			controller: "builderBasicStatsLocationFromController as vm"
		})
		.state('builder.questions.basic-stats.education', {
			url: "/education",
			templateUrl: 'app/views/builder/questions/builderBasicStats/education.html'
		})
		.state('builder.questions.basic-stats.employment', {
			url: "/employment",
			templateUrl: 'app/views/builder/questions/builderBasicStats/employment.html'
		})
		.state('builder.questions.basic-stats.religion', {
			url: "/religion",
			templateUrl: 'app/views/builder/questions/builderBasicStats/religion.html'
		})
		.state('builder.questions.basic-stats.body-type', {
			url: "/body-type",
			templateUrl: 'app/views/builder/questions/builderBasicStats/bodyType.html'
		})
		.state('builder.questions.basic-stats.children', {
			url: "/children",
			templateUrl: 'app/views/builder/questions/builderBasicStats/children.html'
		})
		.state('builder.questions.basic-stats.smoking', {
			url: "/smoking",
			templateUrl: 'app/views/builder/questions/builderBasicStats/smoking.html'
		})
		.state('builder.questions.basic-stats.drinking', {
			url: "/drinking",
			templateUrl: 'app/views/builder/questions/builderBasicStats/drinking.html'
		})
		.state('builder.questions.basic-stats.height', {
			url: "/height",
			templateUrl: 'app/views/builder/questions/builderBasicStats/height.html',
			controller: "builderBasicStatsHeightController as vm"
		})

    	//Interest questions
		.state('builder.questions.interests', {
			url: "/interests",
			templateUrl: "app/views/builder/questions/builderInterests/builderInterests.html",
			controller: "builderInterestsController as vm"
		})
		.state('builder.questions.interests.personality', {
			url: "/personality",
			templateUrl: "app/views/builder/questions/builderInterests/personality.html"
		})
		.state('builder.questions.interests.past-times', {
			url: "/past-times",
			templateUrl: "app/views/builder/questions/builderInterests/pastTimes.html"
		})
		.state('builder.questions.interests.books', {
			url: "/books",
			templateUrl: "app/views/builder/questions/builderInterests/books.html"
		})
		.state('builder.questions.interests.music', {
			url: "/music",
			templateUrl: "app/views/builder/questions/builderInterests/music.html"
		})
		.state('builder.questions.interests.viewings', {
			url: "/viewings",
			templateUrl: "app/views/builder/questions/builderInterests/viewings.html"
		})
		.state('builder.questions.interests.sport', {
			url: "/sport",
			templateUrl: "app/views/builder/questions/builderInterests/sport.html"
		})

    	//Travel questions
		.state('builder.questions.travel', {
			url: "/travel",
			templateUrl: "app/views/builder/questions/builderTravel/builderTravel.html",
			controller: "builderTravelController as vm"
		})
		.state('builder.questions.travel.places-set', {
			url: "/places-set",
			templateUrl: "app/views/builder/questions/builderTravel/placesSet.html",
			controller: "builderTravelPlacesSetController as vm"
		})
		.state('builder.questions.travel.places-describe', {
			url: "/places-describe",
			templateUrl: "app/views/builder/questions/builderTravel/placesDescribe.html",
			controller: "builderTravelPlacesDescribeController as vm"
		})

		//Partner questions
		.state('builder.questions.partner', {
			url: "/partner",
			templateUrl: "app/views/builder/questions/builderPartner/builderPartner.html",
			controller: "builderPartnerController as vm"
		})
		.state('builder.questions.partner.gender', {
			url: "/gender",
			templateUrl: "app/views/builder/questions/builderPartner/gender.html",
			controller: "builderPartnerGenderController as vm"
		})
		.state('builder.questions.partner.age-range', {
			url: "/age-range",
			templateUrl: "app/views/builder/questions/builderPartner/ageRange.html",
			controller: "builderPartnerAgeRangeController as vm"
		})
		.state('builder.questions.partner.location', {
			url: "/location",
			templateUrl: "app/views/builder/questions/builderPartner/location.html",
			controller: "builderPartnerLocationController as vm"
		})
		.state('builder.questions.partner.children', {
			url: "/children",
			templateUrl: "app/views/builder/questions/builderPartner/children.html"
		})
		.state('builder.questions.partner.smoking', {
			url: "/smoking",
			templateUrl: "app/views/builder/questions/builderPartner/smoking.html"
		})
		.state('builder.questions.partner.drinking', {
			url: "/drinking",
			templateUrl: "app/views/builder/questions/builderPartner/drinking.html"
		})
		.state('builder.questions.partner.height', {
			url: "/height",
			templateUrl: "app/views/builder/questions/builderPartner/height.html",
			controller: "builderPartnerHeightController as vm"
		})
		.state('builder.questions.partner.education', {
			url: "/education",
			templateUrl: "app/views/builder/questions/builderPartner/education.html"
		})
		.state('builder.questions.partner.employment', {
			url: "/employment",
			templateUrl: "app/views/builder/questions/builderPartner/employment.html"
		})
		.state('builder.questions.partner.body-type', {
			url: "/body-type",
			templateUrl: "app/views/builder/questions/builderPartner/bodyType.html"
		})
		.state('builder.questions.partner.religion', {
			url: "/religion",
			templateUrl: "app/views/builder/questions/builderPartner/religion.html"
		})
		.state('builder.questions.partner-interests', {
			url: "/partner-interests",
			templateUrl: "app/views/builder/questions/builderPartner/partnerInterests.html",
			controller: "builderPartnerInterestsController as vm"
		})
		.state('builder.questions.partner-interests.personality-traits', {
			url: "/personality-traits",
			templateUrl: "app/views/builder/questions/builderPartner/personality.html"
		})
		.state('builder.questions.partner-interests.past-times', {
			url: "/past-times",
			templateUrl: "app/views/builder/questions/builderPartner/pastTimes.html"
		})
		.state('builder.questions.complete', {
			url: "/complete",
			templateUrl: "app/views/builder/questions/complete.html",
			controller: "builderProfileCompleteController as vm"
		})

    	//Profile states
		.state('profile-view', {
			url: "/profile-view/{id}",
			templateUrl: "app/views/profile/profilePageView.html",
			controller: "profilePageViewController as vm"
		})
		.state('profile', {
			url: "/profile/",
			templateUrl: "app/views/profile/profile.html",
			controller: "profileController as vm",
			redirectTo: 'profile.profile-edit'
		})
		.state('profile.profile-edit', {
			url: "profile-edit",
			templateUrl: "app/views/profile/profileEdit.html",
			controller: "profileEditController as vm"
		})
		.state('profile.photo-gallery', {
			url: "/photo-gallery",
			templateUrl: "app/views/profile/photoGallery.html"
			//controller: "profileGalleryController as vm"
		})
		.state('profile.video-gallery', {
			url: "/video-gallery",
			templateUrl: "app/views/profile/videoGallery.html",
			controller: "profileVideoGalleryController as vm"
		})
        .state('profile.pings', {
            url: "pings",
            templateUrl: "app/views/profile/pings.html",
            //controller: "profileVideoGalleryController as vm"
        })
        .state('profile.activity', {
            url: "activity",
            templateUrl: "app/views/profile/pings.html",
            //controller: "profileVideoGalleryController as vm"
        })
        .state('profile.favourites', {
            url: "favourites",
            templateUrl: "app/views/profile/pings.html",
            //controller: "profileVideoGalleryController as vm"
        })
        .state('profile.diary', {
            url: "pings",
            templateUrl: "app/views/profile/diary.html",
            //controller: "profileVideoGalleryController as vm"
        })
        .state('profile.messages', {
            url: "pings",
            templateUrl: "app/views/profile/messages.html",
            //controller: "profileVideoGalleryController as vm"
        })
        .state('profile.search', {
            url: "search",
            templateUrl: "app/views/profile/search.html",
            //controller: "profileVideoGalleryController as vm"
        });

        //.state('cards.card', {
        //    url: "/card/{id}",
        //    templateUrl: "app/views/cards/card-full-template.html",
        //    params: {
        //        card: null
        //    }
        //}),

});

app.run(['$rootScope', '$state', function($rootScope, $state) {

    $rootScope.$on('$stateChangeStart', function(evt, to, params) {
        if (to.redirectTo) {
            evt.preventDefault();
            $state.go(to.redirectTo, params, {location: 'replace'})
        }
    });
}]);